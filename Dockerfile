# Stage 1 - Maven-Build durchführen
FROM docker.io/maven:3.9.9-amazoncorretto-21-alpine AS builder

# Verzeichnis erstellen, in das alle Dateien reinkopiert werden
RUN mkdir -p /app
WORKDIR /app

# hiermit werden alle Dependencies (vorab) runtergeladen
# Ändert sich was am Source Code, müssen die Dependencies nicht neu heruntergeladen werden
COPY ./pom.xml /app
RUN mvn -f /app/pom.xml clean

COPY src /app/src
RUN mvn -f /app/pom.xml package -DskipTests
# skipTests, damit der Build schneller geht - Tests können lokal ausgeführt werden

# Stage 2 - entstandene Fat Jar in verschiedene Blöcke aufteilen
FROM docker.io/bellsoft/liberica-openjre-alpine:21.0.6 AS fatjardivider

# Verzeichnis erstellen, in das die Fat Jar reinkopiert wird
RUN mkdir -p /app
WORKDIR /app

# Fat Jar reinkopieren
COPY --from=builder /app/target/*.jar /app/app.jar

# Interne Container-Ports, die der Außenwelt zur Verfügung stehen sollen
EXPOSE 8080

# Der nachfolgende Block teilt die Fat Jar in mehrere Parts auf,
# die im Docker-Container als einzelne Layer vorliegen.
# Dadurch sollen inkrementelle Pushes und Pulls in dieser Stage ermöglicht werden
RUN java -Djarmode=layertools -jar /app/app.jar extract


# Stage 3 - Botcchi starten
FROM docker.io/bellsoft/liberica-openjre-alpine:21.0.6

# Verzeichnis erstellen von dem aus Botcchi ausgeführt werden soll + Berechtigungen
RUN addgroup -S botcchi \
    && adduser -S botcchi -G botcchi \
    && mkdir -p /app \
    && chown botcchi /app
USER botcchi:botcchi
WORKDIR /app

COPY --from=fatjardivider /app/spring-boot-loader/ .
COPY --from=fatjardivider /app/dependencies/ .
COPY --from=fatjardivider /app/application/ .

# Was ausgeführt werden soll, wenn "docker run" aufgerufen wird
ENTRYPOINT ["java", "org.springframework.boot.loader.launch.JarLauncher"]