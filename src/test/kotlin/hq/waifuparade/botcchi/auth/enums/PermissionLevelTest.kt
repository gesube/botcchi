package hq.waifuparade.botcchi.auth.enums

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class PermissionLevelTest {

    @Test
    fun getPermissionFromRank_Anonymous() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.ANONYMOUS)
    }

    @Test
    fun getPermissionFromRank_Guest() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.GUEST)
    }

    @Test
    fun getPermissionFromRank_Registered() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.REGISTERED)
    }

    @Test
    fun getPermissionFromRank_Leader() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.LEADER)
    }

    @Test
    fun getPermissionFromRank_Moderator() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.MODERATOR)
    }

    @Test
    fun getPermissionFromRank_ChannelAdmin() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.CHANNEL_ADMIN)
    }

    @Test
    fun getPermissionFromRank_Owner() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.OWNER)
    }

    @Test
    fun getPermissionFromRank_Founder() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.FOUNDER)
    }

    @Test
    fun getPermissionFromRank_Nobody() {
        this.getPermissionFromRank_ActualTest(PermissionLevel.NOBODY)
    }

    private fun getPermissionFromRank_ActualTest(expectedPermissionLevel: PermissionLevel) {
        val rank = expectedPermissionLevel.rank

        // we do 1 mio. tests per permission to guarantee there are no precision problems for the Double type
        for (i in 1..1000000) {
            val actualPermission = PermissionLevel.getPermissionFromRank(rank)
            assertEquals(expectedPermissionLevel, actualPermission)
        }
    }
}