package hq.waifuparade.botcchi.cytube.listener.annotations

import hq.waifuparade.botcchi.AbstractIntegrationTest
import hq.waifuparade.botcchi.TestConstants.Companion.MAIN_PACKAGE
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.ScannedGenericBeanDefinition
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.test.context.ContextConfiguration

@ComponentScan(basePackageClasses = [CytubeEventListener::class])
@ContextConfiguration(
    classes = [
        AbstractIntegrationTest.DefaultStartupTestConfig::class,
        AbstractIntegrationTest.DefaultShutdownTestConfig::class
    ]
                     )
internal class CytubeEventListenerIntegrationTest(
    @Autowired private val eventListener: List<AbstractEventListener>
                                                 ) : AbstractIntegrationTest {

    @Test
    fun checkEveryListenerHasCytubeEventListenerAnnotation() {
        this.eventListener.forEach { emitter ->
            val annotations = emitter.javaClass.annotations
            val eventEmitterAnnotation = annotations.find { it is CytubeEventListener }
            Assertions.assertNotNull(eventEmitterAnnotation)
        }
    }

    @Test
    fun checkCytubeEventListenerAnnotationIsOnlyUsedByListeners() {
        val scanner = ClassPathScanningCandidateComponentProvider(false)
        scanner.addIncludeFilter(AnnotationTypeFilter(CytubeEventListener::class.java))
        val expectedListenerPackage = AbstractEventListener::class.java.packageName

        val beans = scanner.findCandidateComponents(MAIN_PACKAGE)
        Assertions.assertTrue(beans.isNotEmpty())

        beans.forEach { bean ->
            Assertions.assertTrue(bean is ScannedGenericBeanDefinition)
            val metadata = (bean as ScannedGenericBeanDefinition).metadata
            val qualifiedNameOfSuperClass: String = metadata.superClassName.toString()
            Assertions.assertNotNull(qualifiedNameOfSuperClass)
            Assertions.assertTrue(qualifiedNameOfSuperClass.contains(expectedListenerPackage))
        }
    }
}