package hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.impls.timer

import hq.waifuparade.botcchi.cytube.listener.services.chatevents.ChatEventResponse
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.PublicMessageEventResponse
import hq.waifuparade.botcchi.taskscheduling.TaskSchedulingApi
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class TimerCommandTest {

    private lateinit var chatCommand: TimerCommand
    private lateinit var chatPropertiesReader: ChatPropertiesReader
    private lateinit var taskSchedulingApi: TaskSchedulingApi

    private val generalIdentifier = "#"
    private val timerIdentifier = "timer"
    private val completeIdentifier = generalIdentifier + timerIdentifier
    private val completeTime = "3h2m1s"
    private val completeTimeDelay: Long = ((3*60*60) + (2*60) + 1)*1000
    private val userMessage = "Pizza fertig"
    private val userBotcchi = "Botcchi"
    private val userBotcchiList = listOf(userBotcchi)

    @BeforeEach
    fun setUp() {
        this.chatPropertiesReader = mockk<ChatPropertiesReader>()
        every { this@TimerCommandTest.chatPropertiesReader.general.identifier } returns this.generalIdentifier
        every { this@TimerCommandTest.chatPropertiesReader.timer.identifier } returns this.timerIdentifier

        this.taskSchedulingApi = mockk<TaskSchedulingApi>()
        every { taskSchedulingApi.scheduleMessages(any(), any(), any()) } returns Unit

        chatCommand = TimerCommand(this.chatPropertiesReader, this.taskSchedulingApi)
    }

    @Test
    fun reactToCommand_shouldNotReact() = runTest {
        val message = "Hello World"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 0) { taskSchedulingApi.scheduleMessages(any(), any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldNotWork_NoTime() = runTest {
        val message = "$completeIdentifier -m $userMessage -u $userBotcchi"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 0) { taskSchedulingApi.scheduleMessages(any(), any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_completeExample() = runTest {
        val message = "$completeIdentifier -t $completeTime -m $userMessage -u $userBotcchi"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(completeTimeDelay, userMessage, userBotcchiList) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_onlyTime() = runTest {
        val message = "$completeIdentifier -t $completeTime"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(completeTimeDelay, any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_OnlySeconds() = runTest {
        val time = "5s"
        val delay: Long = 5*1000
        val message = "$completeIdentifier -t $time"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(delay, any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_OnlyMinutes() = runTest {
        val time = "5m"
        val delay: Long = 60*5*1000
        val message = "$completeIdentifier -t $time"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(delay, any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_OnlyHours() = runTest {
        val time = "5h"
        val delay: Long = 60*60*5*1000
        val message = "$completeIdentifier -t $time"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(delay, any(), any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_OnlyTimeAndMessage() = runTest {
        val message = "$completeIdentifier -t $completeTime -m $userMessage"
        val user = "user"
        val chatEvent = createChatEventResponse(message, user)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(completeTimeDelay, userMessage, listOf(user)) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_OnlyTimeAndUser() = runTest {
        val message = "$completeIdentifier -t $completeTime -u $userBotcchi"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(completeTimeDelay, any(), userBotcchiList) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_MultipleWordsUserMessage() = runTest {
        val longUserMessage = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor"
        val message = "$completeIdentifier -t $completeTime -m $longUserMessage"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(any(), longUserMessage, any()) }
    }

    @Test
    fun reactToCommand_shouldReact_shouldWork_MultipleUsers() = runTest {
        val multipleUsers = "Botcchi1 Botcchi2 Botcchi3"
        val message = "$completeIdentifier -t $completeTime -u $multipleUsers"
        val chatEvent = createChatEventResponse(message)

        chatCommand.reactToCommand(chatEvent)
        verify(exactly = 1) { taskSchedulingApi.scheduleMessages(any(), any(), listOf("Botcchi1", "Botcchi2", "Botcchi3")) }
    }


    private fun createChatEventResponse(message: String, user: String = "user"): ChatEventResponse {
        return PublicMessageEventResponse(
            msg = message,
            time = System.currentTimeMillis(),
            username =  user,
            meta =
                mapOf()
        )
    }
}