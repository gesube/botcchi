package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.LoginEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.socket.emitter.Emitter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

internal class LoginEventEmitterTest : AbstractEventEmitterTest() {

    private lateinit var socketClient: CytubeSocketClient
    private lateinit var propertiesReader: CytubePropertiesReader
    override lateinit var eventEmitter: AbstractEventEmitter

    override fun getResponsibleEventType(): OutgoingCytubeEventType {
        return OutgoingCytubeEventType.LOGIN
    }

    override fun createEventEmitter(): LoginEventEmitter {
        this.socketClient = mockk()
        coEvery { this@LoginEventEmitterTest.socketClient.emit(any(), any()) } answers { Emitter() }

        this.propertiesReader = mockk()
        every { this@LoginEventEmitterTest.propertiesReader.user.name } answers { "username" }
        every { this@LoginEventEmitterTest.propertiesReader.user.password } answers { "password" }

        return LoginEventEmitter(this.socketClient, this.propertiesReader)
    }

    @Test
    fun emitEvent_NoEventRequestData() = runBlocking {
        verify(exactly = 0) { this@LoginEventEmitterTest.propertiesReader.user }
        this@LoginEventEmitterTest.eventEmitter.emitEvent()

        // name and password is encapsulated in user variable
        verify(exactly = 2) { this@LoginEventEmitterTest.propertiesReader.user }
    }

    @Test
    fun emitEvent_ProvidedEventRequestData() = runBlocking {
        verify(exactly = 0) { this@LoginEventEmitterTest.propertiesReader.user }

        val req = LoginEventRequest(name = "username", pw = "password")
        this@LoginEventEmitterTest.eventEmitter.emitEvent(req)

        // if the login data is provided, the data is not read from the properties
        verify(exactly = 0) { this@LoginEventEmitterTest.propertiesReader.user }
    }
}