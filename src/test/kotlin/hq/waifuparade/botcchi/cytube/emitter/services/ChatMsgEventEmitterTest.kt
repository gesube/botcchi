package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.PublicMessageEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import io.socket.emitter.Emitter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

internal class ChatMsgEventEmitterTest : AbstractEventEmitterTest() {

    private lateinit var socketClient: CytubeSocketClient
    override lateinit var eventEmitter: AbstractEventEmitter

    override fun getResponsibleEventType(): OutgoingCytubeEventType {
        return OutgoingCytubeEventType.CHAT_MSG
    }

    override fun createEventEmitter(): ChatMsgEventEmitter {
        this.socketClient = mockk()
        coEvery { this@ChatMsgEventEmitterTest.socketClient.emit(any(), any()) } answers { Emitter() }
        return ChatMsgEventEmitter(this.socketClient)
    }

    @Test
    fun emitEvent_NoEventRequestData() = runBlocking {
        verify(exactly = 0) { this@ChatMsgEventEmitterTest.socketClient.emit(any(), any()) }
        this@ChatMsgEventEmitterTest.eventEmitter.emitEvent()
        verify(exactly = 0) { this@ChatMsgEventEmitterTest.socketClient.emit(any(), any()) }
    }

    @Test
    fun emitEvent_ProvidedEventRequestData() = runBlocking {
        verify(exactly = 0) { this@ChatMsgEventEmitterTest.socketClient.emit(any(), any()) }
        val req = PublicMessageEventRequest(msg = "test message")
        this@ChatMsgEventEmitterTest.eventEmitter.emitEvent(req)
        verify(exactly = 1) { this@ChatMsgEventEmitterTest.socketClient.emit(any(), any()) }
    }
}