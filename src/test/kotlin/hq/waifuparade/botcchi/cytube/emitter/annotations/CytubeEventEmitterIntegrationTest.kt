package hq.waifuparade.botcchi.cytube.emitter.annotations

import hq.waifuparade.botcchi.AbstractIntegrationTest
import hq.waifuparade.botcchi.TestConstants.Companion.MAIN_PACKAGE
import hq.waifuparade.botcchi.cytube.emitter.services.AbstractEventEmitter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.ScannedGenericBeanDefinition
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.test.context.ContextConfiguration

@ComponentScan(basePackageClasses = [CytubeEventEmitter::class])
@ContextConfiguration(
    classes = [
        AbstractIntegrationTest.DefaultStartupTestConfig::class,
        AbstractIntegrationTest.DefaultShutdownTestConfig::class
    ]
                     )
internal class CytubeEventEmitterIntegrationTest(
    @Autowired private val eventEmitter: List<AbstractEventEmitter>
                                                ) : AbstractIntegrationTest {

    @Test
    fun checkEveryEmitterHasCytubeEventEmitterAnnotation() {
        this.eventEmitter.forEach { emitter ->
            val annotations = emitter.javaClass.annotations
            val eventEmitterAnnotation = annotations.find { it is CytubeEventEmitter }
            Assertions.assertNotNull(eventEmitterAnnotation)
        }
    }

    @Test
    fun checkCytubeEventEmitterAnnotationIsOnlyUsedByEmitters() {
        val scanner = ClassPathScanningCandidateComponentProvider(false)
        scanner.addIncludeFilter(AnnotationTypeFilter(CytubeEventEmitter::class.java))

        val beans = scanner.findCandidateComponents(MAIN_PACKAGE)
        Assertions.assertTrue(beans.isNotEmpty())

        beans.forEach { bean ->
            Assertions.assertTrue(bean is ScannedGenericBeanDefinition)
            val metadata = (bean as ScannedGenericBeanDefinition).metadata
            val qualifiedNameOfSuperClass = metadata.superClassName
            Assertions.assertNotNull(qualifiedNameOfSuperClass)
            Assertions.assertEquals(AbstractEventEmitter::class.qualifiedName, qualifiedNameOfSuperClass)
        }
    }
}