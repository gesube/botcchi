package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal abstract class AbstractEventEmitterTest {

    abstract var eventEmitter: AbstractEventEmitter

    @BeforeEach
    fun setUp() {
        this.eventEmitter = this.createEventEmitter()
    }

    @Test
    fun getEventType() {
        val expectedEventType = this.getResponsibleEventType()
        val actualEventType = this.eventEmitter.getEventType()
        Assertions.assertEquals(expectedEventType, actualEventType)
    }

    abstract fun getResponsibleEventType(): OutgoingCytubeEventType

    abstract fun createEventEmitter(): AbstractEventEmitter
}