package hq.waifuparade.botcchi.cytube.emitter.annotations

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaType

@Tag("unit-tests")
internal class CytubeEventEmitterTest {

    private val annotations = CytubeEventEmitter::class.java.annotations

    @Test
    fun checkTargetAnnotation() {
        // @Target
        val annotation = annotations.find { it is Target }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Target

        Assertions.assertEquals(1, annotation.allowedTargets.size)
        Assertions.assertEquals(AnnotationTarget.CLASS, annotation.allowedTargets[0])
    }

    @Test
    fun checkRetentionAnnotation() {
        // @Target
        val annotation = annotations.find { it is Retention }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Retention

        Assertions.assertEquals(AnnotationRetention.RUNTIME, annotation.value)
    }

    @Test
    fun checkProperties() {
        val expectedPropertyTypeNames = listOf(
            OutgoingCytubeEventType::class.qualifiedName
                                              )
        val actualProperties = CytubeEventEmitter::class.memberProperties
        Assertions.assertEquals(expectedPropertyTypeNames.size, actualProperties.size)

        actualProperties.iterator().forEach { property ->
            val fullyQualifiedName = property.returnType.javaType.typeName
            Assertions.assertTrue(expectedPropertyTypeNames.contains(fullyQualifiedName))
        }
    }
}