package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.JoinChannelEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.socket.emitter.Emitter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class JoinChannelEventEmitterTest : AbstractEventEmitterTest() {

    private lateinit var socketClient: CytubeSocketClient
    private lateinit var propertiesReader: CytubePropertiesReader
    override lateinit var eventEmitter: AbstractEventEmitter

    override fun getResponsibleEventType(): OutgoingCytubeEventType {
        return OutgoingCytubeEventType.JOIN_CHANNEL
    }

    override fun createEventEmitter(): JoinChannelEventEmitter {
        this.socketClient = mockk()
        coEvery { this@JoinChannelEventEmitterTest.socketClient.emit(any(), any()) } answers { Emitter() }

        this.propertiesReader = mockk()
        every { this@JoinChannelEventEmitterTest.propertiesReader.room.name } answers { "roomname" }

        return JoinChannelEventEmitter(this.socketClient, this.propertiesReader)
    }

    @Test
    fun emitEvent_NoEventRequestData() = runBlocking {
        verify(exactly = 0) { this@JoinChannelEventEmitterTest.propertiesReader.room }
        this@JoinChannelEventEmitterTest.eventEmitter.emitEvent()

        // name and password is encapsulated in user variable
        verify(exactly = 1) { this@JoinChannelEventEmitterTest.propertiesReader.room }
    }

    @Test
    fun emitEvent_ProvidedEventRequestData() = runBlocking {
        verify(exactly = 0) { this@JoinChannelEventEmitterTest.propertiesReader.room }

        val req = JoinChannelEventRequest(name = "roomname")
        this@JoinChannelEventEmitterTest.eventEmitter.emitEvent(req)

        // if the login data is provided, the data is not read from the properties
        verify(exactly = 0) { this@JoinChannelEventEmitterTest.propertiesReader.room }
    }
}