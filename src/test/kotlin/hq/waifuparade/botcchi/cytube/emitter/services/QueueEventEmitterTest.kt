package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.QueueMediaEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import io.socket.emitter.Emitter
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

internal class QueueEventEmitterTest : AbstractEventEmitterTest() {

    private lateinit var socketClient: CytubeSocketClient
    override lateinit var eventEmitter: AbstractEventEmitter

    override fun getResponsibleEventType(): OutgoingCytubeEventType {
        return OutgoingCytubeEventType.QUEUE
    }

    override fun createEventEmitter(): QueueEventEmitter {
        this.socketClient = mockk()
        coEvery { this@QueueEventEmitterTest.socketClient.emit(any(), any()) } answers { Emitter() }
        return QueueEventEmitter(this.socketClient)
    }

    @Test
    fun emitEvent_NoEventRequestData() = runBlocking {
        verify(exactly = 0) { this@QueueEventEmitterTest.socketClient.emit(any(), any()) }
        this@QueueEventEmitterTest.eventEmitter.emitEvent()
        verify(exactly = 0) { this@QueueEventEmitterTest.socketClient.emit(any(), any()) }
    }

    @Test
    fun emitEvent_ProvidedEventRequestData() = runBlocking {
        verify(exactly = 0) { this@QueueEventEmitterTest.socketClient.emit(any(), any()) }
        val req = QueueMediaEventRequest(id = "id", type = "type", pos = "pos", title = "title", temp = false)
        this@QueueEventEmitterTest.eventEmitter.emitEvent(req)
        verify(exactly = 1) { this@QueueEventEmitterTest.socketClient.emit(any(), any()) }
    }
}