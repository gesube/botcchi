package hq.waifuparade.botcchi.cytube.emitter

import hq.waifuparade.botcchi.cytube.emitter.services.AbstractEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.services.ChatMsgEventEmitter
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils

@Tag("unit-tests")
internal class EmitterApiTest {

    private lateinit var eventEmitters: List<AbstractEventEmitter>
    private lateinit var emitterApi: EmitterApi

    @BeforeEach
    fun setUp() {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(EmitterApi::class.java)

        this.eventEmitters = this.createEventEmitters()

        primaryConstructor?.let {
            this@EmitterApiTest.emitterApi = BeanUtils.instantiateClass(it, eventEmitters)
        }
    }

    private fun createEventEmitters(): List<AbstractEventEmitter> {
        val emitters = mutableListOf<AbstractEventEmitter>(mockk<ChatMsgEventEmitter>())

        emitters.forEach { emitter ->
            coEvery { emitter.getEventType() } answers { callOriginal() }
            coEvery { emitter.emitEvent(any()) } answers { }
        }

        return emitters
    }

    @Test
    fun callEventEmitter() = runBlocking {
        this@EmitterApiTest.eventEmitters.forEach { emitter ->
            coVerify(exactly = 0) { emitter.getEventType() }
            coVerify(exactly = 0) { emitter.emitEvent(any()) }
        }

        this@EmitterApiTest.emitterApi.callEventEmitter(OutgoingCytubeEventType.CHAT_MSG)

        this@EmitterApiTest.eventEmitters.forEach { emitter ->
            coVerify(exactly = 1) { emitter.getEventType() }
            coVerify(exactly = 1) { emitter.emitEvent(any()) }
        }
    }
}