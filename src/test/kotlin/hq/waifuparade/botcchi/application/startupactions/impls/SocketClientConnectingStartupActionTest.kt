package hq.waifuparade.botcchi.application.startupactions.impls

import hq.waifuparade.botcchi.socket.CytubeSocketClient
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class SocketClientConnectingStartupActionTest {

    @Test
    fun executeAction() {
        val socketMock = mockk<CytubeSocketClient>()
        every { socketMock.connectToSocketServer() } returns Unit

        verify(exactly = 0) { socketMock.connectToSocketServer() }

        val startupAction = SocketClientConnectingStartupAction(socketMock)
        startupAction.executeAction()

        verify(exactly = 1) { socketMock.connectToSocketServer() }
    }
}