package hq.waifuparade.botcchi.application.shutdownactions.impls

import hq.waifuparade.botcchi.socket.CytubeSocketClient
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class SocketClientDisconnectingShutdownActionTest {

    @Test
    fun executeAction() {
         val socketMock = mockk<CytubeSocketClient>()
        every { socketMock.disconnectFromSocketServer() } returns Unit

        verify(exactly = 0) { socketMock.disconnectFromSocketServer() }

        val shutdownAction = SocketClientDisconnectingShutdownAction(socketMock)
        shutdownAction.executeAction()

        verify(exactly = 1) { socketMock.disconnectFromSocketServer() }
    }
}