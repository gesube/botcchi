package hq.waifuparade.botcchi.application

import hq.waifuparade.botcchi.application.shutdownactions.impls.SocketClientDisconnectingShutdownAction
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.context.event.ContextClosedEvent

@Tag("unit-tests")
internal class ApplicationShutdownTest {

    private val contextClosedEvent = mockk<ContextClosedEvent>()

    @Test
    fun handleContextClose() {
        // Create dependencies for shutdown class
        val socketClientDisconnectingShutdownAction = mockk<SocketClientDisconnectingShutdownAction>()

        // Create invocations counter for methods without return type
        var numberOfInvocations = 0
        every { socketClientDisconnectingShutdownAction.executeAction() }.answers(answer = {
            numberOfInvocations++
        })

        // pre-checks
        assertEquals(0, numberOfInvocations)

        // Build shutdown class
        val applicationShutdown = ApplicationShutdown(listOf(socketClientDisconnectingShutdownAction))
        applicationShutdown.handleContextClose(contextClosedEvent)

        // checks
        assertEquals(1, numberOfInvocations)
        verify(exactly = 1) { socketClientDisconnectingShutdownAction.executeAction() }
    }
}