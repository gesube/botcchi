package hq.waifuparade.botcchi.application

import hq.waifuparade.botcchi.application.startupactions.impls.SocketClientConnectingStartupAction
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.context.event.ContextRefreshedEvent

@Tag("unit-tests")
internal class ApplicationStartupTest {

    private val contextClosedEvent = mockk<ContextRefreshedEvent>()

    @Test
    fun handleContextClose() {
        // Create dependencies for shutdown class
        val socketClientConnectingStartupAction = mockk<SocketClientConnectingStartupAction>()

        // Create invocations counter for methods without return type
        var numberOfInvocations = 0
        every { socketClientConnectingStartupAction.executeAction() }.answers(answer = {
            numberOfInvocations++
        })

        // pre-checks
        assertEquals(0, numberOfInvocations)

        // Build shutdown class
        val applicationStartup = ApplicationStartup(listOf(socketClientConnectingStartupAction))
        applicationStartup.handleContextRefresh(contextClosedEvent)

        // checks
        assertEquals(1, numberOfInvocations)
        verify(exactly = 1) { socketClientConnectingStartupAction.executeAction() }
    }
}