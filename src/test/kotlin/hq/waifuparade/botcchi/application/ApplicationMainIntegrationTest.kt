package hq.waifuparade.botcchi.application

import hq.waifuparade.botcchi.AbstractIntegrationTest
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("smoke-tests")
internal class ApplicationMainIntegrationTest : AbstractIntegrationTest {

    @Test
    @Suppress("EmptyMethod")
    fun doSmokeTest() {
        // just check that the application starts
    }
}