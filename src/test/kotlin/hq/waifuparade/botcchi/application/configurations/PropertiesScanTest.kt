package hq.waifuparade.botcchi.application.configurations

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Configuration

@Tag("unit-tests")
internal class PropertiesScanTest {

    private val propertiesScanAnnotations = PropertiesScan().javaClass.annotations
    private val scanPackageName = "hq.waifuparade.botcchi"

    @Test
    fun checkConfigurationAnnotation() {
        // @Configuration
        val annotation = propertiesScanAnnotations.find { it is Configuration }
        assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Configuration

        assertEquals("", annotation.value)
        assertTrue(annotation.proxyBeanMethods)
    }

    @Test
    fun checkConfigurationPropertiesScanAnnotation() {
        // @ConfigurationPropertiesScan
        val annotation = propertiesScanAnnotations.find { it is ConfigurationPropertiesScan }
        assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as ConfigurationPropertiesScan

        assertEquals(1, annotation.value.size)
        assertTrue(annotation.value[0].endsWith(this.scanPackageName, ignoreCase = false))
    }
}