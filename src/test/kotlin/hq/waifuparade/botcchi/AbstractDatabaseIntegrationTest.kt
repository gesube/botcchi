package hq.waifuparade.botcchi

import jakarta.transaction.Transactional
import org.junit.jupiter.api.Tag


@Transactional
@Tag("database-tests")
internal interface AbstractDatabaseIntegrationTest : AbstractIntegrationTest