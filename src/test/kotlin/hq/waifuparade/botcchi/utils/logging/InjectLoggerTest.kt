package hq.waifuparade.botcchi.utils.logging

import io.github.oshai.kotlinlogging.KLogger
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.lang.reflect.Method
import java.lang.reflect.Modifier

internal class InjectLoggerTest {

    private val injectLoggerAnnotations = InjectLogger::class.java.annotations

    @Test
    fun checkTargetAnnotation() {
        // @Target
        val annotation = injectLoggerAnnotations.find { it is Target }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Target

        Assertions.assertEquals(1, annotation.allowedTargets.size)
        Assertions.assertEquals(AnnotationTarget.CLASS, annotation.allowedTargets[0])
    }

    @Test
    fun checkRetentionAnnotation() {
        // @Target
        val annotation = injectLoggerAnnotations.find { it is Retention }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Retention

        Assertions.assertEquals(AnnotationRetention.RUNTIME, annotation.value)
    }

    @Test
    fun checkTypeOfLogger() {
        val methods = InjectLogger.Companion::class.java.methods
        val getterMethods = methods.filter { method: Method -> method.name == "getLogger" }
        Assertions.assertEquals(1, getterMethods.size)
        val getterMethod = getterMethods[0]
        Assertions.assertEquals(KLogger::class.java.canonicalName, getterMethod.returnType.name)
    }

    @Test
    fun checkLoggerGetterIsPublic() {
        val methods = InjectLogger.Companion::class.java.methods
        val getterMethods = methods.filter { method: Method -> method.name == "getLogger" }
        Assertions.assertEquals(1, getterMethods.size)
        val getterMethod = getterMethods[0]
        val modifiers = getterMethod.modifiers
        Assertions.assertTrue(Modifier.isPublic(modifiers))
    }

    @Test
    fun checkLoggerSetterIsPrivate() {
        val methods = InjectLogger.Companion::class.java.methods
        val setterMethods = methods.filter { method: Method -> method.name == "setLogger" }
        Assertions.assertEquals(0, setterMethods.size)
    }
}