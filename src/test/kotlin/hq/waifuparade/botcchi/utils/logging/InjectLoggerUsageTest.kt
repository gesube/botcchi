package hq.waifuparade.botcchi.utils.logging

import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@InjectLogger
internal class InjectLoggerUsageTest {

    @Test
    fun useLogger_TraceMessageTest() {
        this.logger.trace { "test" }
    }

    @Test
    fun useLogger_DebugMessageTest() {
        this.logger.debug { "test" }
    }

    @Test
    fun useLogger_InfoMessageTest() {
        this.logger.info { "test" }
    }

    @Test
    fun useLogger_WarnMessageTest() {
        this.logger.warn { "test" }
    }

    @Test
    fun useLogger_ErrorMessageTest() {
        this.logger.error { "test" }
    }

    @Test
    fun checkNameOfLoggerIsCorrectylSet() {
        Assertions.assertEquals(this::class.simpleName, this.logger.name)
    }
}