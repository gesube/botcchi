package hq.waifuparade.botcchi.utils

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class JsonMapperStringToObjectMappingTest {

    // --- simple objects ---
    @Test
    fun convertStringToSimpleObject_InlineUsage1() {
        val valuePair = this.createStringToConvertAndExpectedSimpleObject()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedObjectFromString: TestClass = JsonMapper.convertStringToObject(stringToConvert)

        this.assertObjectEquality(expectedObjectFromString, convertedObjectFromString)
    }

    @Test
    fun convertStringToSimpleObject_InlineUsage2() {
        val valuePair = this.createStringToConvertAndExpectedSimpleObject()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedObjectFromString = JsonMapper.convertStringToObject<TestClass>(stringToConvert)

        this.assertObjectEquality(expectedObjectFromString, convertedObjectFromString)
    }

    // --- array of objects ---
    @Test
    fun convertStringToArrayOfObjects_InlineUsage1() {
        val valuePair = this.createStringToConvertAndExpectedListOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedListFromString: Array<TestClass> = JsonMapper.convertStringToObject(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedListFromString.size)

        for (i in convertedListFromString.indices) {
            val expectedObject = expectedObjectFromString[i]
            val actualObject = convertedListFromString[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    @Test
    fun convertStringToArrayOfObjects_InlineUsage2() {
        val valuePair = this.createStringToConvertAndExpectedListOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedListFromString = JsonMapper.convertStringToObject<Array<TestClass>>(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedListFromString.size)

        for (i in convertedListFromString.indices) {
            val expectedObject = expectedObjectFromString[i]
            val actualObject = convertedListFromString[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    // --- list of objects ---
    @Test
    fun convertStringToListOfObjects_InlineUsage1() {
        val valuePair = this.createStringToConvertAndExpectedListOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedListFromString: List<TestClass> = JsonMapper.convertStringToObject(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedListFromString.size)

        for (i in convertedListFromString.indices) {
            val expectedObject = expectedObjectFromString[i]
            val actualObject = convertedListFromString[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    @Test
    fun convertStringToListOfObjects_InlineUsage2() {
        val valuePair = this.createStringToConvertAndExpectedListOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedListFromString = JsonMapper.convertStringToObject<List<TestClass>>(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedListFromString.size)

        for (i in convertedListFromString.indices) {
            val expectedObject = expectedObjectFromString[i]
            val actualObject = convertedListFromString[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    // --- map of objects ---
    @Test
    fun convertStringToMapOfObjects_InlineUsage1() {
        val valuePair = this.createStringToConvertAndExpectedMapOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedObjectMapFromString: Map<String, TestClass> =
            JsonMapper.convertStringToObject(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedObjectMapFromString.size)

        for (key in convertedObjectMapFromString.keys) {
            val expectedObject = expectedObjectFromString[key]
            val actualObject = convertedObjectMapFromString[key]
            this.assertObjectEquality(expectedObject!!, actualObject!!)
        }
    }

    @Test
    fun convertStringToMapOfObjects_InlineUsage2() {
        val valuePair = this.createStringToConvertAndExpectedMapOfObjects()
        val stringToConvert = valuePair.first
        val expectedObjectFromString = valuePair.second

        val convertedObjectMapFromString =
            JsonMapper.convertStringToObject<Map<String, TestClass>>(stringToConvert)

        Assertions.assertEquals(expectedObjectFromString.size, convertedObjectMapFromString.size)

        for (key in convertedObjectMapFromString.keys) {
            val expectedObject = expectedObjectFromString[key]
            val actualObject = convertedObjectMapFromString[key]
            this.assertObjectEquality(expectedObject!!, actualObject!!)
        }
    }

    // -- helper methods ---
    private fun createStringToConvertAndExpectedSimpleObject(): Pair<String, TestClass> {
        val stringToConvert = "{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}"
        val expectedObjectFromString =
            TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)

        return Pair(first = stringToConvert, second = expectedObjectFromString)
    }

    private fun createStringToConvertAndExpectedListOfObjects(): Pair<String, List<TestClass>> {
        val stringToConvert =
            "[{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}," +
                "{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}]"

        val testObject1 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val expectedObjectListFromString = listOf(testObject1, testObject2)

        return Pair(first = stringToConvert, second = expectedObjectListFromString)
    }

    private fun createStringToConvertAndExpectedMapOfObjects(): Pair<String, Map<String, TestClass>> {
        val stringToConvert =
            "{\"testObject1\":{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}," +
                "\"testObject2\":{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}}"

        val testObject1 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val expectedObjectMapFromString = mapOf("testObject1" to testObject1, "testObject2" to testObject2)

        return Pair(first = stringToConvert, second = expectedObjectMapFromString)
    }

    private fun assertObjectEquality(expectedObject: TestClass, actualObject: Any) {
        if (actualObject !is TestClass) {
            throw Exception("The provided actual object is not an instance of TestClass!")
        }

        Assertions.assertEquals(expectedObject.javaClass, actualObject.javaClass)
        Assertions.assertEquals(expectedObject.testStringVar, actualObject.testStringVar)
        Assertions.assertEquals(expectedObject.testIntegerVar, actualObject.testIntegerVar)
        Assertions.assertEquals(expectedObject.testBooleanVar, actualObject.testBooleanVar)
    }

    private data class TestClass(
        val testStringVar: String,
        val testIntegerVar: Int,
        val testBooleanVar: Boolean
                                )
}