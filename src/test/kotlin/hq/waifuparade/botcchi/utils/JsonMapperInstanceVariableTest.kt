package hq.waifuparade.botcchi.utils

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

internal class JsonMapperInstanceVariableTest {

    @Test
    fun instanceHasKotlinModuleRegistered() {
        val instanceField = JsonMapper::class.java.getDeclaredField("instance")
        instanceField.isAccessible = true
        val instance = instanceField.get(JsonMapper) as ObjectMapper

        instance.registeredModuleIds.forEach {
            if (it is String) {
                assertTrue(it.contains("module.kotlin.KotlinModule"))
            }
        }

        instanceField.isAccessible = false
    }
}