package hq.waifuparade.botcchi.utils

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class JsonMapperObjectToStringMappingTest {

    @Test
    fun convertSimpleObjectToString() {
        val valuePair = this.createObjectToConvertAndExpectedString()
        val objectToConvert = valuePair.first
        val expectedString = valuePair.second

        val convertedStringFromObject = JsonMapper.convertObjectToString(objectToConvert)
        assertEquals(expectedString, convertedStringFromObject)
    }

    @Test
    fun convertListOfObjectsToString() {
        val valuePair = this.createObjectListToConvertAndExpectedString()
        val objectsToConvert = valuePair.first
        val expectedString = valuePair.second

        val convertedStringFromObjectList = JsonMapper.convertObjectToString(objectsToConvert)
        assertEquals(expectedString, convertedStringFromObjectList)
    }

    @Test
    fun convertMapOfObjectsToString() {
        val valuePair = this.createObjectMapToConvertAndExpectedString()
        val objectsToConvert = valuePair.first
        val expectedString = valuePair.second

        val convertedStringFromObjectMap = JsonMapper.convertObjectToString(objectsToConvert)
        assertEquals(expectedString, convertedStringFromObjectMap)
    }

    // -- helper methods ---
    private fun createObjectToConvertAndExpectedString(): Pair<TestClass, String> {
        val objectToConvert = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val expectedStringFromObject =
            "{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}"

        return Pair(first = objectToConvert, second = expectedStringFromObject)
    }

    private fun createObjectListToConvertAndExpectedString(): Pair<List<TestClass>, String> {
        val testObject1 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObjects = listOf(testObject1, testObject2)

        val expectedStringFromObjectList =
            "[{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}," +
                "{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}]"

        return Pair(first = testObjects, second = expectedStringFromObjectList)
    }

    private fun createObjectMapToConvertAndExpectedString(): Pair<Map<String, TestClass>, String> {
        val testObject1 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TestClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObjects = mapOf("testObject1" to testObject1, "testObject2" to testObject2)

        val expectedStringFromObjectMap =
            "{\"testObject1\":{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}," +
                "\"testObject2\":{\"testStringVar\":\"test\",\"testIntegerVar\":1,\"testBooleanVar\":true}}"

        return Pair(first = testObjects, second = expectedStringFromObjectMap)
    }

    private data class TestClass(
        val testStringVar: String,
        val testIntegerVar: Int,
        val testBooleanVar: Boolean
                                )
}