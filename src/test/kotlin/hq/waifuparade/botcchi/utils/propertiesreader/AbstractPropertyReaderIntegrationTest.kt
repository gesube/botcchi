package hq.waifuparade.botcchi.utils.propertiesreader

import hq.waifuparade.botcchi.AbstractIntegrationTest
import hq.waifuparade.botcchi.utils.JsonMapper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.test.context.ContextConfiguration

@ContextConfiguration(
    classes = [
        AbstractIntegrationTest.DefaultStartupTestConfig::class,
        AbstractIntegrationTest.DefaultShutdownTestConfig::class
    ]
                     )
internal abstract class AbstractPropertyReaderIntegrationTest(
    private val propertyReader: AbstractPropertiesReader
                                                             ) : AbstractIntegrationTest {

    private val annotations = this.propertyReader.javaClass.annotations

    abstract fun getResponsibilityPrefixValue(): String

    fun getValuesOfPropertyReader(): Map<String, Any> {
        return JsonMapper.convertObjectToObject<Map<String, Any>>(this.propertyReader).filter { entry ->
            entry.key.startsWith(this.getResponsibilityPrefixValue())
        }
    }

    @Test
    fun hasConfigurationPropertiesAnnotation() {
        val annotation = this.annotations.find { it is ConfigurationProperties }
        Assertions.assertNotNull(annotation)
        Assertions.assertEquals(
            annotation?.annotationClass?.simpleName,
            ConfigurationProperties::class.java.simpleName
                               )

        Assertions.assertEquals(
            this.getResponsibilityPrefixValue(),
            (annotation as ConfigurationProperties).value
                               )
    }

    @Test
    fun checkReaderCanReadFromFile() {
        this.getValuesOfPropertyReader().forEach { (_, value) ->
            when (value) {
                is String -> {
                    Assertions.assertNotEquals("", value)
                }
                is Number -> {
                    Assertions.assertNotEquals(Long.MIN_VALUE, value)
                }
            }
        }
    }
}