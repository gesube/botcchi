package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertyReaderIntegrationTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.env.AbstractEnvironment
import org.springframework.core.env.EnumerablePropertySource
import org.springframework.core.env.Environment
import org.springframework.core.io.support.ResourcePropertySource
import java.util.*
import java.util.stream.StreamSupport

@ComponentScan(basePackageClasses = [ChatDirectUserInteractionsPropertiesReader::class])
internal class ChatDirectUserInteractionsPropertiesReaderTest(
    @Autowired private val propertiesReader: ChatDirectUserInteractionsPropertiesReader,
    @Autowired private val env: Environment
                                                             ) :
    AbstractPropertyReaderIntegrationTest(
        propertiesReader
                                         ) {

    override fun getResponsibilityPrefixValue(): String {
        return "chat.direct-user-interaction"
    }

    @Test
    fun checkReaderDoesNotChangeGivenString() {
        val testProperties = this.readTestPropertiesFromEnvironment()

        this.getValuesOfPropertyReader().forEach { (key, value) ->
            val testPropertiesKey =
                "${this.getResponsibilityPrefixValue()}.${
                    key.replace(Regex("([A-Z])"), "-\$1").lowercase()
                }"
            Assertions.assertEquals(testProperties[testPropertiesKey], value)
        }
    }

    private fun readTestPropertiesFromEnvironment(): Map<String, Any?> {
        val sources = (this.env as AbstractEnvironment).propertySources
        val testProperties = mutableMapOf<String, Any?>()

        StreamSupport.stream(sources.spliterator(), false)
            .filter { propertySource -> propertySource is ResourcePropertySource }
            .map { propertySource -> (propertySource as EnumerablePropertySource<*>).propertyNames }
            .flatMap(Arrays::stream)
            .sorted()
            .forEach { property -> testProperties[property] = this.env.getProperty(property) }

        return testProperties
    }
}