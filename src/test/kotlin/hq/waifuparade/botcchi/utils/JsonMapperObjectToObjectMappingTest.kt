package hq.waifuparade.botcchi.utils

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class JsonMapperObjectToObjectMappingTest {

    // --- simple objects ---
    @Test
    fun convertStringToSimpleObject_InlineUsage1() {
        val valuePair = this.createObjectToConvertAndExpectedSimpleObject()
        val objectToConvert = valuePair.first
        val expectedObject = valuePair.second

        val convertedObject: TargetClass = JsonMapper.convertObjectToObject(objectToConvert)

        this.assertObjectEquality(expectedObject, convertedObject)
    }

    @Test
    fun convertStringToSimpleObject_InlineUsage2() {
        val valuePair = this.createObjectToConvertAndExpectedSimpleObject()
        val objectToConvert = valuePair.first
        val expectedObject = valuePair.second

        val convertedObject = JsonMapper.convertObjectToObject<TargetClass>(objectToConvert)

        this.assertObjectEquality(expectedObject, convertedObject)
    }

    // --- array of objects ---
    @Test
    fun convertStringToArrayOfObjects_InlineUsage1() {
        val valuePair = this.createObjectListToConvertAndExpectedListOfObjects()
        val objectListToConvert = valuePair.first
        val expectedObjectList = valuePair.second

        val convertedList: Array<TargetClass> = JsonMapper.convertObjectToObject(objectListToConvert)

        Assertions.assertEquals(expectedObjectList.size, convertedList.size)

        for (i in convertedList.indices) {
            val expectedObject = expectedObjectList[i]
            val actualObject = convertedList[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    @Test
    fun convertStringToArrayOfObjects_InlineUsage2() {
        val valuePair = this.createObjectListToConvertAndExpectedListOfObjects()
        val objectListToConvert = valuePair.first
        val expectedObjectList = valuePair.second

        val convertedList = JsonMapper.convertObjectToObject<Array<TargetClass>>(objectListToConvert)

        Assertions.assertEquals(expectedObjectList.size, convertedList.size)

        for (i in convertedList.indices) {
            val expectedObject = expectedObjectList[i]
            val actualObject = convertedList[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    // --- list of objects ---
    @Test
    fun convertStringToListOfObjects_InlineUsage1() {
        val valuePair = this.createObjectListToConvertAndExpectedListOfObjects()
        val objectListToConvert = valuePair.first
        val expectedObjectList = valuePair.second

        val convertedList: List<TargetClass> = JsonMapper.convertObjectToObject(objectListToConvert)

        Assertions.assertEquals(expectedObjectList.size, convertedList.size)

        for (i in convertedList.indices) {
            val expectedObject = expectedObjectList[i]
            val actualObject = convertedList[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    @Test
    fun convertStringToListOfObjects_InlineUsage2() {
        val valuePair = this.createObjectListToConvertAndExpectedListOfObjects()
        val objectListToConvert = valuePair.first
        val expectedObjectList = valuePair.second

        val convertedList = JsonMapper.convertObjectToObject<List<TargetClass>>(objectListToConvert)

        Assertions.assertEquals(expectedObjectList.size, convertedList.size)

        for (i in convertedList.indices) {
            val expectedObject = expectedObjectList[i]
            val actualObject = convertedList[i]
            this.assertObjectEquality(expectedObject, actualObject)
        }
    }

    // --- map of objects ---
    @Test
    fun convertStringToMapOfObjects_InlineUsage1() {
        val valuePair = this.createObjectMapToConvertAndExpectedMapOfObjects()
        val objectMapToConvert = valuePair.first
        val expectedObjectMap = valuePair.second

        val convertedMap: Map<String, TargetClass> = JsonMapper.convertObjectToObject(objectMapToConvert)

        Assertions.assertEquals(expectedObjectMap.size, convertedMap.size)

        for (key in convertedMap.keys) {
            val expectedObject = expectedObjectMap[key]
            val actualObject = convertedMap[key]
            this.assertObjectEquality(expectedObject!!, actualObject!!)
        }
    }

    @Test
    fun convertStringToMapOfObjects_InlineUsage2() {
        val valuePair = this.createObjectMapToConvertAndExpectedMapOfObjects()
        val objectMapToConvert = valuePair.first
        val expectedObjectMap = valuePair.second

        val convertedMap = JsonMapper.convertObjectToObject<Map<String, TargetClass>>(objectMapToConvert)

        Assertions.assertEquals(expectedObjectMap.size, convertedMap.size)

        for (key in convertedMap.keys) {
            val expectedObject = expectedObjectMap[key]
            val actualObject = convertedMap[key]
            this.assertObjectEquality(expectedObject!!, actualObject!!)
        }
    }

    // -- helper methods ---
    private fun createObjectToConvertAndExpectedSimpleObject(): Pair<SourceClass, TargetClass> {
        val testObject1 = SourceClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TargetClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)

        return Pair(first = testObject1, second = testObject2)
    }

    private fun createObjectListToConvertAndExpectedListOfObjects(): Pair<List<SourceClass>, List<TargetClass>> {
        val testObject1 = SourceClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TargetClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)

        return Pair(first = listOf(testObject1), second = listOf(testObject2))
    }

    private fun createObjectMapToConvertAndExpectedMapOfObjects(): Pair<Map<String, SourceClass>, Map<String, TargetClass>> {
        val testObject1 = SourceClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)
        val testObject2 = TargetClass(testStringVar = "test", testIntegerVar = 1, testBooleanVar = true)

        return Pair(first = mapOf("testObject1" to testObject1), second = mapOf("testObject1" to testObject2))
    }

    private fun assertObjectEquality(expectedObject: TargetClass, actualObject: TargetClass) {
        Assertions.assertEquals(expectedObject.javaClass, actualObject.javaClass)
        Assertions.assertEquals(expectedObject.testStringVar, actualObject.testStringVar)
        Assertions.assertEquals(expectedObject.testIntegerVar, actualObject.testIntegerVar)
        Assertions.assertEquals(expectedObject.testBooleanVar, actualObject.testBooleanVar)
    }

    private data class SourceClass(
        val testStringVar: String,
        val testIntegerVar: Int,
        val testBooleanVar: Boolean
                                  )

    private data class TargetClass(
        val testStringVar: String,
        val testIntegerVar: Int,
        val testBooleanVar: Boolean
                                  )
}