package hq.waifuparade.botcchi.utils

import hq.waifuparade.botcchi.TestConstants
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.net.UnknownHostException
import kotlin.test.assertFailsWith

internal class HttpConnectorTest {

    @Test
    fun doGetRequestSuccess_HttpScheme() {
        val response = HttpConnector.doGetRequest(TestConstants.VALID_HTTP_URL)
        assertEquals(true, response.isSuccessful)
        assertNotNull(response.body)
    }

    @Test
    fun doGetRequestSuccess_HttpsScheme() {
        val response = HttpConnector.doGetRequest(TestConstants.VALID_HTTPS_URL)
        assertEquals(true, response.isSuccessful)
        assertNotNull(response.body)
    }

    @Test
    fun doGetRequestSuccess_MalformedUrl() {
        val response = HttpConnector.doGetRequest(TestConstants.INVALID_HTTPS_URL_MALFORMED)
        assertEquals(true, response.isSuccessful)
        assertNotNull(response.body)
    }

    @Test
    fun doGetRequestSuccess_MissingProtocol() {
        val response = HttpConnector.doGetRequest(TestConstants.INVALID_HTTPS_URL_NO_PROTOCOL)
        assertEquals(true, response.isSuccessful)
        assertNotNull(response.body)
    }

    @Test
    fun doGetRequestFail_NonExistingServer() {
        assertFailsWith<UnknownHostException> {
            val response = HttpConnector.doGetRequest(TestConstants.INVALID_HTTPS_URL_NON_EXISTING)
            assertEquals(true, response.isSuccessful)
            assertNotNull(response.body)
        }
    }

    @Test
    fun doGetRequestFail_MissingUrlSchema() {
        assertFailsWith<IllegalArgumentException> {
            val response = HttpConnector.doGetRequest(TestConstants.INVALID_HTTPS_URL_NO_SCHEMA)
            assertEquals(true, response.isSuccessful)
            assertNotNull(response.body)
        }
    }
}