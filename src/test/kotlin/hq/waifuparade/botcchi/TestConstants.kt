package hq.waifuparade.botcchi

@Suppress("HttpUrlsUsage")
internal class TestConstants {

    companion object {

        const val MAIN_PACKAGE = "hq.waifuparade.botcchi"
        const val VALID_ROOM_NAME = "v4c"
        const val VALID_HTTP_URL = "http://www.example.org"
        const val VALID_HTTPS_URL = "https://www.example.org"
        const val INVALID_HTTPS_URL_NON_EXISTING = "https://www.non-existing-test-server.com"
        const val INVALID_HTTPS_URL_MALFORMED = "https:/www.example.org"
        const val INVALID_HTTPS_URL_NO_SCHEMA = "www.example.org"
        const val INVALID_HTTPS_URL_NO_PROTOCOL = "https://example.org"
    }
}