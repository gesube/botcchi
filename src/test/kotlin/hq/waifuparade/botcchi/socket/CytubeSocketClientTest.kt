package hq.waifuparade.botcchi.socket

import hq.waifuparade.botcchi.events.ApplicationEventsApi
import hq.waifuparade.botcchi.socket.dtos.SocketServer
import hq.waifuparade.botcchi.socket.services.CytubeSocketClientCreator
import hq.waifuparade.botcchi.socket.services.CytubeSocketServerRetriever
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONObject
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils
import java.lang.reflect.Constructor

@Tag("unit-tests")
internal class CytubeSocketClientTest {

    private lateinit var instance: CytubeSocketClient
    private lateinit var primaryConstructor: Constructor<CytubeSocketClient>
    private lateinit var cytubeSocketServerRetriever: CytubeSocketServerRetriever
    private lateinit var cytubeSocketClientCreator: CytubeSocketClientCreator
    private lateinit var applicationEventsApi: ApplicationEventsApi

    private lateinit var underlyingSocketClient: Socket
    private lateinit var emitter: Emitter

    @BeforeEach
    fun setUp() {
        BeanUtils.findPrimaryConstructor(CytubeSocketClient::class.java)?.let {
            this.primaryConstructor = it
        }

        this.cytubeSocketServerRetriever = mockk()
        this.cytubeSocketClientCreator = mockk()
        this.applicationEventsApi = mockk()

        this.emitter = mockk()
        this.underlyingSocketClient = mockk()
        every { underlyingSocketClient.connected() }.returns(false)
        every { underlyingSocketClient.connect() }.returns(underlyingSocketClient)
        every { underlyingSocketClient.disconnect() }.returns(underlyingSocketClient)
        every { underlyingSocketClient.emit(any(), any()) }.returns(this.emitter)
        every { underlyingSocketClient.on(any(), any()) }.returns(this.emitter)
        every { underlyingSocketClient.once(any(), any()) }.returns(this.emitter)
        every { underlyingSocketClient.off(any(), any()) }.returns(this.emitter)
        every { underlyingSocketClient.off(any()) }.returns(this.emitter)
        every { underlyingSocketClient.off() }.returns(this.emitter)

        this.instance = this.buildDefaultSocketClientInstance()
    }

    private fun buildDefaultSocketClientInstance(): CytubeSocketClient {
        val socketServer = SocketServer(url = "test", secure = true)
        val socketServers = listOf(socketServer)
        every { cytubeSocketServerRetriever.retrieveSocketServersForRoom() }.returns(socketServers)
        every { cytubeSocketClientCreator.createSocketClient(socketServers) }.returns(this.underlyingSocketClient)
        every { applicationEventsApi.publishEvent(any()) } returns Unit

        return BeanUtils.instantiateClass(
            this.primaryConstructor,
            this.cytubeSocketServerRetriever,
            this.cytubeSocketClientCreator,
            this.applicationEventsApi
                                         )
    }

    @Test
    fun connectToSocketServer_Success() {
        // check the client is only created if there is none already
        every { underlyingSocketClient.connected() }.returns(false)

        verify(exactly = 0) { cytubeSocketServerRetriever.retrieveSocketServersForRoom() }
        verify(exactly = 0) { cytubeSocketClientCreator.createSocketClient(any()) }
        verify(exactly = 0) { underlyingSocketClient.connect() }
        this.instance.connectToSocketServer()
        verify(exactly = 1) { cytubeSocketServerRetriever.retrieveSocketServersForRoom() }
        verify(exactly = 1) { cytubeSocketClientCreator.createSocketClient(any()) }
        verify(exactly = 1) { underlyingSocketClient.connect() }
    }

    @Test
    fun connectToSocketServer_AlreadyConnected() {
        // check the client is not created again if there is one instance already
        every { underlyingSocketClient.connected() }.returns(true)

        verify(exactly = 0) { cytubeSocketServerRetriever.retrieveSocketServersForRoom() }
        verify(exactly = 0) { cytubeSocketClientCreator.createSocketClient(any()) }
        verify(exactly = 0) { underlyingSocketClient.connect() }
        this.instance.connectToSocketServer()
        verify(exactly = 1) { cytubeSocketServerRetriever.retrieveSocketServersForRoom() }
        verify(exactly = 1) { cytubeSocketClientCreator.createSocketClient(any()) }
        verify(exactly = 0) { underlyingSocketClient.connect() }
    }

    @Test
    fun disconnectFromSocketServer() {
        // check the client is only disconnected if there is one
        every { underlyingSocketClient.connected() }.returns(false)
        verify(exactly = 0) { underlyingSocketClient.disconnect() }

        this.instance.disconnectFromSocketServer()
        verify(exactly = 0) { underlyingSocketClient.disconnect() }

        this.instance.connectToSocketServer()
        this.instance.disconnectFromSocketServer()
        verify(exactly = 1) { underlyingSocketClient.disconnect() }

        this.instance.disconnectFromSocketServer()
        verify(exactly = 1) { underlyingSocketClient.disconnect() }

        this.instance.connectToSocketServer()
        this.instance.disconnectFromSocketServer()
        verify(exactly = 2) { underlyingSocketClient.disconnect() }
    }

    @Test
    fun setListenerOn() {
        verify(exactly = 0) { underlyingSocketClient.on(any(), any()) }

        this.instance.connectToSocketServer()
        this.instance.on("") { eventData ->
            eventData.size
        }

        verify(exactly = 1) { underlyingSocketClient.on(any(), any()) }
    }

    @Test
    fun setListenerOnce() {
        verify(exactly = 0) { underlyingSocketClient.once(any(), any()) }

        this.instance.connectToSocketServer()
        this.instance.once("") { eventData ->
            eventData.size
        }

        verify(exactly = 1) { underlyingSocketClient.once(any(), any()) }
    }

    @Test
    fun clearListenerForEvent_WithoutListener() {
        verify(exactly = 0) { underlyingSocketClient.off(any(), any()) }

        this.instance.connectToSocketServer()
        this.instance.off("")

        verify(exactly = 1) { underlyingSocketClient.off(any()) }
    }

    @Test
    fun clearListenerForEvent_WithListener() {
        verify(exactly = 0) { underlyingSocketClient.off(any(), any()) }

        this.instance.connectToSocketServer()
        this.instance.off("") { eventData ->
            eventData.size
        }

        verify(exactly = 1) { underlyingSocketClient.off(any(), any()) }
    }

    @Test
    fun clearAllListener() {
        verify(exactly = 0) { underlyingSocketClient.off() }

        this.instance.connectToSocketServer()
        this.instance.off()

        verify(exactly = 1) { underlyingSocketClient.off() }
    }

    @Test
    fun emitEvent() {
        verify(exactly = 0) { underlyingSocketClient.emit(any(), any()) }

        this.instance.connectToSocketServer()
        this.instance.emit("", JSONObject())

        verify(exactly = 1) { underlyingSocketClient.emit(any(), any()) }
    }
}