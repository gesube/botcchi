package hq.waifuparade.botcchi.socket.services

import hq.waifuparade.botcchi.socket.dtos.SocketServer
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.socket.client.IO
import io.socket.client.Socket
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.net.URI
import kotlin.test.assertNotNull

@Tag("unit-tests")
internal class CytubeSocketClientCreatorTest {

    @BeforeEach
    fun setUp() {
        val socketMock = mockk<Socket>()
        mockkStatic(IO::class)

        every { IO.socket(any<String>()) } returns socketMock
        every { IO.socket(any<String>(), any<IO.Options>()) } returns socketMock
        every { IO.socket(any<URI>()) } returns socketMock
        every { IO.socket(any<URI>(), any<IO.Options>()) } returns socketMock
    }

    @Test
    fun createSocketClient_ReturnsSocket() {
        val cytubeSocketClientCreator = CytubeSocketClientCreator()

        val socketServer = SocketServer(url = "test", secure = true)
        val socketServerList = listOf(socketServer)

        val socket = cytubeSocketClientCreator.createSocketClient(socketServerList)
        assertNotNull(socket)
    }

    @Test
    fun createSocketClient_ReturnsNull() {
        val cytubeSocketClientCreator = CytubeSocketClientCreator()

        val socketServer = SocketServer(url = "test", secure = false)
        val socketServerList = listOf(socketServer)

        val socket = cytubeSocketClientCreator.createSocketClient(socketServerList)
        assertNull(socket)
    }
}