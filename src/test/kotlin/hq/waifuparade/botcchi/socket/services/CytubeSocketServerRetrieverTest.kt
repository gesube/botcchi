package hq.waifuparade.botcchi.socket.services

import hq.waifuparade.botcchi.TestConstants
import hq.waifuparade.botcchi.socket.dtos.SocketServer
import hq.waifuparade.botcchi.socket.dtos.SocketServerListWrapper
import hq.waifuparade.botcchi.utils.HttpConnector
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class CytubeSocketServerRetrieverTest {

    private lateinit var cytubePropertiesReader: CytubePropertiesReader

    @BeforeEach
    fun setUp() {
        this.cytubePropertiesReader = mockk()
        mockkObject(HttpConnector)
    }

    @Test
    fun checkIfServerUrlIsBuiltCorrectly_SocketServerListUrl_Wellformed() {
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns ""

        every { HttpConnector.doGetRequest(TestConstants.VALID_HTTPS_URL) } returns this.buildTestResponse_NotSuccessful()

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        cytubeSocketServerRetriever.retrieveSocketServersForRoom()
    }

    @Test
    fun checkIfServerUrlIsBuiltCorrectly_SocketServerListUrl_Malformed() {
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns "${TestConstants.VALID_HTTPS_URL}//socketconfig"
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns ""

        every { HttpConnector.doGetRequest(TestConstants.VALID_HTTPS_URL) } returns this.buildTestResponse_NotSuccessful()

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        cytubeSocketServerRetriever.retrieveSocketServersForRoom()
    }

    @Test
    fun checkIfServerUrlIsBuiltCorrectly_ServerUrl_Wellformed() {
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns ""

        val selfBuiltUrl =
            "${TestConstants.VALID_HTTPS_URL}/socketconfig/${TestConstants.VALID_ROOM_NAME}.json"
        every { HttpConnector.doGetRequest(selfBuiltUrl) } returns this.buildTestResponse_NotSuccessful()

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        cytubeSocketServerRetriever.retrieveSocketServersForRoom()
    }

    @Test
    fun checkIfServerUrlIsBuiltCorrectly_ServerUrl_Malformed() {
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns "${TestConstants.VALID_HTTPS_URL}/"
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns TestConstants.VALID_ROOM_NAME

        val selfBuiltUrl =
            "${TestConstants.VALID_HTTPS_URL}/socketconfig/${TestConstants.VALID_ROOM_NAME}.json"
        every { HttpConnector.doGetRequest(selfBuiltUrl) } returns this.buildTestResponse_NotSuccessful()

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        cytubeSocketServerRetriever.retrieveSocketServersForRoom()
    }

    @Test
    fun retrieveSocketServersForRoom_NotSuccessful() {
        every { HttpConnector.doGetRequest(any()) } returns this.buildTestResponse_NotSuccessful()

        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns ""

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        val socketServers = cytubeSocketServerRetriever.retrieveSocketServersForRoom()
        assertEquals(0, socketServers.size)
    }

    @Test
    fun retrieveSocketServersForRoom_Successful_NoResponseBody() {
        val response = mockk<Response>()
        every { response.code } returns 200
        every { response.message } returns "true"
        every { response.isSuccessful } returns true
        every { response.body } returns null

        every { HttpConnector.doGetRequest(any()) } returns response

        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns ""

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        val socketServers = cytubeSocketServerRetriever.retrieveSocketServersForRoom()
        assertEquals(0, socketServers.size)
    }

    @Test
    fun retrieveSocketServersForRoom_Successful_WithoutData() {
        val response = mockk<Response>()
        every { response.code } returns 200
        every { response.message } returns "true"
        every { response.isSuccessful } returns true
        every { response.body } returns "".toResponseBody("application/json; charset=utf8".toMediaType())

        every { HttpConnector.doGetRequest(any()) } returns response

        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns TestConstants.VALID_ROOM_NAME

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        val socketServers = cytubeSocketServerRetriever.retrieveSocketServersForRoom()
        assertEquals(0, socketServers.size)
    }

    @Test
    fun retrieveSocketServersForRoom_Successful_WithData() {
        val socketServer1 = SocketServer(url = TestConstants.VALID_HTTPS_URL, secure = true)
        val socketServer2 = SocketServer(url = TestConstants.VALID_HTTP_URL, secure = false)
        val socketServerListWrapper = SocketServerListWrapper(servers = listOf(socketServer1, socketServer2))

        val body = mockk<ResponseBody>()
        every { body.string() } returns JsonMapper.convertObjectToString(socketServerListWrapper)

        val response = mockk<Response>()
        every { response.code } returns 200
        every { response.message } returns "true"
        every { response.isSuccessful } returns true
        every { response.body } returns body

        every { HttpConnector.doGetRequest(any()) } returns response

        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.socketServerListUrl } returns ""
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.server.url } returns TestConstants.VALID_HTTPS_URL
        every { this@CytubeSocketServerRetrieverTest.cytubePropertiesReader.room.name } returns TestConstants.VALID_ROOM_NAME

        val cytubeSocketServerRetriever = CytubeSocketServerRetriever(this.cytubePropertiesReader)
        val socketServers = cytubeSocketServerRetriever.retrieveSocketServersForRoom()
        assertEquals(2, socketServers.size)
    }

    private fun buildTestResponse_NotSuccessful(): Response {
        val response = mockk<Response>()
        every { response.code } returns 503
        every { response.message } returns "false"
        every { response.isSuccessful } returns false
        return response
    }
}