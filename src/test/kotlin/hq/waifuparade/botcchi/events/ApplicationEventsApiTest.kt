package hq.waifuparade.botcchi.events

import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.events.services.EventPublisher
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.RepetitionInfo
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.BeanUtils

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@Tag("unit-tests")
internal class ApplicationEventsApiTest {

    private lateinit var api: ApplicationEventsApi
    private lateinit var publisherMock: EventPublisher

    @BeforeAll
    fun setUp() {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(ApplicationEventsApi::class.java)
        this.publisherMock = mockk()
        every { this@ApplicationEventsApiTest.publisherMock.publishEvent(any()) } returns Unit
        verify(exactly = 0) { this@ApplicationEventsApiTest.publisherMock.publishEvent(any()) }

        primaryConstructor?.let {
            this.api = BeanUtils.instantiateClass(it, this@ApplicationEventsApiTest.publisherMock)
        }

        Assertions.assertNotNull(this.api)
    }

    @RepeatedTest(value = 100)
    fun publishEvent(repetitionInfo: RepetitionInfo) {
        this.api.publishEvent(ApplicationEventType.entries.random())
        verify(exactly = repetitionInfo.currentRepetition) {
            this@ApplicationEventsApiTest.publisherMock.publishEvent(
                any()
                                                                    )
        }
    }
}