package hq.waifuparade.botcchi.events.services

import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.events.models.ApplicationEvent
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.RepetitionInfo
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.BeanUtils
import org.springframework.context.ApplicationEventPublisher

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@Tag("unit-tests")
internal class EventPublisherTest {

    private lateinit var service: EventPublisher
    private lateinit var publisherMock: ApplicationEventPublisher

    @BeforeAll
    fun setUp() {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(EventPublisher::class.java)
        this.publisherMock = mockk()
        every { this@EventPublisherTest.publisherMock.publishEvent(any<ApplicationEvent>()) } returns Unit
        verify(exactly = 0) { this@EventPublisherTest.publisherMock.publishEvent(any<ApplicationEvent>()) }

        primaryConstructor?.let {
            this.service = BeanUtils.instantiateClass(it, this@EventPublisherTest.publisherMock)
        }

        Assertions.assertNotNull(this.service)
    }

    @RepeatedTest(value = 100)
    fun publishEvent(repetitionInfo: RepetitionInfo) {
        this.service.publishEvent(ApplicationEventType.entries.random())
        verify(exactly = repetitionInfo.currentRepetition) {
            this@EventPublisherTest.publisherMock.publishEvent(
                any<ApplicationEvent>()
                                                              )
        }
    }
}