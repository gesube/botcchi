package hq.waifuparade.botcchi

import hq.waifuparade.botcchi.application.ApplicationMain
import hq.waifuparade.botcchi.application.ApplicationShutdown
import hq.waifuparade.botcchi.application.ApplicationStartup
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Tag
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.TestPropertySource

@SpringBootTest(classes = [ApplicationMain::class])
@TestPropertySource(value = ["/application-test.properties"])
@Tag("integration-tests")
internal interface AbstractIntegrationTest {

    @TestConfiguration
    class DefaultStartupTestConfig {

        @Bean
        fun applicationStartup(): ApplicationStartup {
            val mock = mockk<ApplicationStartup>()
            every { mock.handleContextRefresh(any()) } answers { }
            return mock
        }
    }

    @TestConfiguration
    class DefaultShutdownTestConfig {

        @Bean
        fun applicationShutdown(): ApplicationShutdown {
            val mock = mockk<ApplicationShutdown>()
            every { mock.handleContextClose(any()) } answers { }
            return mock
        }
    }
}