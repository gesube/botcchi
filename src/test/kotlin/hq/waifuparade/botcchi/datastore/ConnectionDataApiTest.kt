package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.services.ConnectionDataHandler
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Tag("unit-tests")
internal class ConnectionDataApiTest {

    private lateinit var api: ConnectionDataApi

    @BeforeEach
    fun setUp() = runBlocking<Unit> {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(ConnectionDataApi::class.java)
        val dataHandler = mockk<ConnectionDataHandler>()
        coEvery { dataHandler.setLoginTime(any()) } answers { 0 }
        coEvery { dataHandler.getLoginTime() } answers { 0 }
        coEvery { dataHandler.removeLoginTime() } answers { 0 }
        coEvery { dataHandler.setPlaylistOpen(any()) } answers { true }
        coEvery { dataHandler.isPlaylistOpen() } answers { true }

        primaryConstructor?.let {
            this@ConnectionDataApiTest.api = BeanUtils.instantiateClass(it, dataHandler)
        }
    }

    @Test
    fun setLoginTime() = runBlocking {
        val returnedValue = this@ConnectionDataApiTest.api.setLoginTime(Long.MAX_VALUE)
        assertEquals(0, returnedValue)
    }

    @Test
    fun getLoginTime() = runBlocking {
        val returnedValue = this@ConnectionDataApiTest.api.getLoginTime()
        assertEquals(0, returnedValue)
    }

    @Test
    fun removeLoginTime() = runBlocking {
        val returnedValue = this@ConnectionDataApiTest.api.removeLoginTime()
        assertEquals(0, returnedValue)
    }

    @Test
    fun setPlaylistOpen() = runBlocking {
        val returnedValue = this@ConnectionDataApiTest.api.setPlaylistOpen(true)
        assertTrue(returnedValue)
    }

    @Test
    fun isPlaylistOpen() = runBlocking {
        val returnedValue = this@ConnectionDataApiTest.api.isPlaylistOpen()
        assertTrue(returnedValue)
    }
}