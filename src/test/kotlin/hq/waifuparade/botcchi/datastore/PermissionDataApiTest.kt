package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import hq.waifuparade.botcchi.datastore.services.PermissionDataHandler
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@Tag("unit-tests")
internal class PermissionDataApiTest {

    private lateinit var api: PermissionDataApi
    private val dto = PermissionDto(name = "test", minRequiredRank = Double.MIN_VALUE)

    @BeforeEach
    fun setUp() = runBlocking<Unit> {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(PermissionDataApi::class.java)
        val dataHandler = mockk<PermissionDataHandler>()
        coEvery { dataHandler.saveAll(any()) } answers { listOf(this@PermissionDataApiTest.dto) }
        coEvery { dataHandler.save(any()) } answers { this@PermissionDataApiTest.dto }
        coEvery { dataHandler.removeAll() } answers { true }
        coEvery { dataHandler.remove(any()) } answers { true }
        coEvery { dataHandler.updateByName(any()) } answers { this@PermissionDataApiTest.dto }
        coEvery { dataHandler.removeByName(any()) } answers { true }
        coEvery { dataHandler.findByName(any()) } answers { this@PermissionDataApiTest.dto }
        coEvery { dataHandler.findByRelatedAction(any()) } answers { listOf(this@PermissionDataApiTest.dto) }

        primaryConstructor?.let {
            this@PermissionDataApiTest.api = BeanUtils.instantiateClass(it, dataHandler)
        }
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@PermissionDataApiTest.api.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.saveAll(listOf(this@PermissionDataApiTest.dto))
        assertEquals(1, returnedValue.size)
        this@PermissionDataApiTest.assertDtoEquality(this@PermissionDataApiTest.dto, returnedValue[0])
    }

    @Test
    fun save() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.save(this@PermissionDataApiTest.dto)
        assertNotNull(returnedValue)
        this@PermissionDataApiTest.assertDtoEquality(this@PermissionDataApiTest.dto, returnedValue)
    }

    @Test
    fun remove() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.remove(this@PermissionDataApiTest.dto)
        assertTrue(returnedValue)
    }

    @Test
    fun removeAll() = runBlocking {
        this@PermissionDataApiTest.api.removeAll()
    }

    @Test
    fun updateByName() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.updateByName(this@PermissionDataApiTest.dto)
        this@PermissionDataApiTest.assertDtoEquality(this@PermissionDataApiTest.dto, returnedValue)
    }

    @Test
    fun removeByName() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.removeByName(this@PermissionDataApiTest.dto.name)
        assertTrue(returnedValue)
    }

    @Test
    fun findByName() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.findByName(this@PermissionDataApiTest.dto.name)
        assertNotNull(returnedValue)
        this@PermissionDataApiTest.assertDtoEquality(this@PermissionDataApiTest.dto, returnedValue)
    }

    @Test
    fun findByRelatedAction() = runBlocking {
        val returnedValue = this@PermissionDataApiTest.api.findByRelatedAction(ApplicationAction.ADD_MEDIA)
        assertEquals(1, returnedValue.size)
        this@PermissionDataApiTest.assertDtoEquality(this@PermissionDataApiTest.dto, returnedValue[0])
    }

    private fun assertDtoEquality(expectedDto: PermissionDto, actualDto: PermissionDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.minRequiredRank, actualDto.minRequiredRank)
    }
}