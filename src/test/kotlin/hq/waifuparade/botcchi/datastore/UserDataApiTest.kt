package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.datastore.services.UserDataHandler
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@Tag("unit-tests")
internal class UserDataApiTest {

    private lateinit var api: UserDataApi
    private val dto = UserDto(name = "test", rank = Double.MIN_VALUE)

    @BeforeEach
    fun setUp() = runBlocking<Unit> {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(UserDataApi::class.java)
        val dataHandler = mockk<UserDataHandler>()
        coEvery { dataHandler.saveAll(any()) } answers { listOf(this@UserDataApiTest.dto) }
        coEvery { dataHandler.save(any()) } answers { this@UserDataApiTest.dto }
        coEvery { dataHandler.removeAll() } answers { true }
        coEvery { dataHandler.remove(any()) } answers { true }
        coEvery { dataHandler.updateByName(any()) } answers { this@UserDataApiTest.dto }
        coEvery { dataHandler.removeByName(any()) } answers { true }
        coEvery { dataHandler.findByName(any()) } answers { this@UserDataApiTest.dto }

        primaryConstructor?.let {
            this@UserDataApiTest.api = BeanUtils.instantiateClass(it, dataHandler)
        }
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@UserDataApiTest.api.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.saveAll(listOf(this@UserDataApiTest.dto))
        assertEquals(1, returnedValue.size)
        this@UserDataApiTest.assertDtoEquality(this@UserDataApiTest.dto, returnedValue[0])
    }

    @Test
    fun save() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.save(this@UserDataApiTest.dto)
        assertNotNull(returnedValue)
        this@UserDataApiTest.assertDtoEquality(this@UserDataApiTest.dto, returnedValue)
    }

    @Test
    fun remove() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.remove(this@UserDataApiTest.dto)
        assertTrue(returnedValue)
    }

    @Test
    fun removeAll() = runBlocking {
        this@UserDataApiTest.api.removeAll()
    }

    @Test
    fun updateByName() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.updateByName(this@UserDataApiTest.dto)
        this@UserDataApiTest.assertDtoEquality(this@UserDataApiTest.dto, returnedValue)
    }

    @Test
    fun removeByName() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.removeByName(this@UserDataApiTest.dto.name)
        assertTrue(returnedValue)
    }

    @Test
    fun findByName() = runBlocking {
        val returnedValue = this@UserDataApiTest.api.findByName(this@UserDataApiTest.dto.name)
        assertNotNull(returnedValue)
        this@UserDataApiTest.assertDtoEquality(this@UserDataApiTest.dto, returnedValue)
    }

    private fun assertDtoEquality(expectedDto: UserDto, actualDto: UserDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.rank, actualDto.rank)
    }
}