package hq.waifuparade.botcchi.datastore.repositories

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unit-tests")
internal class InMemoryConnectionDataTest {

    companion object {

        private var defaultLoginTime = Long.MIN_VALUE
        private var defaultIsPlaylistOpen = false

        @JvmStatic
        @BeforeAll
        fun setUp() {
            defaultLoginTime = InMemoryConnectionData.currentLoginTime
            defaultIsPlaylistOpen = InMemoryConnectionData.playlistOpen
        }
    }

    @AfterEach
    fun cleanUp() {
        InMemoryConnectionData.revertLoginTimeToDefault()
        InMemoryConnectionData.updatePlaylistOpen(defaultIsPlaylistOpen)
    }

    @Test
    fun updateLoginTime() {
        val defaultLoginTime = InMemoryConnectionData.currentLoginTime
        val newLoginTime = Long.MAX_VALUE

        // check if newer time (= higher number) will be used as new login time
        val updatedLoginTime = InMemoryConnectionData.updateLoginTime(newLoginTime)
        Assertions.assertNotEquals(defaultLoginTime, updatedLoginTime)
        Assertions.assertEquals(newLoginTime, updatedLoginTime)

        // check if older time (= lower number) will not be used as new login time
        val updatedLoginTime2 = InMemoryConnectionData.updateLoginTime(defaultLoginTime)
        Assertions.assertNotEquals(defaultLoginTime, updatedLoginTime2)
        Assertions.assertEquals(updatedLoginTime, updatedLoginTime2)
    }

    @Test
    fun revertLoginTimeToDefault() {
        val defaultLoginTime = InMemoryConnectionData.currentLoginTime
        val newLoginTime = Long.MAX_VALUE

        InMemoryConnectionData.updateLoginTime(newLoginTime)
        Assertions.assertEquals(newLoginTime, InMemoryConnectionData.currentLoginTime)
        InMemoryConnectionData.revertLoginTimeToDefault()
        Assertions.assertNotEquals(newLoginTime, InMemoryConnectionData.currentLoginTime)
        Assertions.assertEquals(defaultLoginTime, InMemoryConnectionData.currentLoginTime)
    }

    @Test
    fun updatePlaylistOpen() {
        val defaultIsOpen = InMemoryConnectionData.playlistOpen
        val newIsOpen = !defaultIsOpen

        // check if newer time (= higher number) will be used as new login time
        val updatedIsOpen = InMemoryConnectionData.updatePlaylistOpen(newIsOpen)
        Assertions.assertNotEquals(defaultIsOpen, updatedIsOpen)
        Assertions.assertEquals(newIsOpen, updatedIsOpen)
        Assertions.assertEquals(newIsOpen, InMemoryConnectionData.playlistOpen)
    }

    @Test
    fun getLoginTime_DefaultValue() {
        // check default value
        Assertions.assertEquals(Long.MIN_VALUE, InMemoryConnectionData.currentLoginTime)
    }

    @Test
    fun getLoginTime_NoUpdate() {
        // check consequent calls without update in between return same value
        val loginTime1 = InMemoryConnectionData.currentLoginTime
        val loginTime2 = InMemoryConnectionData.currentLoginTime

        Assertions.assertEquals(loginTime1, loginTime2)
    }

    @Test
    fun getLoginTime_WithUpdate() {
        // check consequent calls with update in between return different value
        val loginTime1 = InMemoryConnectionData.currentLoginTime
        InMemoryConnectionData.updateLoginTime(0)
        val loginTime2 = InMemoryConnectionData.currentLoginTime

        Assertions.assertNotEquals(0, loginTime1)
        Assertions.assertEquals(0, loginTime2)
        Assertions.assertNotEquals(loginTime1, loginTime2)
    }

    @Test
    fun isPlaylistOpen_DefaultValue() {
        // check default value
        Assertions.assertEquals(false, InMemoryConnectionData.playlistOpen)
    }

    @Test
    fun isPlaylistOpen_NoUpdate() {
        // check consequent calls without update in between return same value
        val isOpen1 = InMemoryConnectionData.playlistOpen
        val isOpen2 = InMemoryConnectionData.playlistOpen

        Assertions.assertEquals(isOpen1, isOpen2)
    }

    @Test
    fun isPlaylistOpen_WithUpdate() {
        // check consequent calls with update in between return different value
        val isOpen1 = InMemoryConnectionData.playlistOpen
        InMemoryConnectionData.updatePlaylistOpen(!isOpen1)
        val isOpen2 = InMemoryConnectionData.playlistOpen

        Assertions.assertNotEquals(!isOpen1, isOpen1)
        Assertions.assertEquals(!isOpen1, isOpen2)
        Assertions.assertNotEquals(isOpen1, isOpen2)
    }
}