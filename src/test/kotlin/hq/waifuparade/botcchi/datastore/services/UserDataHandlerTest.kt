package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.AbstractDatabaseIntegrationTest
import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.datastore.repositories.UserRepository
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

internal class UserDataHandlerTest(
    @Autowired private val repository: UserRepository
) : AbstractDatabaseIntegrationTest {

    private lateinit var dataHandler: UserDataHandler

    @BeforeEach
    fun setUp() {
        this.dataHandler = UserDataHandler(this.repository)
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@UserDataHandlerTest.dataHandler.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<UserDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@UserDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@UserDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        for (i in 0 until numberOfDtos) {
            val expectedDto = expectedDtos[i]
            val actualDto = actualDtos[i]
            this@UserDataHandlerTest.assertDtoEquality(expectedDto, actualDto)
        }
    }

    @Test
    fun save() = runBlocking {
        val dto = this@UserDataHandlerTest.createDto()
        val savedDto = this@UserDataHandlerTest.dataHandler.save(dto)
        this@UserDataHandlerTest.assertDtoEquality(dto, savedDto)
    }

    @Test
    fun remove() = runBlocking {
        val dto = this@UserDataHandlerTest.createDto()
        this@UserDataHandlerTest.dataHandler.save(dto)

        assertTrue(this@UserDataHandlerTest.dataHandler.remove(dto))
        assertFalse(this@UserDataHandlerTest.dataHandler.remove(dto))
    }

    @Test
    fun removeAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<UserDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@UserDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@UserDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        this@UserDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@UserDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun getNumberOfModels() = runBlocking {
        val dto = this@UserDataHandlerTest.createDto()
        this@UserDataHandlerTest.dataHandler.save(dto)
        assertEquals(1, this@UserDataHandlerTest.dataHandler.getNumberOfModels())

        this@UserDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@UserDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun updateByName() = runBlocking {
        val dto1 = UserDto(name = "testDto", rank = Double.MIN_VALUE)
        this@UserDataHandlerTest.dataHandler.save(dto1)

        val dto2 = dto1.copy(rank = Double.MAX_VALUE)
        val updatedDto = this@UserDataHandlerTest.dataHandler.updateByName(dto2)
        this@UserDataHandlerTest.assertDtoEquality(dto2, updatedDto)
    }

    @Test
    fun removeByName() = runBlocking {
        val dto1 = UserDto(name = "testDto", rank = Double.MIN_VALUE)
        this@UserDataHandlerTest.dataHandler.save(dto1)
        assertEquals(1, this@UserDataHandlerTest.dataHandler.getNumberOfModels())
        this@UserDataHandlerTest.dataHandler.removeByName(dto1.name)
        assertEquals(0, this@UserDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun findByName() = runBlocking {
        val dto1 = UserDto(name = "testDto", rank = Double.MIN_VALUE)
        this@UserDataHandlerTest.dataHandler.save(dto1)
        assertEquals(1, this@UserDataHandlerTest.dataHandler.getNumberOfModels())
        val foundModel = this@UserDataHandlerTest.dataHandler.findByName("testDto")
        assertNotNull(foundModel)
        this@UserDataHandlerTest.assertDtoEquality(dto1, foundModel)

        assertNull(this@UserDataHandlerTest.dataHandler.findByName("not-existing-model"))
    }

    private fun createDto(numberOfDto: Int = 0): UserDto {
        return UserDto(name = "testName${numberOfDto}", rank = Double.MIN_VALUE)
    }

    private fun assertDtoEquality(expectedDto: UserDto, actualDto: UserDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.rank, actualDto.rank)
    }
}