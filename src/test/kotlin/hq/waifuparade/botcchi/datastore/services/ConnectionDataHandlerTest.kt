package hq.waifuparade.botcchi.datastore.services

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

@Tag("unit-tests")
internal class ConnectionDataHandlerTest {

    private lateinit var dataHandler: ConnectionDataHandler

    @BeforeEach
    fun setUp() {
        this.dataHandler = ConnectionDataHandler()
    }

    @AfterEach
    fun cleanUp() {
        this.dataHandler.removeLoginTime()
        this.dataHandler.setPlaylistOpen(false)
    }

    @Test
    fun setLoginTime() {
        val loginTime = System.nanoTime()
        val updatedLoginTime = this.dataHandler.setLoginTime(loginTime)
        assertEquals(loginTime, updatedLoginTime)
    }

    @Test
    fun getLoginTime() {
        val loginTime = System.nanoTime()
        val updatedLoginTime = this.dataHandler.setLoginTime(loginTime)
        assertEquals(loginTime, updatedLoginTime)
        assertEquals(loginTime, this.dataHandler.getLoginTime())
    }

    @Test
    fun removeLoginTime() {
        val defaultLoginTime = this.dataHandler.getLoginTime()

        val loginTime = System.nanoTime()
        val updatedLoginTime = this.dataHandler.setLoginTime(loginTime)
        assertEquals(loginTime, updatedLoginTime)

        val loginTimeAfterRemoval = this.dataHandler.removeLoginTime()
        assertEquals(defaultLoginTime, loginTimeAfterRemoval)
    }

    @Test
    fun setPlaylistOpen() {
        val defaultIsPlaylistOpen = this.dataHandler.isPlaylistOpen()
        val invertedDefaultValue = this.dataHandler.setPlaylistOpen(!defaultIsPlaylistOpen)
        assertNotEquals(defaultIsPlaylistOpen, invertedDefaultValue)

        val shouldBeDefaultValue = this.dataHandler.setPlaylistOpen(defaultIsPlaylistOpen)
        assertEquals(defaultIsPlaylistOpen, shouldBeDefaultValue)
    }

    @Test
    fun isPlaylistOpen() {
        val defaultIsPlaylistOpen = this.dataHandler.isPlaylistOpen()
        val invertedDefaultValue = this.dataHandler.setPlaylistOpen(!defaultIsPlaylistOpen)
        assertNotEquals(defaultIsPlaylistOpen, invertedDefaultValue)
        assertEquals(invertedDefaultValue, this.dataHandler.isPlaylistOpen())

        val shouldBeDefaultValue = this.dataHandler.setPlaylistOpen(defaultIsPlaylistOpen)
        assertEquals(defaultIsPlaylistOpen, shouldBeDefaultValue)
        assertEquals(shouldBeDefaultValue, this.dataHandler.isPlaylistOpen())
    }
}