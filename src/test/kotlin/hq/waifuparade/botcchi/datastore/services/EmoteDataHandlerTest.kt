package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.AbstractDatabaseIntegrationTest
import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.datastore.repositories.EmoteRepository
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class EmoteDataHandlerTest(
    @Autowired private val repository: EmoteRepository
) : AbstractDatabaseIntegrationTest {

    private lateinit var dataHandler: EmoteDataHandler

    @BeforeEach
    fun setUp() {
        this.dataHandler = EmoteDataHandler(this.repository)
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@EmoteDataHandlerTest.dataHandler.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<EmoteDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@EmoteDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@EmoteDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        for (i in 0 until numberOfDtos) {
            val expectedDto = expectedDtos[i]
            val actualDto = actualDtos[i]
            this@EmoteDataHandlerTest.assertDtoEquality(expectedDto, actualDto)
        }
    }

    @Test
    fun save() = runBlocking {
        val dto = this@EmoteDataHandlerTest.createDto()
        val savedDto = this@EmoteDataHandlerTest.dataHandler.save(dto)
        this@EmoteDataHandlerTest.assertDtoEquality(dto, savedDto)
    }

    @Test
    fun remove() = runBlocking {
        val dto = this@EmoteDataHandlerTest.createDto()
        this@EmoteDataHandlerTest.dataHandler.save(dto)

        assertTrue(this@EmoteDataHandlerTest.dataHandler.remove(dto))
        assertFalse(this@EmoteDataHandlerTest.dataHandler.remove(dto))
    }

    @Test
    fun removeAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<EmoteDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@EmoteDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@EmoteDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        this@EmoteDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@EmoteDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun getNumberOfModels() = runBlocking {
        val dto = this@EmoteDataHandlerTest.createDto()
        this@EmoteDataHandlerTest.dataHandler.save(dto)
        assertEquals(1, this@EmoteDataHandlerTest.dataHandler.getNumberOfModels())

        this@EmoteDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@EmoteDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun updateImage() = runBlocking {
        val dto1 = EmoteDto(name = "testDto1", image = "testImage1")
        this@EmoteDataHandlerTest.dataHandler.save(dto1)
        val dto2 = dto1.copy(image = "imageTest1")
        val updatedDto = this@EmoteDataHandlerTest.dataHandler.updateImage(dto2)
        this@EmoteDataHandlerTest.assertDtoEquality(dto2, updatedDto)
    }

    @Test
    fun updateName() = runBlocking {
        val dto1 = EmoteDto(name = "testDto1", image = "testImage1")
        this@EmoteDataHandlerTest.dataHandler.save(dto1)

        val dto2 = dto1.copy(name = "dtoTest1")
        val updatedDto = this@EmoteDataHandlerTest.dataHandler.updateName(dto1, dto2.name)
        this@EmoteDataHandlerTest.assertDtoEquality(dto2, updatedDto)
    }

    @Test
    fun filterByNamePrefix() = runBlocking {
        val dto1 = EmoteDto(name = "testDto1", image = "testImage1")
        val dto2 = EmoteDto(name = "testDto2", image = "testImage2")
        val dto3 = EmoteDto(name = "dtoTest1", image = "testImage3")
        this@EmoteDataHandlerTest.dataHandler.saveAll(listOf(dto1, dto2, dto3))
        assertEquals(3, this@EmoteDataHandlerTest.dataHandler.getNumberOfModels())

        val foundEmotes = this@EmoteDataHandlerTest.dataHandler.filterByNamePrefix("test")
        assertEquals(2, foundEmotes.size)
        this@EmoteDataHandlerTest.assertDtoEquality(dto1, foundEmotes[0])
        this@EmoteDataHandlerTest.assertDtoEquality(dto2, foundEmotes[1])
    }

    private fun createDto(numberOfDto: Int = 0): EmoteDto {
        return EmoteDto(name = "testName${numberOfDto}", image = "testImage${numberOfDto}")
    }

    private fun assertDtoEquality(expectedDto: EmoteDto, actualDto: EmoteDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.image, actualDto.image)
    }
}