package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.AbstractDatabaseIntegrationTest
import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import hq.waifuparade.botcchi.datastore.repositories.PermissionRepository
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

internal class PermissionDataHandlerTest(
    @Autowired private val repository: PermissionRepository,
) : AbstractDatabaseIntegrationTest {

    private lateinit var dataHandler: PermissionDataHandler

    @BeforeEach
    fun setUp() {
        this.dataHandler = PermissionDataHandler(this.repository)
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@PermissionDataHandlerTest.dataHandler.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<PermissionDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@PermissionDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@PermissionDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        for (i in 0 until numberOfDtos) {
            val expectedDto = expectedDtos[i]
            val actualDto = actualDtos[i]
            this@PermissionDataHandlerTest.assertDtoEquality(expectedDto, actualDto)
        }
    }

    @Test
    fun save() = runBlocking {
        val dto = this@PermissionDataHandlerTest.createDto()
        val savedDto = this@PermissionDataHandlerTest.dataHandler.save(dto)
        this@PermissionDataHandlerTest.assertDtoEquality(dto, savedDto)
    }

    @Test
    fun remove() = runBlocking {
        val dto = this@PermissionDataHandlerTest.createDto()
        this@PermissionDataHandlerTest.dataHandler.save(dto)

        assertTrue(this@PermissionDataHandlerTest.dataHandler.remove(dto))
        assertFalse(this@PermissionDataHandlerTest.dataHandler.remove(dto))
    }

    @Test
    fun removeAll() = runBlocking {
        val numberOfDtos = 1000
        val expectedDtos = mutableListOf<PermissionDto>()

        for (i in 0 until numberOfDtos) {
            val dto = this@PermissionDataHandlerTest.createDto(i)
            expectedDtos.add(dto)
        }

        val actualDtos = this@PermissionDataHandlerTest.dataHandler.saveAll(expectedDtos)
        assertEquals(numberOfDtos, actualDtos.size)

        this@PermissionDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun getNumberOfModels() = runBlocking {
        val dto = this@PermissionDataHandlerTest.createDto()
        this@PermissionDataHandlerTest.dataHandler.save(dto)
        assertEquals(1, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())

        this@PermissionDataHandlerTest.dataHandler.removeAll()
        assertEquals(0, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun updateByName() = runBlocking {
        val dto1 = PermissionDto(name = "testDto", minRequiredRank = Double.MIN_VALUE)
        this@PermissionDataHandlerTest.dataHandler.save(dto1)

        val dto2 = dto1.copy(minRequiredRank = Double.MAX_VALUE)
        val updatedDto = this@PermissionDataHandlerTest.dataHandler.updateByName(dto2)
        this@PermissionDataHandlerTest.assertDtoEquality(dto2, updatedDto)
    }

    @Test
    fun removeByName() = runBlocking {
        val dto1 = PermissionDto(name = "testDto", minRequiredRank = Double.MIN_VALUE)
        this@PermissionDataHandlerTest.dataHandler.save(dto1)
        assertEquals(1, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())
        this@PermissionDataHandlerTest.dataHandler.removeByName(dto1.name)
        assertEquals(0, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())
    }

    @Test
    fun findByName() = runBlocking {
        val dto1 = PermissionDto(name = "testDto", minRequiredRank = Double.MIN_VALUE)
        this@PermissionDataHandlerTest.dataHandler.save(dto1)
        assertEquals(1, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())
        val foundModel = this@PermissionDataHandlerTest.dataHandler.findByName("testDto")
        assertNotNull(foundModel)
        this@PermissionDataHandlerTest.assertDtoEquality(dto1, foundModel)

        assertNull(this@PermissionDataHandlerTest.dataHandler.findByName("not-existing-model"))
    }

    @Test
    fun findByRelatedAction() = runBlocking {
        val dto1 = PermissionDto(name = "playlistadd", minRequiredRank = Double.MIN_VALUE)
        val dto2 = PermissionDto(name = "oplaylistadd", minRequiredRank = Double.MAX_VALUE)
        val dto3 = PermissionDto(name = "testDto", minRequiredRank = Double.MAX_VALUE)
        this@PermissionDataHandlerTest.dataHandler.saveAll(listOf(dto1, dto2, dto3))
        assertEquals(3, this@PermissionDataHandlerTest.dataHandler.getNumberOfModels())

        val foundEmotes =
            this@PermissionDataHandlerTest.dataHandler.findByRelatedAction(ApplicationAction.ADD_MEDIA)
        assertEquals(2, foundEmotes.size)
        this@PermissionDataHandlerTest.assertDtoEquality(dto1, foundEmotes[0])
        this@PermissionDataHandlerTest.assertDtoEquality(dto2, foundEmotes[1])
    }

    private fun createDto(numberOfDto: Int = 0): PermissionDto {
        return PermissionDto(name = "testName${numberOfDto}", minRequiredRank = Double.MIN_VALUE)
    }

    private fun assertDtoEquality(expectedDto: PermissionDto, actualDto: PermissionDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.minRequiredRank, actualDto.minRequiredRank)
    }
}