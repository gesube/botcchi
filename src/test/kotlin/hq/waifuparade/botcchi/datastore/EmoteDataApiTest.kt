package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.datastore.services.EmoteDataHandler
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.BeanUtils
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@Tag("unit-tests")
internal class EmoteDataApiTest {

    private lateinit var api: EmoteDataApi
    private val dto = EmoteDto(name = "test", image = "test")

    @BeforeEach
    fun setUp() = runBlocking<Unit> {
        val primaryConstructor = BeanUtils.findPrimaryConstructor(EmoteDataApi::class.java)
        val dataHandler = mockk<EmoteDataHandler>()
        coEvery { dataHandler.saveAll(any()) } answers { listOf(this@EmoteDataApiTest.dto) }
        coEvery { dataHandler.save(any()) } answers { this@EmoteDataApiTest.dto }
        coEvery { dataHandler.removeAll() } answers { true }
        coEvery { dataHandler.remove(any()) } answers { true }
        coEvery { dataHandler.updateName(any(), any()) } answers { this@EmoteDataApiTest.dto }
        coEvery { dataHandler.updateImage(any()) } answers { this@EmoteDataApiTest.dto }
        coEvery { dataHandler.filterByNamePrefix(any()) } answers { listOf(this@EmoteDataApiTest.dto) }
        coEvery { dataHandler.getNumberOfModels() } answers { 1 }

        primaryConstructor?.let {
            this@EmoteDataApiTest.api = BeanUtils.instantiateClass(it, dataHandler)
        }
    }

    @AfterEach
    fun cleanUp() = runBlocking<Unit> {
        this@EmoteDataApiTest.api.removeAll()
    }

    @Test
    fun saveAll() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.saveAll(listOf(this@EmoteDataApiTest.dto))
        assertEquals(1, returnedValue.size)
        this@EmoteDataApiTest.assertDtoEquality(this@EmoteDataApiTest.dto, returnedValue[0])
    }

    @Test
    fun save() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.save(this@EmoteDataApiTest.dto)
        assertNotNull(returnedValue)
        this@EmoteDataApiTest.assertDtoEquality(this@EmoteDataApiTest.dto, returnedValue)
    }

    @Test
    fun remove() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.remove(this@EmoteDataApiTest.dto)
        assertTrue(returnedValue)
    }

    @Test
    fun removeAll() = runBlocking {
        this@EmoteDataApiTest.api.removeAll()
    }

    @Test
    fun updateName() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.updateName(this@EmoteDataApiTest.dto, "")
        this@EmoteDataApiTest.assertDtoEquality(this@EmoteDataApiTest.dto, returnedValue)
    }

    @Test
    fun updateImage() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.updateImage(this@EmoteDataApiTest.dto)
        this@EmoteDataApiTest.assertDtoEquality(this@EmoteDataApiTest.dto, returnedValue)
    }

    @Test
    fun filterByNamePrefix() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.filterByNamePrefix(this@EmoteDataApiTest.dto.name)
        assertEquals(1, returnedValue.size)
        this@EmoteDataApiTest.assertDtoEquality(this@EmoteDataApiTest.dto, returnedValue[0])
    }

    @Test
    fun getNumberOfModels() = runBlocking {
        val returnedValue = this@EmoteDataApiTest.api.getNumberOfModels()
        assertEquals(1, returnedValue)
    }

    private fun assertDtoEquality(expectedDto: EmoteDto, actualDto: EmoteDto) {
        assertEquals(expectedDto.name, actualDto.name)
        assertEquals(expectedDto.image, actualDto.image)
    }
}