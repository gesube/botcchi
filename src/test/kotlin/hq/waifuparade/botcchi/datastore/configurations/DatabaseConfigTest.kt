package hq.waifuparade.botcchi.datastore.configurations

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Tag("unit-tests")
internal class DatabaseConfigTest {

    private val databaseConfigAnnotations = DatabaseConfig().javaClass.annotations
    private val modelPackage = "datastore.models"
    private val repositoryPackage = "datastore.repositories"

    @Test
    fun checkConfigurationAnnotation() {
        // @Configuration
        val annotation = databaseConfigAnnotations.find { it is Configuration }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as Configuration

        Assertions.assertEquals("", annotation.value)
        Assertions.assertTrue(annotation.proxyBeanMethods)
    }

    @Test
    fun checkEnableJpaRepositoriesAnnotation() {
        // @EnableJpaRepositories
        val annotation = databaseConfigAnnotations.find { it is EnableJpaRepositories }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as EnableJpaRepositories

        Assertions.assertEquals(1, annotation.value.size)
        Assertions.assertTrue(annotation.value[0].endsWith(this.repositoryPackage, ignoreCase = false))
    }

    @Test
    fun checkEntityScanAnnotation() {
        // @EntityScan
        val annotation = databaseConfigAnnotations.find { it is EntityScan }
        Assertions.assertNotNull(annotation)

        // give the compiler a hint which type the annotation should have
        annotation as EntityScan

        Assertions.assertEquals(1, annotation.value.size)
        Assertions.assertTrue(annotation.value[0].endsWith(this.modelPackage, ignoreCase = false))
    }
}