package hq.waifuparade.botcchi.events.enums

/**
 * Enum with all possible application event types.
 * Every application events needs a type.
 *
 * Example usages to create a listener that listens to a specific event type can be found
 * in [hq.waifuparade.botcchi.cytube.listener.ListenerCoordinator].
 */
enum class ApplicationEventType(val eventName: String) {

    SOCKET_CONNECTED("socket_connected"),
    SOCKET_DISCONNECTED("socket_connected"),
    CHANNEL_LOGIN("channel_login")
}