package hq.waifuparade.botcchi.events.services

import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.events.models.ApplicationEvent
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

/**
 * Class to publish application wide events.
 * Currently, it encapsulates the [ApplicationEventPublisher] provided by Spring.
 */
@Service
internal class EventPublisher private constructor(
    private val applicationEventPublisher: ApplicationEventPublisher
                                                 ) {

    /**
     * Publish an event with the provided [ApplicationEventType].
     * @param applicationEventType - the type of the application event to publish
     */
    fun publishEvent(applicationEventType: ApplicationEventType) {
        this.logger.info { "Publish internal event: ${applicationEventType.eventName}" }
        val applicationEvent = ApplicationEvent(applicationEventType)
        this.applicationEventPublisher.publishEvent(applicationEvent)
    }
}