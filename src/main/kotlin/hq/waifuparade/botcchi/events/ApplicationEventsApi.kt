package hq.waifuparade.botcchi.events

import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.events.services.EventPublisher
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import org.springframework.stereotype.Service

@Service
@InjectLogger
class ApplicationEventsApi private constructor(
    private val eventPublisher: EventPublisher
                                              ) {

    /**
     * Publish an event with the provided [ApplicationEventType].
     * @param applicationEventType - the type of the application event to publish
     */
    fun publishEvent(applicationEventType: ApplicationEventType) {
        this.eventPublisher.publishEvent(applicationEventType)
    }
}