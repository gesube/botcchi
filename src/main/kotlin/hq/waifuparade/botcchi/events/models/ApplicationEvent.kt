package hq.waifuparade.botcchi.events.models

import hq.waifuparade.botcchi.events.enums.ApplicationEventType

data class ApplicationEvent(
    val eventType: ApplicationEventType
                           )