package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.ChannelPasswordEventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

@Service
@CytubeEventEmitter(OutgoingCytubeEventType.CHANNEL_PASSWORD)
internal class ChannelPasswordEventEmitter(
    socketClient: CytubeSocketClient,
    private val cytubePropertiesReader: CytubePropertiesReader
                                      ) : AbstractEventEmitter(socketClient) {

    override suspend fun emitEvent(eventRequestData: EventRequest) {
        val requestData = when {
            (eventRequestData is ChannelPasswordEventRequest) -> eventRequestData
            else -> ChannelPasswordEventRequest(channelPassword = this.cytubePropertiesReader.room.password)
        }
        this.doEmit(requestData.channelPassword)
    }
}