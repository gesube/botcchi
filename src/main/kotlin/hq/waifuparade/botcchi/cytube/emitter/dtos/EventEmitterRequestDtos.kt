package hq.waifuparade.botcchi.cytube.emitter.dtos

open class EventRequest

open class MessageEventRequest : EventRequest()

data class PersonalMessageEventRequest(
    val msg: String,
    val to: String
                                      ) : MessageEventRequest()

data class PublicMessageEventRequest(
    val msg: String
                                    ) : MessageEventRequest()

data class JoinChannelEventRequest(
    val name: String
                                  ) : EventRequest()

data class ChannelPasswordEventRequest(
    val channelPassword: String
                                      ) : EventRequest()

data class LoginEventRequest(
    val name: String, val pw: String
                            ) : EventRequest()

data class QueueMediaEventRequest(
    val id: String,
    val type: String,
    val pos: String,
    val title: String,
    val temp: Boolean
                                 ) : EventRequest()