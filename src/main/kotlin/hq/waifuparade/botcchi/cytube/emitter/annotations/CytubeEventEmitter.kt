package hq.waifuparade.botcchi.cytube.emitter.annotations

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType

/**
 * Annotation to register a class as an emitter for cytube events.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class CytubeEventEmitter(
    val eventType: OutgoingCytubeEventType
                                            )