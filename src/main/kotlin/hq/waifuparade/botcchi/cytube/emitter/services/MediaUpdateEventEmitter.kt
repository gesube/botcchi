package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.QueueMediaEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import org.springframework.stereotype.Service

@Service
@CytubeEventEmitter(OutgoingCytubeEventType.MEDIA_UPDATE)
internal class MediaUpdateEventEmitter(
    socketClient: CytubeSocketClient
                                      ) : AbstractEventEmitter(socketClient) {

    override suspend fun emitEvent(eventRequestData: EventRequest) {
        if (eventRequestData is QueueMediaEventRequest) {
            this.doEmit(eventRequestData)
        }
    }
}