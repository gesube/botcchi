package hq.waifuparade.botcchi.cytube.emitter

/**
 * Enum with all possible cytube events to emit.
 */
@Suppress("unused")
enum class OutgoingCytubeEventType(val eventName: String) {

    // channel.js
    READ_CHAN_LOG("readChanLog"),
    LOAD_FAIL("loadFail"),

    // accesscontrol.js
    CHANNEL_PASSWORD("channelPassword"),

    // user.js
    LOGIN("login"),
    SET_FLAG("setFlag"),
    BORROW_RANK("borrowRank"),
    JOIN_CHANNEL("joinChannel"),
    INIT_ACP("initACP"),
    AFK("afk"),

    // chat.js
    CHAT_MSG("chatMsg"),
    PM("pm"),

    // playlist.js
    QUEUE("queue"),
    SET_TEMP("setTemp"),
    MOVE_MEDIA("moveMedia"),
    ASSIGN_LEADER("assignLeader"),
    MEDIA_UPDATE("mediaUpdate"),
    DELETE("delete"),
    JUMP_TO("jumpTo"),
    PLAY_NEXT("playNext"),
    PLAYER_READY("playerReady"),
    REQUEST_PLAYLIST("requestPlaylist"),
    CLEAR_PLAYLIST("clearPlaylist"),
    SHUFFLE_PLAYLIST("shufflePlaylist"),
    LIST_PLAYLISTS("listPlaylists"),
    CLONE_PLAYLIST("clonePlaylist"),
    DELETE_PLAYLIST("deletePlaylist"),
    QUEUE_PLAYLIST("queuePlaylist"),

    // emotes.js
    RENAME_EMOTE("renameEmote"),
    UPDATE_EMOTE("updateEmote"),
    IMPORT_EMOTES("importEmotes"),
    MOVE_EMOTE("moveEmote"),
    REMOVE_EMOTE("removeEmote"),

    // filters.js
    ADD_FILTER("addFilter"),
    UPDATE_FILTER("updateFilter"),
    IMPORT_FILTERS("importFilters"),
    MOVE_FILTER("moveFilter"),
    REMOVE_FILTER("removeFilter"),
    REQUEST_CHAT_FILTERS("requestChatFilters"),

    // poll.js
    EFFECTIVE_RANK_CHANGE("effectiveRankChange"),
    NEW_POLL("newPoll"),
    VOTE("vote"),
    CLOSE_POLL("closePoll"),

    // permissions.js
    SET_PERMISSIONS("setPermissions"),
    TOGGLE_PLAYLIST_LOCK("togglePlaylistLock"),

    // customization.js
    SET_CHANNEL_CSS("setChannelCSS"),
    SET_CHANNEL_JS("setChannelJS"),
    SET_MOTD("setMotd"),

    // voteskip.js
    VOTESKIP("voteskip"),

    // ranks.js
    SET_CHANNEL_RANKS("setChannelRanks"),
    REQUEST_CHANNEL_RANKS("requestChannelRanks"),

    // opts.js
    SET_OPTIONS("setOptions"),

    // kickban.js
    REQUEST_BANLIST("requestBanlist"),
    UNBAN("unban"),

    // library.js
    UNCACHE("uncache"),
    SEARCH_MEDIA("searchMedia"),

    // acp.js
    ACP_ANNOUNCE("acp-announce"),
    ACP_ANNOUNCE_CLEAR("acp-announce-clear"),
    ACP_GBAN("acp-gban"),
    ACP_GBAN_DELETE("acp-gban-delete"),
    ACP_LIST_USERS("acp-list-users"),
    ACP_SET_RANK("acp-set-rank"),
    ACP_RESET_PASSWORD("acp-reset-password"),
    ACP_LIST_CHANNELS("acp-list-channels"),
    ACP_DELETE_CHANNEL("acp-delete-channel"),
    ACP_LIST_ACTIVE_CHANNELS("acp-list-activechannels"),
    ACP_FORCE_UNLOAD("acp-force-unload"),

    // server.js
    USER_PROFILE_CHANGED("UserProfileChanged"),
    CHANNEL_DELETED("ChannelDeleted"),
    CHANNEL_REGISTERED("ChannelRegistered")
}