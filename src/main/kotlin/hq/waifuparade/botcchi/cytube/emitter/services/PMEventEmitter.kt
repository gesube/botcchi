package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.PersonalMessageEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import org.springframework.stereotype.Service

@Service
@CytubeEventEmitter(OutgoingCytubeEventType.PM)
internal class PMEventEmitter(
    socketClient: CytubeSocketClient
                                  ) : AbstractEventEmitter(socketClient) {

    override suspend fun emitEvent(eventRequestData: EventRequest) {
        if (eventRequestData is PersonalMessageEventRequest) {
            this.doEmit(eventRequestData)
        }
    }
}