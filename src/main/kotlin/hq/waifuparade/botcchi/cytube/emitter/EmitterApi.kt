package hq.waifuparade.botcchi.cytube.emitter

import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.cytube.emitter.services.AbstractEventEmitter
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import kotlinx.coroutines.delay
import org.springframework.stereotype.Service

@Service
@InjectLogger
class EmitterApi private constructor(
    private val eventEmitters: List<AbstractEventEmitter>
                                    ) {

    /**
     * Calls the event emitter for the provided [OutgoingCytubeEventType] with the provided [eventRequestData].
     * If no emitter exists, nothing happens.
     * @param outEvent the type of event to emit
     * @param eventRequestData additional data to emit for the event
     * @param delayTimeAfterEventEmitting the cooldown time in milliseconds after the event was emitted
     */
    suspend fun callEventEmitter(
        outEvent: OutgoingCytubeEventType,
        eventRequestData: EventRequest = EventRequest(),
        delayTimeAfterEventEmitting: Long = 0
                                ) {

        this.logger.info { "Trying to call an emitter for $outEvent." }
        var emitterWasCalled = false

        eventEmitters.forEach { emitter ->
            if (emitter.getEventType() == outEvent) {
                emitter.emitEvent(eventRequestData)
                emitterWasCalled = true
                return@forEach
            }
        }

        if (emitterWasCalled) {
            this.logger.info { "Emitter was found and called." }
            delay(delayTimeAfterEventEmitting)
        } else {
            this.logger.info { "No emitter was found and therefore not called." }
        }
    }
}