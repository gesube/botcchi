package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.LoginEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

@Service
@CytubeEventEmitter(OutgoingCytubeEventType.LOGIN)
internal class LoginEventEmitter(
    socketClient: CytubeSocketClient,
    private val cytubePropertiesReader: CytubePropertiesReader
                                ) : AbstractEventEmitter(socketClient) {

    override suspend fun emitEvent(eventRequestData: EventRequest) {
        val requestData = when {
            (eventRequestData is LoginEventRequest) -> eventRequestData
            else -> LoginEventRequest(
                name = this.cytubePropertiesReader.user.name,
                pw = this.cytubePropertiesReader.user.password
                                     )
        }
        this.doEmit(requestData)
    }
}