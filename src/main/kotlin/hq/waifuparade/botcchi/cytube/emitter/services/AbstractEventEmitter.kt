package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.json.JSONObject

/**
 * Handles the emission of the event mentioned in the [CytubeEventEmitter] annotation.
 * If the emitter doesn't have such an annotation, it does nothing.
 */
internal abstract class AbstractEventEmitter(
    private val socketClient: CytubeSocketClient
                                            ) {

    /**
     * Gets the event to listen to from the [CytubeEventEmitter] annotation every emitter should have.
     */
    fun getEventType(): OutgoingCytubeEventType {
        val cytubeEventAnnotation =
            this.javaClass.annotations.find { it is CytubeEventEmitter } as CytubeEventEmitter
        return cytubeEventAnnotation.eventType
    }

    /**
     * Emits the event with the provided [eventRequestData].
     *
     * @param eventRequestData additional data to emit for the event
     */
    abstract suspend fun emitEvent(eventRequestData: EventRequest = EventRequest())

    /**
     * Converts the provided [eventRequestData] to its json representation and emits the event.
     *
     * Can and should only be called from an emitter implementation to finally commit the emission.
     *
     * @param eventRequestData additional data to emit for the event
     */
    protected fun doEmit(eventRequestData: EventRequest) {
        this.logger.info { "Trying to emit event ${this.getEventType().eventName} " +
                "with data: " + JsonMapper.convertObjectToString(eventRequestData) }

        this.socketClient.emit(
            event = this.getEventType().eventName,
            eventData = JSONObject(JsonMapper.convertObjectToString(eventRequestData))
                              )
    }

    /**
     * Emits the event with the provided [stringifiedEventRequestData] as single value string.
     *
     * Can and should only be called from an emitter implementation to finally commit the emission.
     *
     * @param stringifiedEventRequestData additional single value string to emit for the event
     */
    protected fun doEmit(stringifiedEventRequestData: String) {
        this.logger.info { "Trying to emit event ${this.getEventType().eventName} " +
                "with data: " + stringifiedEventRequestData }

        this.socketClient.emitWithSingleString(
            event = this.getEventType().eventName,
            eventData = stringifiedEventRequestData
                              )
    }
}