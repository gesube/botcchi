package hq.waifuparade.botcchi.cytube.emitter.services

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.annotations.CytubeEventEmitter
import hq.waifuparade.botcchi.cytube.emitter.dtos.EventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.JoinChannelEventRequest
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

@Service
@CytubeEventEmitter(OutgoingCytubeEventType.JOIN_CHANNEL)
internal class JoinChannelEventEmitter(
    socketClient: CytubeSocketClient,
    private val cytubePropertiesReader: CytubePropertiesReader
                                      ) : AbstractEventEmitter(socketClient) {

    override suspend fun emitEvent(eventRequestData: EventRequest) {
        val requestData = when {
            (eventRequestData is JoinChannelEventRequest) -> eventRequestData
            else -> JoinChannelEventRequest(name = this.cytubePropertiesReader.room.name)
        }
        this.doEmit(requestData)
    }
}