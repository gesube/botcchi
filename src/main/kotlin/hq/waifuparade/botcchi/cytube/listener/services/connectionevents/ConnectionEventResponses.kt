package hq.waifuparade.botcchi.cytube.listener.services.connectionevents

internal interface ConnectionEventResponse

data class LoginEventResponse(val name: String, val success: Boolean) : ConnectionEventResponse