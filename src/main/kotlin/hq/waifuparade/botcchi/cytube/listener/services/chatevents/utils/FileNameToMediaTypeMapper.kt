package hq.waifuparade.botcchi.cytube.listener.services.chatevents.utils

import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service
import java.net.URLConnection

@Service
@InjectLogger
internal class FileNameToMediaTypeMapper {

    private val fileNameMap = URLConnection.getFileNameMap()

    fun getAndValidateMediaTypeFromFileName(fileName: String): MediaType? {
        this.getMediaTypeFromFileName(fileName)?.let { mediaType ->
            this.validateMediaType(fileName, mediaType)?.let { validatedMediaType ->
                if (validatedMediaType == mediaType) {
                    return mediaType
                }
            }
        }

        return null
    }

    private fun getMediaTypeFromFileName(fileName: String): MediaType? {
        this.detectMimeTypeFromFileName(fileName)?.let { mimeType ->
            return this.getMediaTypeFromMimeType(mimeType)
        }

        return null
    }

    private fun validateMediaType(fileName: String, mediaType: MediaType): MediaType? {
        MediaType.entries.forEach { mediaType ->
            mediaType.fileTypes.forEach { fileType ->
                if (fileName.endsWith(fileType, ignoreCase = true)) {
                    return mediaType
                }
            }
        }

        this.logger.info {
            "Unknown file type ${fileName}. Allowed values are ${
                MediaType.entries
            }"
        }
        return null
    }

    private fun detectMimeTypeFromFileName(fileName: String): String? {
        return this.fileNameMap.getContentTypeFor(fileName)
    }

    private fun getMediaTypeFromMimeType(mimeType: String): MediaType? {
        MediaType.entries.forEach { mediaType ->
            if (mediaType.mimeType.equals(mimeType, ignoreCase = true)) {
                return mediaType
            }
        }

        this.logger.info {
            "Unknown mime type ${mimeType}. Allowed values are ${
                MediaType.entries
            }"
        }
        return null
    }

    enum class MediaType(val mimeType: String, val fileTypes: List<String>) {

        VIDEO_MP4("video/mp4", listOf("mp4")),
        VIDEO_WEBM("video/webm", listOf("webm")),
        VIDEO_OGG("video/ogg", listOf("ogv")),
        VIDEO_QUICKTIME("video/quicktime", listOf("mov")),
        AUDIO_MP4("audio/mp4", listOf("m4a")),
        AUDIO_OGG("audio/ogg", listOf("ogg", "oga")),
        AUDIO_MPEG("audio/mpeg", listOf("mp3")),
        AUDIO_AAC("audio/aac", listOf("aac")),
        APPLICATION_HLS("application/x-mpegURL", listOf("m3u8"));
    }
}