package hq.waifuparade.botcchi.cytube.listener.services.userevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.ConnectionDataApi
import hq.waifuparade.botcchi.datastore.UserDataApi
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.USER_LEAVE)
internal class UserLeaveEventListener(
    socketClient: CytubeSocketClient,
    private val connectionDatabaseApi: ConnectionDataApi,
    private val cytubePropertiesReader: CytubePropertiesReader,
    private val userDataApi: UserDataApi
                                     ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse: UserLeaveEventResponse = JsonMapper.convertStringToObject(eventData[0].toString())
        this.logger.debug { eventResponse }

        when (this.cytubePropertiesReader.user.name == eventResponse.name) {
            true -> {
                this.connectionDatabaseApi.removeLoginTime()
                this.userDataApi.removeAll()
            }
            false -> {
                this.userDataApi.removeByName(eventResponse.name)
            }
        }
    }
}