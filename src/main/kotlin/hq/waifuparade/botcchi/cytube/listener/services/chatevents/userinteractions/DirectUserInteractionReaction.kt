package hq.waifuparade.botcchi.cytube.listener.services.chatevents.userinteractions

import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.MessageEventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.PersonalMessageEventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.PublicMessageEventRequest
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.MessageEvent
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.MessageEventVisibility
import hq.waifuparade.botcchi.datastore.EmoteDataApi
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader

internal abstract class DirectUserInteractionReaction(
    private val emoteDataApi: EmoteDataApi,
    private val cytubePropertiesReader: CytubePropertiesReader
                                                     ) {

    abstract suspend fun reactToInteraction(messageEvent: MessageEvent)

    protected suspend fun handleDirectUserInteraction(
        msg: String, callingUser: String, reactionVisibility: MessageEventVisibility,
        wordsToReactTo: List<String>, reactionWords: List<String>, reactionEmotes: List<String>
                                                     ): MessageEventRequest? {

        // if the bot answers a PM with a PM, it will also generate a second PM and send it to itself
        // hence this disgusting if statement here
        if (callingUser == cytubePropertiesReader.user.name) {
            return null
        }

        wordsToReactTo.forEach {
            val wordRegex = Regex.fromLiteral(it.lowercase())
            if (msg.contains(wordRegex)) {
                val randomEmoteReaction = this.getRandomEmoteReaction(reactionEmotes)
                val randomWordReaction = this.getRandomWordReaction(reactionWords)
                val suitableEmotes = emoteDataApi.filterByNamePrefix(randomEmoteReaction)

                val randomReaction = when(suitableEmotes.isNotEmpty()) {
                    true -> { "${suitableEmotes.random().name} $randomWordReaction" }
                    false -> { randomWordReaction }
                }

                val messageEventRequest: MessageEventRequest = when (reactionVisibility) {
                    MessageEventVisibility.PUBLIC -> { PublicMessageEventRequest(msg = "$randomReaction $callingUser") }
                    MessageEventVisibility.PERSONAL -> { PersonalMessageEventRequest(msg = randomReaction, to = callingUser) }
                }

                return messageEventRequest
            }
        }

        return null
    }

    private fun getRandomEmoteReaction(reactionEmotes: List<String>): String {
        return when(reactionEmotes.isNotEmpty()) {
            true -> { reactionEmotes.random() }
            false -> { "" }
        }
    }

    private fun getRandomWordReaction(reactionWords: List<String>): String {
        return when(reactionWords.isNotEmpty()) {
            true -> { reactionWords.random() }
            false -> { "" }
        }
    }

    protected fun getOutgoingEventType(messageEvent: MessageEvent): OutgoingCytubeEventType {
        return when (messageEvent.visibility) {
            MessageEventVisibility.PUBLIC -> { OutgoingCytubeEventType.CHAT_MSG }
            MessageEventVisibility.PERSONAL -> { OutgoingCytubeEventType.PM }
        }
    }
}