package hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.impls.timer

import hq.waifuparade.botcchi.cytube.listener.services.chatevents.ChatEventResponse
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.ChatCommand
import hq.waifuparade.botcchi.taskscheduling.TaskSchedulingApi
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.util.regex.Pattern

@Service
@InjectLogger
@ConditionalOnProperty(
    name = ["chat.feature.enable.command.timer"],
    havingValue = "true",
    matchIfMissing = false
)
internal class TimerCommand(
    private val chatPropertiesReader: ChatPropertiesReader,
    private val taskScheduler: TaskSchedulingApi,
) : ChatCommand {

    private val timeParameter = "-t"
    private val messageParameter = "-m"
    private val userParameter = "-u"
    private val parameters = listOf(timeParameter, messageParameter, userParameter)
    private val messageRegex = Pattern.compile("-(\\w+)\\s([^\\-]+)")
    private val timeRegex = Pattern.compile("(\\d+)([hms])")
    private val whiteSpaceRegex = Pattern.compile("\\s+")

    override suspend fun getCommandToListenTo(): String {
        return "${this.chatPropertiesReader.general.identifier}${this.chatPropertiesReader.timer.identifier}"
    }

    override suspend fun shouldReact(msg: String): Boolean {
        return msg.startsWith(this.getCommandToListenTo())
    }

    override suspend fun reactToCommand(eventResponse: ChatEventResponse) {
        val messageMap = this.splitMessage(eventResponse.msg)
        var timeToken = ""
        var messageToken = "DING DING DING DING DING DING"
        var userToken = eventResponse.username

        parameters.forEach { parameter ->
            if (parameter.equals(timeParameter, true) && messageMap.containsKey(parameter)) {
                timeToken = messageMap.getValue(parameter)
            }

            if (parameter.equals(messageParameter, true) && messageMap.containsKey(parameter)) {
                messageToken = messageMap.getValue(parameter)
            }

            if (parameter.equals(userParameter, true) && messageMap.containsKey(parameter)) {
                userToken = messageMap.getValue(parameter)
            }
        }

        if (timeToken.isEmpty()) {
            this.logger.debug { "Keine Zeit angegeben!" }
            return
        }

        val delay = this.getDelayFromTimeToken(timeToken)
        val userList = this.getUserListFromToken(userToken)
        this.taskScheduler.scheduleMessages(delay, messageToken, userList)
    }

    private suspend fun splitMessage(message: String): Map<String, String> {
        val matcher = this.messageRegex.matcher(message)
        val messageMap = mutableMapOf<String, String>()

        while (matcher.find()) {
            val key = "-" + matcher.group(1)
            val value = matcher.group(2).trim()
            messageMap[key] = value
        }

        return messageMap
    }

    private suspend fun getDelayFromTimeToken(timeToken: String): Long {
        val matcher = this.timeRegex.matcher(timeToken)
        var delay: Long = 0

        while (matcher.find()) {
            var timeValue = matcher.group(1).toLong()
            val unit: Char = matcher.group(2).toCharArray()[0]

            // Seconds is the lowest metric used - so everything is converted to seconds here
            when(unit) {
                'h' -> timeValue *= 60*60
                'm' -> timeValue *= 60
            }

            // the delay must be in milliseconds, so we multiply the sum of all seconds with 1000
            delay += timeValue*1000
        }

        return delay
    }

    private suspend fun getUserListFromToken(userToken: String): List<String> {
        return userToken.split(this.whiteSpaceRegex).toList()
    }
}