package hq.waifuparade.botcchi.cytube.listener.services.chatevents

internal data class MessageEvent(
    val message: ChatEventResponse,
    val visibility: MessageEventVisibility
)

interface ChatEventResponse {
    val msg: String
    val time: Long
    val username: String
    val meta: Map<String, Any>
}

internal enum class MessageEventVisibility {
    PUBLIC, PERSONAL
}

internal data class PublicMessageEventResponse(
    override val msg: String,
    override val time: Long,
    override val username: String,
    override val meta: Map<String, Any>)
    : ChatEventResponse

internal data class PersonalMessageEventResponse(
    override val msg: String,
    override val time: Long,
    override val username: String,
    override val meta: Map<String, Any>,
    val to: String)
    : ChatEventResponse


internal data class ClearChatEventResponse(
    val clearedBy: String
)