package hq.waifuparade.botcchi.cytube.listener.services

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.enums.StartupTime
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * Handles the listening of the event mentioned in the [CytubeEventListener] annotation.
 * If the listener doesn't have such an annotation, it does nothing.
 */
@InjectLogger
internal abstract class AbstractEventListener(
    private val socketClient: CytubeSocketClient
                                             ) {

    open suspend fun listenToEvent() {
        this.socketClient.on(this.getEventType().eventName) { eventData ->
            CoroutineScope(Job()).launch {
                this@AbstractEventListener.handleEvent(eventData)
            }
        }
    }

    fun stopListeningToEvent() {
        this.socketClient.off(this.getEventType().eventName)
    }

    protected open suspend fun handleEvent(eventData: Array<Any>) {
        this.logger.info { "------------------------------" }
        this.logger.info { this.javaClass.simpleName }
        this.logger.info { eventData[0] }
        this.logger.info { "------------------------------" }
    }

    private fun getEventType(): IngoingCytubeEventType {
        val cytubeEventAnnotation =
            this.javaClass.annotations.find { it is CytubeEventListener } as CytubeEventListener
        return cytubeEventAnnotation.eventType
    }

    fun getStartupTime(): StartupTime {
        val cytubeEventAnnotation =
            this.javaClass.annotations.find { it is CytubeEventListener } as CytubeEventListener
        return cytubeEventAnnotation.startupTime
    }
}