package hq.waifuparade.botcchi.cytube.listener.services.permissionevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.PermissionDataApi
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.SET_PERMISSION)
internal class SetPermissionEventListener(
    socketClient: CytubeSocketClient,
    private val permissionDataApi: PermissionDataApi
                                         ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse =
            JsonMapper.convertStringToObject<SetPermissionEventResponse>(eventData[0].toString())
        this.logger.debug { eventResponse }

        val dtos = eventResponse.javaClass.declaredFields.map {
            it.isAccessible = true
            val dto = PermissionDto(name = it.name, minRequiredRank = it.getDouble(eventResponse))
            it.isAccessible = false
            dto
        }.toList()

        this.permissionDataApi.saveAll(dtos)
    }
}