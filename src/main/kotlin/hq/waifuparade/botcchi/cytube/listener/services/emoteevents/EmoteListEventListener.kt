package hq.waifuparade.botcchi.cytube.listener.services.emoteevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.EmoteDataApi
import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import okhttp3.internal.toImmutableList
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.EMOTE_LIST)
internal class EmoteListEventListener(
    socketClient: CytubeSocketClient,
    private val emoteDataApi: EmoteDataApi
                                     ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse =
            JsonMapper.convertStringToObject<List<EmoteEventResponse>>(eventData[0].toString())
        this.logger.debug { eventResponse }

        val emoteDtoList = eventResponse.map { emoteEventResponse ->
            EmoteDto(name = emoteEventResponse.name, image = emoteEventResponse.image)
        }.toImmutableList()

        this.emoteDataApi.saveAll(emoteDtoList)
    }
}