package hq.waifuparade.botcchi.cytube.listener.services.mediaevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.CHANGE_MEDIA)
internal class ChangeMediaEventListener(
    socketClient: CytubeSocketClient
                                       ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse = JsonMapper.convertStringToObject<MediaEventResponse>(eventData[0].toString())
        this.logger.info { eventResponse }
    }
}