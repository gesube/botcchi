package hq.waifuparade.botcchi.cytube.listener.services.connectionevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.ConnectionDataApi
import hq.waifuparade.botcchi.datastore.EmoteDataApi
import hq.waifuparade.botcchi.datastore.PermissionDataApi
import hq.waifuparade.botcchi.datastore.UserDataApi
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.DISCONNECT)
internal class DisconnectEventListener(
    socketClient: CytubeSocketClient,
    private val connectionDataApi: ConnectionDataApi,
    private val emoteDataApi: EmoteDataApi,
    private val userDataApi: UserDataApi,
    private val permissionDataApi: PermissionDataApi
                                      ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        this.logger.info { "Socket has disconnected." }
        this.logger.debug { eventData[0] }

        this.connectionDataApi.removeLoginTime()
        this.connectionDataApi.setPlaylistOpen(false)
        this.emoteDataApi.removeAll()
        this.userDataApi.removeAll()
        this.permissionDataApi.removeAll()
    }
}