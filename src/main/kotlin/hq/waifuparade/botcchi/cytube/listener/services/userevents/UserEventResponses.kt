package hq.waifuparade.botcchi.cytube.listener.services.userevents

internal data class UserEventResponse(val name: String, val rank: Double, val profile: UserProfile, val meta: Map<String, Any>)
internal data class UserProfile(val image: String, val text: String)
internal data class SetUserRankEventResponse(val name: String, val rank: Double)
internal data class SetAfkEventResponse(val name: String, val afk: Boolean)
internal data class UserLeaveEventResponse(val name: String)