package hq.waifuparade.botcchi.cytube.listener.services.mediaevents

import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.PublicMessageEventRequest
import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.enums.StartupTime
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.SET_PLAYLIST_LOCKED, startupTime = StartupTime.DEFERRED)
internal class SetPlaylistLockedEventListener(
    socketClient: CytubeSocketClient,
    private val emitterApi: EmitterApi,
                                             ) : AbstractEventListener(socketClient) {

    private val hasBeenLockedLogMessage = "Playlist ist gesperrt. Es können keine neuen Videos hinzugefügt werden."
    private val hasBeenUnlockedLogMessage = "Playlist ist entsperrt. Es können wieder neue Videos hinzugefügt werden."

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse = JsonMapper.convertStringToObject<Boolean>(eventData[0].toString())
        var logMessage: String

        if (eventResponse) {
            logMessage = this.hasBeenLockedLogMessage
        } else {
            logMessage = this.hasBeenUnlockedLogMessage
        }

        this.logger.info { logMessage }
        val publicMessage = PublicMessageEventRequest(msg =  logMessage)
        this.emitterApi.callEventEmitter(OutgoingCytubeEventType.CHAT_MSG, publicMessage, 50)
    }
}