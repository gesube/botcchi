package hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.impls.media

import hq.waifuparade.botcchi.auth.AuthApi
import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.emitter.dtos.PersonalMessageEventRequest
import hq.waifuparade.botcchi.cytube.emitter.dtos.QueueMediaEventRequest
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.ChatEventResponse
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.ChatCommand
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.utils.FileNameToMediaTypeMapper
import hq.waifuparade.botcchi.utils.HttpConnector
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import okhttp3.Response
import org.jsoup.Jsoup
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.net.URI
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import kotlin.collections.getValue

@Service
@InjectLogger
@ConditionalOnProperty(
    name = ["chat.feature.enable.command.add-media"],
    havingValue = "true",
    matchIfMissing = false
                      )
internal class MediaAddChatCommand(
    private val chatPropertiesReader: ChatPropertiesReader,
    private val fileNameToMediaTypeMapper: FileNameToMediaTypeMapper,
    private val emitterApi: EmitterApi,
    private val authApi: AuthApi
                                  ) : ChatCommand {

    private val positionParameter = "-pos"
    private val dirParameter = "-d"
    private val fileParameter = "-f"
    private val parameters = listOf(positionParameter, dirParameter, fileParameter)
    private val messageRegex = Pattern.compile("(-pos|-d|-f)\\s+(end|next|(https://\\S+\\s*)+)")
    private val httpRegex = Pattern.compile("""\s+(?=http)""")

    override suspend fun getCommandToListenTo(): String {
        return "${this.chatPropertiesReader.general.identifier}${this.chatPropertiesReader.addMedia.identifier}"
    }

    override suspend fun shouldReact(msg: String): Boolean {
        return msg.startsWith(this.getCommandToListenTo())
    }

    override suspend fun reactToCommand(eventResponse: ChatEventResponse) {
        if (!hasPermissionForCommand(userName = eventResponse.username)) {
            this.logger.warn { "${eventResponse.username} hat versucht Videos zu addieren, besitzt jedoch keine Rechte dazu." }
            return
        }

        val messageMap = this.splitMessage(eventResponse.msg)

        var posToken = ""
        var dirToken = ""
        var fileToken = ""

        parameters.forEach { parameter ->
            if (parameter.equals(positionParameter, true) && messageMap.containsKey(parameter)) {
                posToken = messageMap.getValue(parameter)
            }

            if (parameter.equals(dirParameter, true) && messageMap.containsKey(parameter)) {
                dirToken = messageMap.getValue(parameter)
            }

            if (parameter.equals(fileParameter, true) && messageMap.containsKey(parameter)) {
                fileToken = messageMap.getValue(parameter)
            }
        }

        val pos = this.getInsertPosition(posToken)
        val dirList = this.getDirListFromToken(dirToken)
        val fileList = this.getFileListFromToken(fileToken)

        dirList.forEach { token ->
            val response = this.tryGetRequest(token, eventResponse.username)

            response?.let {
                val moreFilesList = this.processSuccessfulDirResponse(response)
                fileList.addAll(moreFilesList)
                it.close()
            }
        }

        fileList.forEach { token ->
            val response = this.tryGetRequest(token, eventResponse.username)

            response?.let {
                this.processMediaFile(response, pos)
                it.close()
            }
        }

        val pmRequest = PersonalMessageEventRequest(msg = "I tried adding all the videos.", to = eventResponse.username)
        this.emitterApi.callEventEmitter(OutgoingCytubeEventType.PM, pmRequest, 50)
    }

    private suspend fun hasPermissionForCommand(userName: String): Boolean {
        return this.authApi.hasPermissionForAction(ApplicationAction.ADD_MEDIA, userName)
    }

    private suspend fun splitMessage(message: String): Map<String, String> {
        val matcher = this.messageRegex.matcher(message)
        val messageMap = mutableMapOf<String, String>()

        while (matcher.find()) {
            val key = matcher.group(1)
            val value = matcher.group(2).trim()
            messageMap[key] = value
        }

        return messageMap
    }

    private suspend fun getInsertPosition(posToken: String): String {
        return when (posToken) {
            "next" -> "next"
            "end" -> "end"
            else -> "end"
        }
    }

    private suspend fun getDirListFromToken(dirToken: String): List<String> {
        return this.splitHttpTokens(dirToken)
    }

    private suspend fun getFileListFromToken(fileToken: String): MutableList<String> {
        return this.splitHttpTokens(fileToken)
    }

    private suspend fun splitHttpTokens(token: String): MutableList<String> {
        return token.split(this.httpRegex).filter { regexToken ->
            regexToken.startsWith("https")                  // only HTTPS is allowed by cytube
        }.toMutableList()
    }

    private suspend fun tryGetRequest(urlString: String, notificationUser: String): Response? {
        val response = HttpConnector.doGetRequest(urlString)
        if (!response.isSuccessful) {
            this.logger.info { "$urlString is not available." }
            val pmRequest = PersonalMessageEventRequest(msg = "I could not add some of the videos.", to = notificationUser)
            this.emitterApi.callEventEmitter(OutgoingCytubeEventType.PM, pmRequest, 50)
            response.close()
            return null
        }
        return response
    }

    private suspend fun processSuccessfulDirResponse(response: Response): List<String> {
        val contentType = response.headers["content-type"]
        contentType?.let {
            val splitContentType = it.split(";")

            return when (splitContentType[0]) {
                "text/html" -> {
                    this.processTextFile(response)
                }
                else -> {
                    emptyList()
                }
            }
        }

        return emptyList()
    }

    private suspend fun processTextFile(response: Response): List<String> {
        val fileUrls = mutableListOf<String>()

        response.body?.let { responseBody ->
            val document = Jsoup.parse(responseBody.string())

            document.body().getElementsByTag("a").forEach { anchorElement ->
                val link = response.request.url.toString()
                val href = anchorElement.attr("href")
                this.fileNameToMediaTypeMapper.getAndValidateMediaTypeFromFileName(href) ?: return@forEach

                fileUrls.add(URI.create("$link$href").toURL().toExternalForm())
            }
        }

        return fileUrls
    }

    private suspend fun processMediaFile(response: Response, pos: String) {
        val url = response.request.url.toUrl().toExternalForm()
        val startOfFileName = url.lastIndexOf('/') + 1
        val endOfFileName = url.lastIndexOf('.')
        val encodedFileName = url.substring(startOfFileName, endOfFileName)
        val decodedFileName = URLDecoder.decode(encodedFileName, StandardCharsets.UTF_8)

        val eventRequest = this.buildEventRequest(
            id = url,
            pos = pos,
            title = decodedFileName
                                                 )

        this.sendMediaAddRequestToServer(eventRequest)
    }

    private suspend fun buildEventRequest(id: String, pos: String, title: String): QueueMediaEventRequest {
        return QueueMediaEventRequest(
            id = id,
            type = "fi",
            pos = pos,
            title = title,
            temp = true
                                     )
    }

    private suspend fun sendMediaAddRequestToServer(eventRequest: QueueMediaEventRequest) {
        val delayTime = TimeUnit.SECONDS.toMillis(this.chatPropertiesReader.addMedia.cooldownBetweenAdditions)
        this.emitterApi.callEventEmitter(OutgoingCytubeEventType.QUEUE, eventRequest, delayTime)
    }
}