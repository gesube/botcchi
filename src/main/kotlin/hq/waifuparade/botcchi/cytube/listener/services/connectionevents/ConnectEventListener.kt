package hq.waifuparade.botcchi.cytube.listener.services.connectionevents

import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.CONNECT)
internal class ConnectEventListener(
    socketClient: CytubeSocketClient,
    private val emitterApi: EmitterApi
                                   ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        this.logger.info { "Socket has connected." }
        this.emitterApi.callEventEmitter(OutgoingCytubeEventType.JOIN_CHANNEL)
        this.emitterApi.callEventEmitter(OutgoingCytubeEventType.LOGIN)
    }
}