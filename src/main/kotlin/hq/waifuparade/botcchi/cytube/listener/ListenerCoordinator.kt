package hq.waifuparade.botcchi.cytube.listener

import hq.waifuparade.botcchi.cytube.listener.enums.StartupTime
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.events.models.ApplicationEvent
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

/**
 * Coordinates everything related to the cytube listeners.
 * The coordinator isn't meant to be called from the outside.
 * Instead, it listens on [ApplicationEvent]s and does its things when the right conditions are met.
 */
@Service
@InjectLogger
internal class ListenerCoordinator(
    private val eventListeners: List<AbstractEventListener>,
    private val cytubePropertiesReader: CytubePropertiesReader
                                  ) {

    /**
     * Listens on the [hq.waifuparade.botcchi.events.enums.ApplicationEventType.SOCKET_CONNECTED] application event.
     * Registers all early cytube socket listener (= with [StartupTime] = [StartupTime.IMMEDIATE])
     * @param event - the [ApplicationEvent] that causes the execution of this function
     */
    @OptIn(DelicateCoroutinesApi::class)
    @EventListener(condition = "#event.eventType == T(hq.waifuparade.botcchi.events.enums.ApplicationEventType).SOCKET_CONNECTED")
    fun registerEarlyListeners(event: ApplicationEvent) {
        GlobalScope.launch {
            this@ListenerCoordinator.logger.info { "Register early listeners..." }

            this@ListenerCoordinator.eventListeners.filter { listener ->
                listener.getStartupTime() == StartupTime.IMMEDIATE
            }.forEach { listener ->
                listener.listenToEvent()
            }

            this@ListenerCoordinator.logger.info { "Early listeners registered." }
        }
    }

    /**
     * Listens on the [hq.waifuparade.botcchi.events.enums.ApplicationEventType.CHANNEL_LOGIN] application event.
     * Registers all late cytube socket listener (= with [StartupTime] = [StartupTime.DEFERRED])
     * @param event - the [ApplicationEvent] that causes the execution of this function
     */
    @OptIn(DelicateCoroutinesApi::class)
    @EventListener(condition = "#event.eventType == T(hq.waifuparade.botcchi.events.enums.ApplicationEventType).CHANNEL_LOGIN")
    fun registerLateListener(event: ApplicationEvent) {
        GlobalScope.launch {
            delay(TimeUnit.SECONDS.toMillis(this@ListenerCoordinator.cytubePropertiesReader.socket.listenerDelay)) // .socketListenerDelay))
            this@ListenerCoordinator.logger.info { "Register late listeners..." }

            this@ListenerCoordinator.eventListeners.filter { listener ->
                listener.getStartupTime() == StartupTime.DEFERRED
            }.forEach { listener ->
                listener.listenToEvent()
            }

            this@ListenerCoordinator.logger.info { "Late listeners registered." }
        }
    }

    /**
     * Listens on the [hq.waifuparade.botcchi.events.enums.ApplicationEventType.SOCKET_DISCONNECTED] application event.
     * Deregisters all cytube socket listener
     * @param event - the [ApplicationEvent] that causes the execution of this function
     */
    @OptIn(DelicateCoroutinesApi::class)
    @EventListener(condition = "#event.eventType == T(hq.waifuparade.botcchi.events.enums.ApplicationEventType).SOCKET_DISCONNECTED")
    fun deregisterAllListeners(event: ApplicationEvent) {
        GlobalScope.launch {
            this@ListenerCoordinator.logger.info { "Deregister all listeners..." }
            this@ListenerCoordinator.eventListeners.forEach {
                it.stopListeningToEvent()
            }

            this@ListenerCoordinator.logger.info { "All listeners deregistered." }
        }
    }
}