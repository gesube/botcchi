package hq.waifuparade.botcchi.cytube.listener.services.userevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.UserDataApi
import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.USERLIST)
internal class UserListEventListener(
    socketClient: CytubeSocketClient,
    private val userDataApi: UserDataApi
                                    ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse: List<UserEventResponse> = JsonMapper.convertStringToObject(eventData[0].toString())

        this.convertUserEventResponsesToUserRankDtos(eventResponse).let { userRankDtos ->
            this.logger.debug { userRankDtos }
            this.userDataApi.saveAll(userRankDtos)
        }
    }

    private suspend fun convertUserEventResponsesToUserRankDtos(userEventResponses: List<UserEventResponse>): List<UserDto> {
        return userEventResponses.map { userEventResponse ->
            UserDto(name = userEventResponse.name, rank = userEventResponse.rank)
        }.toList()
    }
}