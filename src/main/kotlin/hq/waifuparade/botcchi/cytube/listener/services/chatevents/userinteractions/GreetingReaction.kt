package hq.waifuparade.botcchi.cytube.listener.services.chatevents.userinteractions

import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.MessageEvent
import hq.waifuparade.botcchi.datastore.EmoteDataApi
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatDirectUserInteractionsPropertiesReader
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(
    name = ["chat.feature.enable.direct-user-interaction.greeting"],
    havingValue = "true",
    matchIfMissing = false
                      )
internal class GreetingReaction(
    emoteDataApi: EmoteDataApi,
    private val emitterApi: EmitterApi,
    private val userInteractionsPropertiesReader: ChatDirectUserInteractionsPropertiesReader,
    private val cytubePropertiesReader: CytubePropertiesReader
                               ) : DirectUserInteractionReaction(emoteDataApi, cytubePropertiesReader) {

    override suspend fun reactToInteraction(messageEvent: MessageEvent) {
        val eventResponse = messageEvent.message
        this.handleDirectUserInteraction(
            msg = eventResponse.msg,
            callingUser = eventResponse.username,
            reactionVisibility = messageEvent.visibility,
            wordsToReactTo = this.userInteractionsPropertiesReader.greetingWords,
            reactionWords = this.userInteractionsPropertiesReader.greetingReactionWords,
            reactionEmotes = this.userInteractionsPropertiesReader.greetingReactionEmotes
                                        )?.let {
            this.emitterApi.callEventEmitter(this.getOutgoingEventType(messageEvent), it, 50)
        }
    }
}