package hq.waifuparade.botcchi.cytube.listener.enums

/**
 * Enum with all possible cytube events to listen to.
 */
internal enum class IngoingCytubeEventType(val eventName: String) {

    // general
    CONNECT("connect"),
    CONNECT_ERROR("connect_error"),
    DISCONNECT("disconnect"),
    ERROR_MSG("errorMsg"),

    // accesscontrol.js
    NEED_PASSWORD("needPassword"),
    CANCEL_NEED_PASSWORD("cancelNeedPassword"),

    // channel.js
    ADD_USER("addUser"),
    SET_USER_RANK("setUserRank"),
    USER_LEAVE("userLeave"),
    USERCOUNT("usercount"),
    SET_AFK("setAFK"),
    USERLIST("userlist"),
    LOGIN("login"),
    SET_FLAG("setFlag"),
    CLEAR_FLAG("clearFlag"),
    READ_CHAN_LOG("readChanLog"),
    CHANNEL_NOT_REGISTERED("channelNotRegistered"),
    WARN_LARGE_CHAN_DUMP("warnLargeChandump"),
    LOAD_FAIL("loadFail"),

    // chat.js
    CHAT_MSG("chatMsg"),
    PM("pm"),
    CLEAR_CHAT("clearchat"),
    NO_FLOOD("noFlood"),
    COOLDOWN("coolDown"),
    SPAM_FILTERED("spamFiltered"),

    // customization.js
    CHANNEL_CSS_JS("channelCSSJS"),
    SET_MOTD("setMotd"),

    // drink.js
    DRINK_COUNT("drinkCount"),

    // emotes.js
    EMOTE_LIST("emoteList"),
    REMOVE_EMOTE("removeEmote"),
    RENAME_EMOTE("renameEmote"),
    UPDATE_EMOTE("updateEmote"),

    // filters.js
    CHAT_FILTERS("chatFilters"),
    ADD_FILTER_SUCCESS("addFilterSuccess"),
    UPDATE_CHAT_FILTER("updateChatFilter"),
    DELETE_CHAT_FILTER("deleteChatFilter"),

    // kickban.js
    BANLIST("banlist"),
    BANLIST_REMOVE("banlistRemove"),
    CONSTANZA("constanza"), // "you can't ban yourself"

    // library.js
    SEARCH_RESULSTS("searchResults"),

    // opts.js
    CHANNEL_OPTS("channelOpts"),
    VALIDATION_PASSED("validationPassed"),
    VALIDATION_ERROR("validationError"),

    // permission.js
    SET_PERMISSION("setPermissions"),
    SET_PLAYLIST_LOCKED("setPlaylistLocked"),

    // player
    PLAY_NEXT("playNext"),

    // playlist.js
    SET_LEADER("setLeader"),
    SET_CURRENT("setCurrent"),
    CHANGE_MEDIA("changeMedia"),
    MEDIA_UPDATE("mediaUpdate"),
    SET_TEMP("setTemp"),
    MOVE_VIDEO("moveVideo"),
    PLAYLIST("playlist"),
    SET_PLAYLIST_META("setPlaylistMeta"),
    DELETE("delete"), //deleteMedia
    QUEUE("queue"), //addMedia
    QUEUE_FAIL("queueFail"),
    QUEUE_WARN("queueWarn"),
    LIST_PLAYLISTS("listPlaylists"),

    // poll.js
    UPDATE_POLL("updatePoll"),
    CLOSE_POLL("closePoll"),
    NEW_POLL("newPoll"),

    // ranks.js
    CHANNEL_RANKS("channelRanks"),
    CHANNEL_RANK_FAIL("channelRankFail"),
    EFFECTIVE_RANK_CHANGE("effectiveRankChange"),
    RANK("rank"),

    // voteskip.js
    VOTESKIP("voteskip"),
    CLEAR_VOTESKIP_VOTE("clearVoteskipVote");
}