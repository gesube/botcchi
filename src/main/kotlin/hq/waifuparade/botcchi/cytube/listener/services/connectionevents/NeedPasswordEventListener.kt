package hq.waifuparade.botcchi.cytube.listener.services.connectionevents

import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.emitter.OutgoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

/**
 * Listener is called when the channel to join requires a password.
 */
@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.NEED_PASSWORD)
internal class NeedPasswordEventListener(
    private val socketClient: CytubeSocketClient,
    private val emitterApi: EmitterApi,
    private val cytubePropertiesReader: CytubePropertiesReader
                                        ) : AbstractEventListener(socketClient) {

    var numberOfRetries: Int = 0

    override suspend fun handleEvent(eventData: Array<Any>) {
        // The event Data is either
        //  false - in case it's the first time / attempt
        //  true - all consecutive times / attempts
        // Don't really know why cytube behaves like this, but either way the content doesn't matter

        this.logger.info { "A password is required to join the channel." }

        if (numberOfRetries > 3) {
            this.logger.info { "But there were 3 consecutive unsuccessful password attemps." }
            this.logger.info { "You most likely provided the wrong channel password in the config." }
            this.logger.info { "${cytubePropertiesReader.user.name} will now disconnect." }
            this.socketClient.disconnectFromSocketServer()
            return
        }

        this.logger.info { "Sending password." }
        this.emitterApi.callEventEmitter(outEvent = OutgoingCytubeEventType.CHANNEL_PASSWORD)
        this.numberOfRetries++
    }
}