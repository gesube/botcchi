package hq.waifuparade.botcchi.cytube.listener.enums

/**
 * Enum with all possible startup times for cytube event listeners.
 * Some listeners can be started immediately, some must be deferred to avoid problems on startup.
 */
internal enum class StartupTime {

    IMMEDIATE,
    DEFERRED
}