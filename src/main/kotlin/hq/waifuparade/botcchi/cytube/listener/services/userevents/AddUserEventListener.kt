package hq.waifuparade.botcchi.cytube.listener.services.userevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.ConnectionDataApi
import hq.waifuparade.botcchi.datastore.UserDataApi
import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.events.ApplicationEventsApi
import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.ADD_USER)
internal class AddUserEventListener(
    socketClient: CytubeSocketClient,
    private val connectionDatabaseApi: ConnectionDataApi,
    private val userDataApi: UserDataApi,
    private val cytubePropertiesReader: CytubePropertiesReader,
    private val applicationEventsApi: ApplicationEventsApi
                                   ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse: UserEventResponse = JsonMapper.convertStringToObject(eventData[0].toString())
        val timeOfLogin = System.nanoTime()
        this.logger.debug { eventResponse }

        val userDto = UserDto(name = eventResponse.name, rank = eventResponse.rank)

        when (this.cytubePropertiesReader.user.name == eventResponse.name) {
            true -> {
                this.applicationEventsApi.publishEvent(ApplicationEventType.CHANNEL_LOGIN)
                this.connectionDatabaseApi.setLoginTime(timeOfLogin)
                this.userDataApi.updateByName(userDto)
            }
            false -> {
                this.userDataApi.save(userDto)
            }
        }
    }
}