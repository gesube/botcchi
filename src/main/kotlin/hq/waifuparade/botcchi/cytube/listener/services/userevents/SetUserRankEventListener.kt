package hq.waifuparade.botcchi.cytube.listener.services.userevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.UserDataApi
import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.SET_USER_RANK)
internal class SetUserRankEventListener(
    socketClient: CytubeSocketClient,
    private val userDataApi: UserDataApi
                                       ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse =
            JsonMapper.convertStringToObject<SetUserRankEventResponse>(eventData[0].toString())
        this.logger.debug { eventResponse }

        val userRankDto = UserDto(name = eventResponse.name, rank = eventResponse.rank)
        this.userDataApi.updateByName(userRankDto)
    }
}