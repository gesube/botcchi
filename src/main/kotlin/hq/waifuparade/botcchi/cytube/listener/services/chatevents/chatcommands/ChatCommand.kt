package hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands

import hq.waifuparade.botcchi.cytube.listener.services.chatevents.ChatEventResponse

interface ChatCommand {

    suspend fun getCommandToListenTo(): String

    suspend fun shouldReact(msg: String): Boolean

    suspend fun reactToCommand(eventResponse: ChatEventResponse)
}