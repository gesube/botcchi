package hq.waifuparade.botcchi.cytube.listener.services.chatevents

import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.ChatCommand
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.userinteractions.DirectUserInteractionReaction
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader

internal abstract class AbstractMessageEventListener(
    socketClient: CytubeSocketClient,
    cytubePropertiesReader: CytubePropertiesReader,
    private val chatPropertiesReader: ChatPropertiesReader,
    private val userInteractionReactions: List<DirectUserInteractionReaction>,
    private val chatCommands: List<ChatCommand>,
                                                    ) : AbstractEventListener(socketClient) {

    val commandIdentifier = this.chatPropertiesReader.general.identifier
    val userNameRegex = Regex.fromLiteral(cytubePropertiesReader.user.name.lowercase())

    protected abstract fun getResponsibleMessageEventVisibility(): MessageEventVisibility

    protected abstract fun parseMessageEvent(eventData: Array<Any>): ChatEventResponse

    protected abstract fun shouldReactToInteraction(parsedMsg: String, userNameRegex: Regex): Boolean

    protected fun shouldReactToCommand(parsedMsg: String, commandIdentifier: String): Boolean {
        return parsedMsg.startsWith(commandIdentifier)
    }

    override suspend fun handleEvent(eventData: Array<Any>) {
        val parsedEventResponse = this.parseMessageEvent(eventData)
        this.logger.info { parsedEventResponse }
        val parsedMsg = parsedEventResponse.msg
        val messageEvent = MessageEvent(message = parsedEventResponse,
                                        visibility = this.getResponsibleMessageEventVisibility())

        if (this.shouldReactToInteraction(parsedMsg, userNameRegex)) {
            this.userInteractionReactions.forEach {
                it.reactToInteraction(messageEvent)
            }
        }

        if (this.shouldReactToCommand(parsedMsg, commandIdentifier)) {
            this.chatCommands.filter {
                it.shouldReact(parsedMsg)
            }.forEach {
                it.reactToCommand(messageEvent.message)
            }
        }
    }
}