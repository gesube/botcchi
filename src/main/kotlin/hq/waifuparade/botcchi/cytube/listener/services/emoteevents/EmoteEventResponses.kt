package hq.waifuparade.botcchi.cytube.listener.services.emoteevents

internal interface AbstractEmoteEventResponse {
    val image: String
    val name: String
    val source: String }

internal data class EmoteEventResponse(
    override val image: String,
    override val name: String,
    override val source: String)
    : AbstractEmoteEventResponse

internal data class UpdateEmoteEventResponse(
    override val image: String,
    override val name: String,
    override val source: String)
    : AbstractEmoteEventResponse

internal data class RenameEmoteEventResponse(
    override val image: String,
    override val name: String,
    override val source: String,
    val old: String)
    : AbstractEmoteEventResponse

internal data class RemoveEmoteEventResponse(
    override val image: String,
    override val name: String,
    override val source: String,
    val regex: Map<String, Any> = mapOf())
    : AbstractEmoteEventResponse

// RemoveEmoteResponse = RemoveEmoteRequest