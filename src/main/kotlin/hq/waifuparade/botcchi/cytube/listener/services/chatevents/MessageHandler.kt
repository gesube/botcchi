package hq.waifuparade.botcchi.cytube.listener.services.chatevents

import hq.waifuparade.botcchi.cytube.emitter.EmitterApi
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.ChatCommand
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.userinteractions.DirectUserInteractionReaction
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.jsoup.Jsoup

@InjectLogger
internal class MessageHandler(
    private val cytubePropertiesReader: CytubePropertiesReader,
    private val chatPropertiesReader: ChatPropertiesReader,
    private val userInteractionReactions: List<DirectUserInteractionReaction>,
    private val chatCommands: List<ChatCommand>,
    private val emitterApi: EmitterApi,
                            ) {

    val commandIdentifier = this.chatPropertiesReader.general.identifier
    val userNameRegex = Regex.fromLiteral(cytubePropertiesReader.user.name.lowercase())

    suspend fun handleMessage(messageEvent: MessageEvent) {
        this.logger.info { messageEvent }
        val parsedEventResponse = this.parseMessageEvent(messageEvent)
        val parsedMsg = parsedEventResponse.message.msg

        // react to interactions if
        // - 1. the bot is being called directy in the public chat
        // - 2. the bot is being called via private message
        if ((parsedEventResponse.visibility == MessageEventVisibility.PUBLIC && parsedMsg.contains(this.userNameRegex))
                || parsedEventResponse.visibility == MessageEventVisibility.PERSONAL) {

            this.userInteractionReactions.forEach {
                it.reactToInteraction(parsedEventResponse)
            }
        }

        if (parsedMsg.startsWith(commandIdentifier)) {
            this.chatCommands.forEach {
                it.reactToCommand(parsedEventResponse.message)
            }
        }

        /*val wrongNames = listOf("Kmapfu", "Kmapfi", "Kurampfu", "Kempfu", "Senpfu", "Senfu")

        if (parsedEventResponse.username == this.cytubePropertiesReader.user.name) {
            return
        }

        wrongNames.forEach {
            val startIndex = parsedMsg.indexOf(it, ignoreCase = true)

            if (startIndex < 0) {
                return@forEach
            }

            val endIndex = startIndex + it.length
            val substring = parsedMsg.substring(startIndex, endIndex)
            val request = MessageEventRequest(msg = "Der arme Jung heißt nicht $substring! /saueruto")
            this.emitterApi.callEventEmitter(OutgoingCytubeEventType.CHAT_MSG, request, 25)
        }*/
    }

    private fun parseMessageEvent(messageEvent: MessageEvent): MessageEvent {
        val message = messageEvent.message
        val parsedMsg = Jsoup.parse(message.msg.lowercase()).text()

        val parsedEventResponse = when (message) {
            is PublicMessageEventResponse -> { message.copy(msg = parsedMsg) }
            else -> { (message as PersonalMessageEventResponse).copy(msg = parsedMsg) }
        }

        return messageEvent.copy(message = parsedEventResponse)
    }
}