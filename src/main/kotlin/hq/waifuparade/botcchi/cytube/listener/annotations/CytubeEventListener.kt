package hq.waifuparade.botcchi.cytube.listener.annotations

import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.enums.StartupTime

/**
 * Annotation to register a class as a listener for cytube events.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class CytubeEventListener(
    val eventType: IngoingCytubeEventType,
    val startupTime: StartupTime = StartupTime.IMMEDIATE
                                             )