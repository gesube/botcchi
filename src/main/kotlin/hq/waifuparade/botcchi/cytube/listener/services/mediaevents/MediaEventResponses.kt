package hq.waifuparade.botcchi.cytube.listener.services.mediaevents

internal data class MediaEventResponse(val currentTime: Double,
                              val duration: String,
                              val id: String,
                              val meta: Map<String, Any>,
                              val paused: Boolean,
                              val seconds: Int,
                              val title: String,
                              val type: String)

internal data class PlaylistEventResponse(val media: MediaEventResponse,
                                          val queueby: String,
                                          val temp: Boolean,
                                          val uid: Int)

internal data class MediaUpdateEventResponse(val currentTime: Double, val paused: Boolean)


internal data class QueueSuccessEventResponse(val item: QueueItem, val after: Int)
internal data class QueueItem(val uid: Int, val temp: Boolean, val queueby: String, val media: QueuedMedia)
internal data class QueuedMedia(val duration: String, val seconds: Int, val meta: Map<String, Any>, val id: String, val title: String, val type: String)
internal data class QueueFailEventResponse(val msg: String, val link: String, val id: String)