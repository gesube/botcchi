package hq.waifuparade.botcchi.cytube.listener.services.chatevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.chatcommands.ChatCommand
import hq.waifuparade.botcchi.cytube.listener.services.chatevents.userinteractions.DirectUserInteractionReaction
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.ChatPropertiesReader
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.jsoup.Jsoup
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.PM)
internal class PMEventListener(
    socketClient: CytubeSocketClient,
    cytubePropertiesReader: CytubePropertiesReader,
    chatPropertiesReader: ChatPropertiesReader,
    userInteractionReactions: List<DirectUserInteractionReaction>,
    chatCommands: List<ChatCommand>,
                              )
    : AbstractMessageEventListener(socketClient, cytubePropertiesReader, chatPropertiesReader,
                                   userInteractionReactions, chatCommands) {

    override fun getResponsibleMessageEventVisibility(): MessageEventVisibility {
        return MessageEventVisibility.PERSONAL
    }

    override fun parseMessageEvent(eventData: Array<Any>): PersonalMessageEventResponse {
        val eventResponse =
            JsonMapper.convertStringToObject<PersonalMessageEventResponse>(eventData[0].toString())

        val parsedMsg = Jsoup.parse(eventResponse.msg.lowercase()).text()
        return eventResponse.copy(msg = parsedMsg)
    }

    override fun shouldReactToInteraction(parsedMsg: String, userNameRegex: Regex): Boolean {
        return true
    }
}