package hq.waifuparade.botcchi.cytube.listener.services.emoteevents

import hq.waifuparade.botcchi.cytube.listener.annotations.CytubeEventListener
import hq.waifuparade.botcchi.cytube.listener.enums.IngoingCytubeEventType
import hq.waifuparade.botcchi.cytube.listener.services.AbstractEventListener
import hq.waifuparade.botcchi.datastore.EmoteDataApi
import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

@Service
@InjectLogger
@CytubeEventListener(eventType = IngoingCytubeEventType.REMOVE_EMOTE)
internal class RemoveEmoteEventListener(
    socketClient: CytubeSocketClient,
    private val emoteDataApi: EmoteDataApi
                                       ) : AbstractEventListener(socketClient) {

    override suspend fun handleEvent(eventData: Array<Any>) {
        val eventResponse =
            JsonMapper.convertStringToObject<RemoveEmoteEventResponse>(eventData[0].toString())
        this.logger.debug { eventResponse }

        val emoteDto = EmoteDto(name = eventResponse.name, image = eventResponse.image)
        this.emoteDataApi.remove(emoteDto)
    }
}