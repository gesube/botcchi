package hq.waifuparade.botcchi.datastore.dtos

data class EmoteDto(
    val name: String,
    val image: String
                   )