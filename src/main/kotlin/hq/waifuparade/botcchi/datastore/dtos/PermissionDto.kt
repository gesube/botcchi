package hq.waifuparade.botcchi.datastore.dtos

data class PermissionDto(
    var name: String = "",
    var minRequiredRank: Double = Double.MAX_VALUE
                        )