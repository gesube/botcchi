package hq.waifuparade.botcchi.datastore.dtos

data class UserDto(
    val name: String,
    val rank: Double
                  )