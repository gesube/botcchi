package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.datastore.services.EmoteDataHandler
import org.springframework.stereotype.Service

@Service
class EmoteDataApi private constructor(
    private val dataHandler: EmoteDataHandler
                                      ) : AbstractDataApi<EmoteDto> {

    override suspend fun saveAll(dtos: List<EmoteDto>): List<EmoteDto> {
        return this.dataHandler.saveAll(dtos)
    }

    override suspend fun save(dto: EmoteDto): EmoteDto {
        return this.dataHandler.save(dto)
    }

    override suspend fun remove(dto: EmoteDto): Boolean {
        return this.dataHandler.remove(dto)
    }

    override suspend fun removeAll() {
        return this.dataHandler.removeAll()
    }

    suspend fun updateImage(dto: EmoteDto): EmoteDto {
        return this.dataHandler.updateImage(dto)
    }

    suspend fun updateName(dto: EmoteDto, newName: String): EmoteDto {
        return this.dataHandler.updateName(dto, newName)
    }

    suspend fun filterByNamePrefix(prefix: String): List<EmoteDto> {
        return this.dataHandler.filterByNamePrefix(prefix)
    }

    suspend fun getNumberOfModels(): Long {
        return this.dataHandler.getNumberOfModels()
    }
}