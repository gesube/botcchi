package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import hq.waifuparade.botcchi.datastore.services.PermissionDataHandler
import org.springframework.stereotype.Service

@Service
class PermissionDataApi private constructor(
    private val dataHandler: PermissionDataHandler
                                           ) : AbstractDataApi<PermissionDto> {

    override suspend fun saveAll(dtos: List<PermissionDto>): List<PermissionDto> {
        return this.dataHandler.saveAll(dtos)
    }

    override suspend fun save(dto: PermissionDto): PermissionDto {
        return this.dataHandler.save(dto)
    }

    override suspend fun remove(dto: PermissionDto): Boolean {
        return this.dataHandler.remove(dto)
    }

    override suspend fun removeAll() {
        this.dataHandler.removeAll()
    }

    suspend fun updateByName(dto: PermissionDto): PermissionDto {
        return this.dataHandler.updateByName(dto)
    }

    suspend fun removeByName(name: String): Boolean {
        return this.dataHandler.removeByName(name)
    }

    suspend fun findByName(name: String): PermissionDto? {
        return this.dataHandler.findByName(name)
    }

    suspend fun findByRelatedAction(relatedAction: ApplicationAction): List<PermissionDto> {
        return this.dataHandler.findByRelatedAction(relatedAction)
    }
}