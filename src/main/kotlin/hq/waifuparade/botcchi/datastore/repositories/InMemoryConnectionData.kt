package hq.waifuparade.botcchi.datastore.repositories

/**
 * Temporary data about the connection that isn't needed
 * anymore when the application stops.
 */
internal object InMemoryConnectionData {

    var currentLoginTime: Long = Long.MIN_VALUE
        private set

    var playlistOpen = false
        private set

    /**
     * Updates the login time if the provided login time is higher (= more recent) than the current one.
     * @param newLoginTime the new login time to update with
     * @return [Long] - the login time after it was maybe updated.
     */
    fun updateLoginTime(newLoginTime: Long): Long {
        if (newLoginTime > currentLoginTime) {
            currentLoginTime = newLoginTime
        }

        return currentLoginTime
    }

    /**
     * Sets the login time to the default value.
     * @return [Long] - the default login time
     */
    fun revertLoginTimeToDefault(): Long {
        currentLoginTime = Long.MIN_VALUE
        return currentLoginTime
    }

    /**
     * Updates the status of the playlist.
     * true = playlist is open / false = playlist is not open
     * @param playlistOpen - Provide true to set the playlist open or false to lock it
     * @return [Boolean] - true when the playlist is open, false otherwise
     */
    fun updatePlaylistOpen(playlistOpen: Boolean): Boolean {
        InMemoryConnectionData.playlistOpen = playlistOpen
        return InMemoryConnectionData.playlistOpen
    }
}