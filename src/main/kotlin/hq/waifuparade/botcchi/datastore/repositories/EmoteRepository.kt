package hq.waifuparade.botcchi.datastore.repositories

import hq.waifuparade.botcchi.datastore.models.EmoteModel
import jakarta.transaction.Transactional
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EmoteRepository : CrudRepository<EmoteModel, UUID> {


    /**
     * Removes the model with the provided name.
     * @param name the name of the model to remove
     * @return [Boolean] - true, if the model was removed, false otherwise
     */
    @Transactional
    fun removeByName(name: String): Int

    /**
     * Filters the underlying data structure by the provided name prefix.
     * @param prefix the name prefix to filter by
     * @return List<[EmoteModel]> - a list with the filtered results. Empty, if no data was found.
     */
    fun findByNameStartingWith(prefix: String): List<EmoteModel>

    /**
     * Tries to find the model with the provided name in the underlying data structure.
     * @param name the name of the model to find
     * @return [EmoteModel] - the model, if one was found, null otherwise
     */
    fun findByName(name: String): EmoteModel?
}