package hq.waifuparade.botcchi.datastore.repositories

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.models.PermissionModel
import jakarta.transaction.Transactional
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface PermissionRepository : CrudRepository<PermissionModel, UUID> {


    /**
     * Removes the model with the provided name.
     * @param name the name of the model to remove
     * @return [Boolean] - true, if the model was removed, false otherwise
     */
    @Transactional
    fun removeByName(name: String): Int

    /**
     * Tries to find the model with the provided name in the underlying data structure.
     * @param name the name of the model to find
     * @return [PermissionModel] - the model, if one was found, null otherwise
     */
    fun findByName(name: String): PermissionModel?

    fun findAllByRelatedAction(relatedAction: ApplicationAction): List<PermissionModel>
}