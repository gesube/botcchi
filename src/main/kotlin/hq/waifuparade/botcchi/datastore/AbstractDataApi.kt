package hq.waifuparade.botcchi.datastore

internal interface AbstractDataApi<Dto> {

    suspend fun saveAll(dtos: List<Dto>): List<Dto>

    suspend fun save(dto: Dto): Dto

    suspend fun remove(dto: Dto): Boolean

    suspend fun removeAll()
}