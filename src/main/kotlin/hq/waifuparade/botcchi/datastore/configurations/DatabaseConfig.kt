package hq.waifuparade.botcchi.datastore.configurations

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/**
 * Configuration class for initializing everything database related.
 */
@Configuration
@EnableJpaRepositories("hq.waifuparade.botcchi.datastore.repositories")
@EntityScan("hq.waifuparade.botcchi.datastore.models")
internal class DatabaseConfig()