package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.services.ConnectionDataHandler
import org.springframework.stereotype.Service

@Service
class ConnectionDataApi private constructor(
    private val dataHandler: ConnectionDataHandler
                                           ) {

    fun setLoginTime(loginTime: Long): Long {
        return this.dataHandler.setLoginTime(loginTime)
    }

    fun getLoginTime(): Long {
        return this.dataHandler.getLoginTime()
    }

    fun removeLoginTime(): Long {
        return this.dataHandler.removeLoginTime()
    }

    fun setPlaylistOpen(playlistOpen: Boolean): Boolean {
        return this.dataHandler.setPlaylistOpen(playlistOpen)
    }

    fun isPlaylistOpen(): Boolean {
        return this.dataHandler.isPlaylistOpen()
    }
}