package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.datastore.repositories.InMemoryConnectionData
import org.springframework.stereotype.Service

/**
 * Handler for all sorts of temporary connection data.
 */
@Service
internal class ConnectionDataHandler {

    fun setLoginTime(loginTime: Long): Long {
        return InMemoryConnectionData.updateLoginTime(loginTime)
    }

    fun getLoginTime(): Long {
        return InMemoryConnectionData.currentLoginTime
    }

    fun removeLoginTime(): Long {
        return InMemoryConnectionData.revertLoginTimeToDefault()
    }

    fun setPlaylistOpen(playlistOpen: Boolean): Boolean {
        return InMemoryConnectionData.updatePlaylistOpen(playlistOpen)
    }

    fun isPlaylistOpen(): Boolean {
        return InMemoryConnectionData.playlistOpen
    }
}