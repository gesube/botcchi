package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.datastore.models.UserModel
import hq.waifuparade.botcchi.datastore.repositories.UserRepository
import org.springframework.stereotype.Service

@Service
internal class UserDataHandler(
    private val repository: UserRepository,
) : AbstractDataHandler<UserModel, UserDto> {

    override suspend fun saveAll(dtos: List<UserDto>): List<UserDto> {
        val models = dtos.map { dto ->
            UserModel.Companion.fromDto(dto)
        }

        val upsertedModels = this.repository.saveAll(models)

        return upsertedModels.map { model ->
            UserModel.Companion.toDto(model)
        }
    }

    override suspend fun save(dto: UserDto): UserDto {
        val model = UserModel.Companion.fromDto(dto)
        val upsertedModel = this.repository.save(model)
        return UserModel.Companion.toDto(upsertedModel)
    }

    override suspend fun remove(dto: UserDto): Boolean {
        return this.removeByName(dto.name)
    }

    override suspend fun removeAll() {
        return this.repository.deleteAll()
    }

    override suspend fun getNumberOfModels(): Long {
        return this.repository.count()
    }

    suspend fun updateByName(dto: UserDto): UserDto {
        val persistedModel = this.repository.findByName(dto.name)
        var updateModel = UserModel.fromDto(dto)
        persistedModel?.let {
            persistedModel.updateModel(updateModel)

            this.repository.save(it)
        }
        return UserModel.toDto(persistedModel!!)
    }

    suspend fun removeByName(name: String): Boolean {
        return if (this.repository.removeByName(name) > 0) {
            true
        } else {
            false
        }
    }

    suspend fun findByName(name: String): UserDto? {
        val foundUser = this.repository.findByName(name)

        foundUser?.let {
            return UserModel.Companion.toDto(it)
        }

        return null
    }
}