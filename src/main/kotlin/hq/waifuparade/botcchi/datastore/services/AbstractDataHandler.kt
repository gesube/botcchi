package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.datastore.models.BaseModel

@Suppress("unused")
internal interface AbstractDataHandler<Model : BaseModel<Model>, Dto> {

    suspend fun saveAll(dtos: List<Dto>): List<Dto>

    suspend fun save(dto: Dto): Dto

    suspend fun remove(dto: Dto): Boolean

    suspend fun removeAll()

    suspend fun getNumberOfModels(): Long
}