package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import hq.waifuparade.botcchi.datastore.models.PermissionModel
import hq.waifuparade.botcchi.datastore.repositories.PermissionRepository
import org.springframework.stereotype.Service

@Service
internal class PermissionDataHandler(
    private val repository: PermissionRepository
) : AbstractDataHandler<PermissionModel, PermissionDto> {

    override suspend fun saveAll(dtos: List<PermissionDto>): List<PermissionDto> {
        val models = dtos.map { dto ->
            PermissionModel.Companion.fromDto(dto)
        }

        val upsertedModels = this.repository.saveAll(models)

        return upsertedModels.map { model ->
            PermissionModel.Companion.toDto(model)
        }
    }

    override suspend fun save(dto: PermissionDto): PermissionDto {
        val model = PermissionModel.Companion.fromDto(dto)
        val upsertedModel = this.repository.save(model)
        return PermissionModel.Companion.toDto(upsertedModel)
    }

    override suspend fun remove(dto: PermissionDto): Boolean {
        return if (this.repository.removeByName(dto.name) > 0) {
            true
        } else {
            false
        }
    }

    override suspend fun removeAll() {
        return this.repository.deleteAll()
    }

    override suspend fun getNumberOfModels(): Long {
        return this.repository.count()
    }

    suspend fun updateByName(dto: PermissionDto): PermissionDto {
        val persistedModel = this.repository.findByName(dto.name)
        persistedModel?.let {
            persistedModel.name = dto.name
            persistedModel.minRequiredRank = dto.minRequiredRank
            this.repository.save(it)
        }

        return PermissionModel.toDto(persistedModel!!)
    }

    suspend fun removeByName(name: String): Boolean {
        return if (this.repository.removeByName(name) > 0) {
            true
        } else {
            false
        }
    }

    suspend fun findByName(name: String): PermissionDto? {
        val foundModel = this.repository.findByName(name)

        foundModel?.let {
            return PermissionModel.Companion.toDto(it)
        }

        return null
    }

    suspend fun findByRelatedAction(relatedAction: ApplicationAction): List<PermissionDto> {
        return this.repository.findAllByRelatedAction(relatedAction).map {
            PermissionModel.Companion.toDto(it)
        }.toList()
    }
}