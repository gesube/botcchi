package hq.waifuparade.botcchi.datastore.services

import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import hq.waifuparade.botcchi.datastore.models.EmoteModel
import hq.waifuparade.botcchi.datastore.repositories.EmoteRepository
import org.springframework.stereotype.Service

@Service
internal class EmoteDataHandler(
    private val repository: EmoteRepository,
) : AbstractDataHandler<EmoteModel, EmoteDto> {

    override suspend fun saveAll(dtos: List<EmoteDto>): List<EmoteDto> {
        val models = dtos.map { dto ->
            EmoteModel.fromDto(dto)
        }

        val upsertedModels = this.repository.saveAll(models)

        return upsertedModels.map { model ->
            EmoteModel.toDto(model)
        }
    }

    override suspend fun save(dto: EmoteDto): EmoteDto {
        val model = EmoteModel.fromDto(dto)
        val updatedModel = this.repository.save(model)
        return EmoteModel.toDto(updatedModel)
    }

    override suspend fun remove(dto: EmoteDto): Boolean {
        return if (this.repository.removeByName(dto.name) > 0) {
            true
        } else {
            false
        }
    }

    override suspend fun removeAll() {
        return this.repository.deleteAll()
    }

    override suspend fun getNumberOfModels(): Long {
        return this.repository.count()
    }

    suspend fun updateImage(dto: EmoteDto): EmoteDto {
        val persistedModel = this.repository.findByName(dto.name)
        val updateModel = EmoteModel.fromDto(dto)

        persistedModel?.let {
            persistedModel.updateModel(updateModel)
            this.repository.save(persistedModel)

        }
        return EmoteModel.toDto(persistedModel!!)
    }

    suspend fun updateName(dto: EmoteDto, newName: String): EmoteDto {
        val persistedModel = this.repository.findByName(dto.name)
        val updateModel = EmoteModel.fromDto(dto)
        updateModel.name = newName
        var renamedModel = EmoteModel()

        persistedModel?.let {
            persistedModel.updateModel(updateModel)
            renamedModel = this.repository.save(persistedModel)

        }

        return EmoteModel.toDto(renamedModel)
    }

    suspend fun filterByNamePrefix(prefix: String): List<EmoteDto> {
        return this.repository.findByNameStartingWith(prefix).map {
            EmoteModel.toDto(it)
        }.toList()
    }
}