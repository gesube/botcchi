package hq.waifuparade.botcchi.datastore

import hq.waifuparade.botcchi.datastore.dtos.UserDto
import hq.waifuparade.botcchi.datastore.services.UserDataHandler
import org.springframework.stereotype.Service

@Service
class UserDataApi private constructor(
    private val dataHandler: UserDataHandler
                                     ) : AbstractDataApi<UserDto> {

    override suspend fun saveAll(dtos: List<UserDto>): List<UserDto> {
        return this.dataHandler.saveAll(dtos)
    }

    override suspend fun save(dto: UserDto): UserDto {
        return this.dataHandler.save(dto)
    }

    override suspend fun remove(dto: UserDto): Boolean {
        return this.dataHandler.remove(dto)
    }

    override suspend fun removeAll() {
        this.dataHandler.removeAll()
    }

    suspend fun updateByName(dto: UserDto): UserDto {
        return this.dataHandler.updateByName(dto)
    }

    suspend fun removeByName(name: String): Boolean {
        return this.dataHandler.removeByName(name)
    }

    suspend fun findByName(name: String): UserDto? {
        return this.dataHandler.findByName(name)
    }
}