package hq.waifuparade.botcchi.datastore.models

import hq.waifuparade.botcchi.datastore.dtos.UserDto
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

/**
 * Model to represent a cytuber user.
 */
@Entity
@Table(name = "users")
class UserModel(
    @Id
    @Column(name = "name", unique = true)
    var name: String = "",

    @Column(name = "rank")
    var rank: Double = Double.MIN_VALUE
) : BaseModel<UserModel> {

    internal companion object {

        /**
         * Creates a new instance of the [UserModel] from the provided [UserDto].
         * @param dto the dto to base the new model on
         * @return [UserModel] - the newly created model
         */
        fun fromDto(dto: UserDto): UserModel {
            return UserModel(name = dto.name, rank = dto.rank)
        }

        /**
         * Creates a new instance of the [UserDto] from the provided [UserModel].
         * @param model the model to base the new dto on
         * @return [UserDto] - the newly created dto
         */
        fun toDto(model: UserModel): UserDto {
            return UserDto(name = model.name, rank = model.rank)
        }
    }

    override fun updateModel(model: UserModel) {
        this.name = model.name
        this.rank = model.rank
    }

}