package hq.waifuparade.botcchi.datastore.models

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDate
import java.time.LocalTime

@Entity
@Table(name = "user_counts")
@Suppress("unused")
internal class UserCountModel(

    @Id
    @Column(name = "date")
    val date: LocalDate = LocalDate.now(),

    @Column(name = "time")
    val time: LocalTime = LocalTime.now(),

    @Column(name = "numberOfUsers")
    var numberOfUsers: Int = Int.MIN_VALUE
                                  )