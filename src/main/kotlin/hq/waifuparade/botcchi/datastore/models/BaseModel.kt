package hq.waifuparade.botcchi.datastore.models

/**
 * Base interface for data models (models that are only stored in memory).
 */
fun interface BaseModel<Model> {

    /**
     * Updates the current model with the values from the provided [Model].
     * @param model a model with the values to insert into the current model.
     */
    fun updateModel(model: Model)
}