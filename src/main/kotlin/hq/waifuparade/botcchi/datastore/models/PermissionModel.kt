package hq.waifuparade.botcchi.datastore.models

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.dtos.PermissionDto
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.Id
import jakarta.persistence.Table

/**
 * Model to represent a cytube permission.
 */
@Entity
@Table(name = "permissions")
class PermissionModel(
    @Id
    @Column(name = "name")
    var name: String = "",

    @Column(name = "minRequiredRank")
    var minRequiredRank: Double = Double.MAX_VALUE,

    @Column(name = "relatedAction")
    @Enumerated(EnumType.STRING)
    var relatedAction: ApplicationAction = ApplicationAction.DO_NOTHING
) : BaseModel<PermissionModel> {

    internal companion object {

        /**
         * Creates a new instance of the [PermissionModel] from the provided [PermissionDto].
         * @param dto the dto to base the new model on
         * @return [PermissionModel] - the newly created model
         */
        fun fromDto(dto: PermissionDto): PermissionModel {
            val relatedAction = inferRelatedActionFromPermissionName(dto.name)
            return PermissionModel(name = dto.name, minRequiredRank = dto.minRequiredRank, relatedAction = relatedAction)
        }

        /**
         * Creates a new instance of the [PermissionDto] from the provided [PermissionModel].
         * @param model the model to base the new dto on
         * @return [PermissionDto] - the newly created dto
         */
        fun toDto(model: PermissionModel): PermissionDto {
            return PermissionDto(name = model.name, minRequiredRank = model.minRequiredRank)
        }

        private fun inferRelatedActionFromPermissionName(name: String): ApplicationAction {
            return when (name) {
                "oplaylistadd" -> ApplicationAction.ADD_MEDIA   // for open playlists
                "playlistadd" -> ApplicationAction.ADD_MEDIA    // for restricted / general playlists

                else -> ApplicationAction.DO_NOTHING
            }
        }
    }

    override fun updateModel(model: PermissionModel) {
        this.name = model.name
        this.minRequiredRank = model.minRequiredRank
    }
}

/**
 *     "addnontemp": 2,
 *     "ban": 2,
 *     "chat": 0,
 *     "chatclear": 2,
 *     "deletefromchannellib": 2,
 *     "drink": 1.5,
 *     "emoteedit": 3,
 *     "emoteimport": 3,
 *     "exceedmaxdurationperuser": 2,
 *     "exceedmaxitems": 2,
 *     "exceedmaxlength": 2,
 *     "filteredit": 3,
 *     "filterimport": 3,
 *     "kick": 1.5,
 *     "leaderctl": 2,
 *     "motdedit": 3,
 *     "mute": 1.5,
 *     "oplaylistadd": 0,
 *     "oplaylistaddlist": 1.5
 *     "oplaylistdelete": 2,
 *     "oplaylistjump": 1.5,
 *     "oplaylistmove": 1.5,
 *     "oplaylistnext": 1,
 *     "playlistadd": -1,
 *     "playlistaddcustom": 3,
 *     "playlistaddlist": 1.5,
 *     "playlistaddlive": 1.5,
 *     "playlistaddrawfile": 2,
 *     "playlistclear": 2,
 *     "playlistdelete": 2,
 *     "playlistjump": 1.5,
 *     "playlistlock": 2,
 *     "playlistmove": 1.5,
 *     "playlistnext": 1.5,
 *     "playlistshuffle": 2,
 *     "pollctl": 1.5,
 *     "pollvote": -1,
 *     "seeplaylist": -1,
 *     "settemp": 2,
 *     "viewhiddenpoll": 1.5,
 *     "viewvoteskip": 1.5,
 *     "voteskip": -1,
 **/