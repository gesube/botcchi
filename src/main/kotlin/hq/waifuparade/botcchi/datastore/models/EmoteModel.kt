package hq.waifuparade.botcchi.datastore.models

import hq.waifuparade.botcchi.datastore.dtos.EmoteDto
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

/**
 * Model to represent an emote.
 */
@Entity
@Table(name = "emotes")
class EmoteModel(
    @Id
    @Column(name = "name", unique = true)
    var name: String = "",

    @Column(name = "image")
    var image: String = ""
) : BaseModel<EmoteModel> {

    internal companion object {

        /**
         * Creates a new instance of the [EmoteModel] from the provided [EmoteDto].
         * @param dto the dto to base the new model on
         * @return [EmoteModel] - the newly created model
         */
        fun fromDto(dto: EmoteDto): EmoteModel {
            return EmoteModel(name = dto.name, image = dto.image)
        }

        /**
         * Creates a new instance of the [EmoteDto] from the provided [EmoteModel].
         * @param model the model to base the new dto on
         * @return [EmoteDto] - the newly created dto
         */
        fun toDto(model: EmoteModel): EmoteDto {
            return EmoteDto(name = model.name, image = model.image)
        }
    }

    override fun updateModel(model: EmoteModel) {
        this.name = model.name
        this.image = model.image
    }
}