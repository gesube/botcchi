package hq.waifuparade.botcchi.application.shutdownactions.impls

import hq.waifuparade.botcchi.application.shutdownactions.ShutdownAction
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.stereotype.Service

/**
 * Disconnects all existing clients from the socket servers.
 */
@Service
@InjectLogger
internal class SocketClientDisconnectingShutdownAction(
    private val cytubeSocketClient: CytubeSocketClient
                                                      ) : ShutdownAction {

    override fun executeAction() {
        this.logger.info { "Open sockets to the cytube server are being closed." }
        this.cytubeSocketClient.disconnectFromSocketServer()
        this.logger.info { "Closed all open sockets to the cytube server." }
    }
}