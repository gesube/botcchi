package hq.waifuparade.botcchi.application.shutdownactions

/**
 * Every implementation of this class will be called on shutdown.
 */
fun interface ShutdownAction {

    fun executeAction()
}