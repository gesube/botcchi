package hq.waifuparade.botcchi.application.startupactions.impls

import hq.waifuparade.botcchi.application.startupactions.StartupAction
import hq.waifuparade.botcchi.socket.CytubeSocketClient
import org.springframework.stereotype.Service

/**
 * Retrieves socket servers, builds a socket client and connects the client to the server.
 */
@Service
internal class SocketClientConnectingStartupAction(
    private val cytubeSocketClient: CytubeSocketClient
                                                  ) : StartupAction {

    /**
     * Creates and connects a socket client to the user defined cytube server.
     */
    override fun executeAction() {
        this.cytubeSocketClient.connectToSocketServer()
    }
}