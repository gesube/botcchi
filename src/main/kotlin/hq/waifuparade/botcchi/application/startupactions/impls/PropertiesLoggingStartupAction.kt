package hq.waifuparade.botcchi.application.startupactions.impls

import hq.waifuparade.botcchi.application.startupactions.StartupAction
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.core.env.AbstractEnvironment
import org.springframework.core.env.EnumerablePropertySource
import org.springframework.core.env.Environment
import org.springframework.core.io.support.ResourcePropertySource
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.StreamSupport

/**
 * Prints out all user-defined variables from properties file.
 */
@Service
@InjectLogger
internal class PropertiesLoggingStartupAction(
    private val env: Environment
                                             ) : StartupAction {

    override fun executeAction() {
        this.logger.info { "====== Environment and configuration ======" }
        val sources = (env as AbstractEnvironment).propertySources

        StreamSupport.stream(sources.spliterator(), false)
            .filter { propertySource ->
                propertySource is ResourcePropertySource
            }
            .map { propertySource -> (propertySource as EnumerablePropertySource<*>).propertyNames }
            .flatMap(Arrays::stream)
            .sorted()
            .forEach { property -> this.logger.info { "$property, $env.getProperty(property)" } }

        this.logger.info { "===========================================" }
    }
}