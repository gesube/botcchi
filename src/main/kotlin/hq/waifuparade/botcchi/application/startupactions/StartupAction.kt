package hq.waifuparade.botcchi.application.startupactions

/**
 * Every implementation of this class will be called on startup.
 */
fun interface StartupAction {

    fun executeAction()
}