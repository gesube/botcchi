package hq.waifuparade.botcchi.application

import hq.waifuparade.botcchi.application.startupactions.StartupAction
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

/**
 * Listens on the startup event and executes every implemented startup action.
 */
@Component
@InjectLogger
internal class ApplicationStartup(
    private val startupActions: List<StartupAction>
                                 ) {

    @EventListener
    @Suppress("UNUSED_PARAMETER")
    fun handleContextRefresh(event: ContextRefreshedEvent) {
        this.logger.info { "Executing startup actions..." }
        this.startupActions.forEach { startupAction -> startupAction.executeAction() }
        this.logger.info { "Application has fully started." }
        this.logger.info { "Tanoshinde ne~" }
    }
}