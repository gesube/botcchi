package hq.waifuparade.botcchi.application

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.PropertySource

@SpringBootApplication
@PropertySource("application.properties", ignoreResourceNotFound = true)
@ComponentScan("hq.waifuparade.botcchi")
internal class ApplicationMain {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<ApplicationMain>(*args)
        }
    }
}