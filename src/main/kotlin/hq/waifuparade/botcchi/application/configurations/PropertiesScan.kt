package hq.waifuparade.botcchi.application.configurations

import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Configuration

/**
 * Enables the ConfigurationProperties annotations in all packages.
 */
@Configuration
@ConfigurationPropertiesScan("hq.waifuparade.botcchi")
internal class PropertiesScan