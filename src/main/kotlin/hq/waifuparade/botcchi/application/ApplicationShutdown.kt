package hq.waifuparade.botcchi.application

import hq.waifuparade.botcchi.application.shutdownactions.ShutdownAction
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import org.springframework.context.event.ContextClosedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

/**
 * Listens on the shutdown event and executes every implemented shutdown action.
 */
@Component
@InjectLogger
internal class ApplicationShutdown(
    private val shutdownActions: List<ShutdownAction>
                                  ) {

    @EventListener
    @Suppress("UNUSED_PARAMETER")
    fun handleContextClose(event: ContextClosedEvent) {
        this.logger.info { "Shutting down..." }
        this.shutdownActions.forEach { shutdownAction -> shutdownAction.executeAction() }
        this.logger.info { "Mata nee~" }
    }
}