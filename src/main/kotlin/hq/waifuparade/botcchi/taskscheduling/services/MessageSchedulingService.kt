package hq.waifuparade.botcchi.taskscheduling.services

import hq.waifuparade.botcchi.cytube.emitter.dtos.PersonalMessageEventRequest
import hq.waifuparade.botcchi.cytube.emitter.services.PMEventEmitter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service

@Service
class MessageSchedulingService private constructor(
    private val emitter: PMEventEmitter
                                        ) {

    fun scheduleMessage(delay: Long, message: String, userToSendTo: String) {
        val pmEventRequest = PersonalMessageEventRequest(msg = message, to = userToSendTo)

        CoroutineScope(Job()).launch {
            delay(delay)
            this@MessageSchedulingService.emitter.emitEvent(pmEventRequest)
        }
    }
}