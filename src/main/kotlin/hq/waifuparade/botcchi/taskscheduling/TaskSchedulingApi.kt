package hq.waifuparade.botcchi.taskscheduling

import hq.waifuparade.botcchi.taskscheduling.services.MessageSchedulingService
import org.springframework.stereotype.Service

@Service
class TaskSchedulingApi(
    private val messageSchedulingService: MessageSchedulingService
                        ) {

    fun scheduleMessages(delay: Long, message: String, usersToSendTo: List<String>) {
        usersToSendTo.forEach { user ->
            this.messageSchedulingService.scheduleMessage(delay, message, user)
        }
    }

    fun scheduleMessage(delay: Long, message: String, userToSendTo: String) {
        this.messageSchedulingService.scheduleMessage(delay, message, userToSendTo)
    }
}