package hq.waifuparade.botcchi.auth

import hq.waifuparade.botcchi.auth.enums.ApplicationAction
import hq.waifuparade.botcchi.datastore.ConnectionDataApi
import hq.waifuparade.botcchi.datastore.PermissionDataApi
import hq.waifuparade.botcchi.datastore.UserDataApi
import org.springframework.stereotype.Service

@Service
class AuthApi(
    private val userDataApi: UserDataApi,
    private val permissionDataApi: PermissionDataApi,
    private val connectionDataApi: ConnectionDataApi
             ) {

    suspend fun hasPermissionForAction(action: ApplicationAction, userName: String): Boolean {
        val user = this.userDataApi.findByName(userName) ?: return false
        val permission = this.permissionDataApi.findByRelatedAction(action).toMutableList()

        if (permission.isEmpty()) {
            return false
        }

        if (action == ApplicationAction.ADD_MEDIA) {
            val unusedPermission = when (this.connectionDataApi.isPlaylistOpen()) {
                true -> {
                    permission.find { !it.name.startsWith("o") }
                }
                false -> {
                    permission.find { it.name.startsWith("o") }
                }
            }

            permission.remove(unusedPermission)
        }

        if (permission.size == 1) {
            return user.rank > permission[0].minRequiredRank
        }

        return false
    }
}