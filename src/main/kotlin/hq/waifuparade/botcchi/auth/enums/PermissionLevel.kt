package hq.waifuparade.botcchi.auth.enums

import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger

internal enum class PermissionLevel(val rank: Double) {

    ANONYMOUS(-1.0),
    GUEST(0.0),
    REGISTERED(1.0),
    LEADER(1.5),
    MODERATOR(2.0),
    CHANNEL_ADMIN(3.0),
    OWNER(4.0),
    FOUNDER(5.0),
    NOBODY(255.0);

    @InjectLogger
    companion object {

        @JvmStatic
        fun getPermissionFromRank(rank: Double): PermissionLevel {
            entries.forEach { permissionRank ->
                if (permissionRank.rank == rank) {
                    return permissionRank
                }
            }

            this.logger.info { "Unknown enum type ${rank}. Allowed values are $entries." }
            this.logger.info { "Defaulting to not giving permission." }
            return NOBODY
        }
    }
}