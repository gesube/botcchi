package hq.waifuparade.botcchi.auth.enums

enum class ApplicationAction {

    ADD_MEDIA,
    DO_NOTHING
}