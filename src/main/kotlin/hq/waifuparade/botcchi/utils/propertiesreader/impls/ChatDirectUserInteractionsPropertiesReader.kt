package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertiesReader
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Class to capsule reading the environment variables for direct user interactions.
 */
@ConfigurationProperties("chat.direct-user-interaction")
data class ChatDirectUserInteractionsPropertiesReader(
    val greetingWords: List<String>,
    val leavingWords: List<String>,
    val skinshipWords: List<String>,
    val greetingReactionWords: List<String>,
    val leavingReactionWords: List<String>,
    val skinshipReactionWords: List<String>,
    val greetingReactionEmotes: List<String>,
    val leavingReactionEmotes: List<String>,
    val skinshipReactionEmotes: List<String>
                                                     ) : AbstractPropertiesReader