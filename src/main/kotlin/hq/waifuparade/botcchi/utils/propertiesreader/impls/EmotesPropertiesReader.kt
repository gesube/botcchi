package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertiesReader
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Class to capsule reading the emote-related environment variables.
 */
@ConfigurationProperties("emotes")
data class EmotesPropertiesReader(
    val prefix: String = ""
                                 ) : AbstractPropertiesReader