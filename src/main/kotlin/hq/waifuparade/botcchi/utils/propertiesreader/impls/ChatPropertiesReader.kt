package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertiesReader
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Class to capsule reading the environment variables for chat commands.
 */
@ConfigurationProperties("chat.command")
data class ChatPropertiesReader(
    val general: General,
    val addMedia: AddMedia,
    val timer: Timer
                               ) : AbstractPropertiesReader {

    data class General(
        val identifier: String
                      )

    data class AddMedia(
        val identifier: String,
        val cooldownBetweenAdditions: Long = 1
                       )

    data class Timer(
        val identifier: String
                    )
}