package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertiesReader
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Class to capsule reading the feature toggles.
 */
@ConfigurationProperties("chat.feature.enable")
data class ChatFeatureToggleReader(
    val directUserInteraction: DirectUserInteraction,
    val command: Command
                                  ) : AbstractPropertiesReader {

    data class DirectUserInteraction(
        val greeting: Boolean = false,
        val leaving: Boolean = false,
        val skinship: Boolean = false
                                    )

    data class Command(
        val addMedia: Boolean = false,
        val timer: Boolean = false
                      )
}