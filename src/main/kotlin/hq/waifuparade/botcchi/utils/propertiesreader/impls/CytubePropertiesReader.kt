package hq.waifuparade.botcchi.utils.propertiesreader.impls

import hq.waifuparade.botcchi.utils.propertiesreader.AbstractPropertiesReader
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Class to capsule reading the cytube-related environment variables.
 */
@ConfigurationProperties("cytube")
data class CytubePropertiesReader(
    val room: Room,
    val server: Server,
    val user: User,
    val socket: Socket
                                 ) : AbstractPropertiesReader {

    data class Server(
        val url: String = "",
        val socketServerListUrl: String = ""
                     )

    data class Room(
        val name: String = "",
        val password: String = ""
                   )

    data class User(
        val name: String = "",
        val password: String = ""
                   )

    data class Socket(
        val listenerDelay: Long = Long.MIN_VALUE
                     )
}