package hq.waifuparade.botcchi.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

/**
 * Class to encapsulate the json mapper.
 */
class JsonMapper private constructor() {

    companion object {

        @JvmSynthetic
        @PublishedApi
        @Suppress("EmptyMethod")
        internal val instance: ObjectMapper = ObjectMapper().registerKotlinModule()

        /**
         * Converts the provided json string to an instance of the provided type.
         * @param sourceString the json string to convert to an instance of [T]
         * @return [T] - an instance of the provided type
         */
        inline fun <reified T> convertStringToObject(sourceString: String): T {
            return this.instance.readValue(sourceString)
        }

        /**
         * Converts the provided object to an instance of the provided type.
         * @param sourceObject the object to convert to an instance of [T]
         * @return [T] - an instance of the provided type
         */
        inline fun <reified T> convertObjectToObject(sourceObject: Any): T {
            return this.instance.convertValue(sourceObject)
        }

        /**
         * Converts the provided object to a json string representation of its content.
         * @param sourceObject the object to convert to a json string
         * @return [String] - the json representation of the [sourceObject]
         */
        fun convertObjectToString(sourceObject: Any): String {
            return this.instance.writeValueAsString(sourceObject)
        }
    }
}