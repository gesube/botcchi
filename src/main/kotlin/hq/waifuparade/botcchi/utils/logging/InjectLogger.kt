package hq.waifuparade.botcchi.utils.logging

import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging

/**
 * Annotation to inject a logger instance into a class.
 * Currently, it's an instance of [KLogger].
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class InjectLogger {

    companion object {

        inline val <reified T> T.logger: KLogger
            get() = KotlinLogging.logger(T::class.java.simpleName)
    }
}