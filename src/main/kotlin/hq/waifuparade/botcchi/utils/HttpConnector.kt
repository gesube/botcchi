package hq.waifuparade.botcchi.utils

import okhttp3.CipherSuite
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.TlsVersion
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Build and send http requests.
 */
class HttpConnector {

    companion object {

        private val httpConnectionSpec =
            ConnectionSpec.Builder(ConnectionSpec.CLEARTEXT).build()

        private val httpsConnectionSpec =
            ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256
                             )
                .build()

        private val httpClient =
            OkHttpClient.Builder()
                .connectionSpecs(Collections.singletonList(httpConnectionSpec))
                .build()

        private val httpsClient =
            OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectionSpecs(listOf(httpsConnectionSpec, httpConnectionSpec))
                .build()

        /**
         * Executes a GET request against the given [urlString].
         * @param urlString - the URL to execute a GET request against as a [String], e.g. https://www.google.com
         */
        @JvmStatic
        fun doGetRequest(urlString: String): Response {
            val request = Request.Builder().url(urlString).method(method = "GET", body = null).build()

            if (urlString.startsWith("https")) {
                return this.httpsClient.newCall(request).execute()
            }

            return this.httpClient.newCall(request).execute()
        }
    }
}