package hq.waifuparade.botcchi.socket

import hq.waifuparade.botcchi.events.ApplicationEventsApi
import hq.waifuparade.botcchi.events.enums.ApplicationEventType
import hq.waifuparade.botcchi.socket.services.CytubeSocketClientCreator
import hq.waifuparade.botcchi.socket.services.CytubeSocketServerRetriever
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONObject
import org.springframework.stereotype.Service

/**
 * Coordinates everything related to the cytube socket.
 */
@Service
@InjectLogger
class CytubeSocketClient private constructor(
    private val cytubeSocketServerRetriever: CytubeSocketServerRetriever,
    private val cytubeSocketClientCreator: CytubeSocketClientCreator,
    private val applicationEventsApi: ApplicationEventsApi
                                            ) {

    private var socketClient: Socket? = null

    /**
     * Builds a socket client and connects the client to one of the cytube socket servers.
     */
    fun connectToSocketServer() {
        if (this.socketClient == null) {
            val socketServers = this.cytubeSocketServerRetriever.retrieveSocketServersForRoom()
            this.socketClient = this.cytubeSocketClientCreator.createSocketClient(socketServers)
        }

        this.socketClient?.let { socketClient ->
            if (socketClient.connected()) {
                return@let
            }

            this.logger.info { "Connecting socket client to cytube server." }
            this.logger.info { "Please wait..." }
            try {
                socketClient.connect()
                this.logger.info { "Socket client has connected to cytube server." }
                this.applicationEventsApi.publishEvent(ApplicationEventType.SOCKET_CONNECTED)
            } catch (ex: Exception) {
                this.logger.error { "$ex.message, $ex" }
            }
        }
    }

    /**
     * Disconnects all existing clients from the socket servers.
     */
    fun disconnectFromSocketServer() {
        this.socketClient?.let { socketClient ->
            socketClient.disconnect()
            this.applicationEventsApi.publishEvent(ApplicationEventType.SOCKET_DISCONNECTED)
            this.socketClient = null
        }
    }

    /**
     * Registers the given listener to the given event.
     * @param event - event name.
     * @param fn - the listener (function) to execute
     * @return a reference to the underlying emitter object.
     */
    fun on(event: String, fn: Emitter.Listener): Emitter? {
        return this.socketClient?.on(event, fn)
    }

    /**
     * Registers the given one time listener to the given event.
     * @param event - event name.
     * @param fn - the listener (function) to execute
     * @return a reference to the underlying emitter object.
     */
    fun once(event: String, fn: Emitter.Listener): Emitter? {
        return this.socketClient?.once(event, fn)
    }

    /**
     * Deregisters the given listener from the given event.
     * @param event - the event to no longer listen to
     * @param fn - optional - the listener (function) to deregister from the event
     * @return a reference to the underlying emitter object.
     */
    fun off(event: String, fn: Emitter.Listener? = null): Emitter? {
        fn?.let { return this.socketClient?.off(event, fn) }
        return this.socketClient?.off(event)
    }

    /**
     * Deregisters all listeners from all events.
     * @return a reference to the underlying emitter object.
     */
    fun off(): Emitter? {
        return this.socketClient?.off()
    }

    /**
     * Emits the given event with the given [eventData].
     * @param event - the event to emit
     * @param eventData - additional data to emit for the event
     */
    fun emit(event: String, eventData: JSONObject): Emitter? {
        return this.socketClient?.emit(event, eventData)
    }

    /**
     * Emits the given event with the given [eventData].
     * @param event - the event to emit
     * @param eventData - additional data to emit for the event
     *
     * !! Only use this for transferring single strings to cytube - for objects use [emit] !!
     */
    fun emitWithSingleString(event: String, eventData: String): Emitter? {
        return this.socketClient?.emit(event, eventData)
    }
}