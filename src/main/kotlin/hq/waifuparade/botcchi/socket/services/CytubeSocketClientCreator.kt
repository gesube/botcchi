package hq.waifuparade.botcchi.socket.services

import hq.waifuparade.botcchi.socket.dtos.SocketServer
import io.socket.client.IO
import io.socket.client.Socket
import org.springframework.stereotype.Service

/**
 * Builds a socket client and connects the client to the server.
 */
@Service
internal class CytubeSocketClientCreator {

    /**
     * Builds a socket client and connects the client to the one of the given socket servers.
     * @param socketServers - a list of socket servers to connect to
     */
    fun createSocketClient(socketServers: List<SocketServer>): Socket? {
        socketServers.forEach { socketServer ->
            // cytube usually has a secure and an insecure socket server per channel
            // we only want to use the secure one
            if (socketServer.secure) {
                return IO.socket(socketServer.url, this.getOptions())
            }
        }

        return null
    }

    /**
     * Creates the options for the socket client.
     * @return IO.Options - the options for the socket client
     */
    private fun getOptions(): IO.Options {
        val options = IO.Options()
        options.forceNew = true
        options.transports = arrayOf("websocket")
        options.reconnectionAttempts = 5
        return options
    }
}