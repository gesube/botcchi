package hq.waifuparade.botcchi.socket.services

import hq.waifuparade.botcchi.socket.dtos.SocketServer
import hq.waifuparade.botcchi.socket.dtos.SocketServerListWrapper
import hq.waifuparade.botcchi.utils.HttpConnector
import hq.waifuparade.botcchi.utils.JsonMapper
import hq.waifuparade.botcchi.utils.logging.InjectLogger
import hq.waifuparade.botcchi.utils.logging.InjectLogger.Companion.logger
import hq.waifuparade.botcchi.utils.propertiesreader.impls.CytubePropertiesReader
import org.springframework.stereotype.Service

/**
 * Retrieves the socket server list from the cytube server.
 */
@Service
@InjectLogger
internal class CytubeSocketServerRetriever(
    private val cytubePropertiesReader: CytubePropertiesReader
                                          ) {

    /**
     * Retrieves the socket server list from the cytube server.
     * @return List<[SocketServer]> - an immutable list with the retrieved socket servers
     */
    fun retrieveSocketServersForRoom(): List<SocketServer> {
        val url = this.buildSocketServerRetrieveUrl()
        val response = HttpConnector.doGetRequest(url)

        if (!response.isSuccessful) {
            this.logger.info { "$url is not accessible." }
            this.logger.info { "${response.code} - ${response.message}" }
        } else {
            response.body?.let {
                try {
                    return JsonMapper.convertStringToObject<SocketServerListWrapper>(it.string()).servers
                } catch (ex: Exception) {
                    this.logger.error {
                        "${it.string()} does not match " +
                            "the expected structure of ${this.createStringOfExpectedStructure()}"
                                      }
                    this.logger.error { "$ex.message, $ex" }
                }
            }
        }

        return listOf()
    }

    /**
     * Handles the sanitizing, creation etc. for the retrieval request url.
     * @return String - the url for the retrieval request
     */
    private fun buildSocketServerRetrieveUrl(): String {
        if (this.cytubePropertiesReader.server.socketServerListUrl.isNotEmpty()) {
            // It could be possible that the user (unknowingly) set something like https://cytu.be//socketconfig,
            // so we need to handle that here
            return this.cytubePropertiesReader.server.socketServerListUrl.replace("//", "/")
                .replace("/www", "//www")
        }

        // Same thing here
        val sanitizedRetrieveUrl = this.cytubePropertiesReader.server.url.trimEnd('/')
        return "$sanitizedRetrieveUrl/socketconfig/${this.cytubePropertiesReader.room.name}.json"
    }

    /**
     * Creates a string representation of the expected structure of the response body from the server retrieve request.
     * @return String - a string representation of the expected structure of the response body
     */
    private fun createStringOfExpectedStructure(): String {
        val socketServer1 = SocketServer(url = "", secure = true)
        val socketServer2 = SocketServer(url = "", secure = true)
        val socketServerListWrapper = SocketServerListWrapper(servers = listOf(socketServer1, socketServer2))
        return JsonMapper.convertObjectToString(socketServerListWrapper)
    }
}