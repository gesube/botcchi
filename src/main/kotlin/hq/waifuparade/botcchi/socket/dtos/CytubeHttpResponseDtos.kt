package hq.waifuparade.botcchi.socket.dtos

data class SocketServer(val url: String, val secure: Boolean)

data class SocketServerListWrapper(val servers: List<SocketServer>)